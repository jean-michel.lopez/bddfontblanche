#include <MainWindow.h>

void MainWindow::deleteComment(int a, int b)
{
    if (QMessageBox::question(NULL, "Suppression d'un commentaire", "Confirmez la suppression du commentaire", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
    {
        QTableWidget *commentTable;
        commentTable = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(7, 1)->widget());
        QFile commentFile("bin/comment");
        QString line;
        QStringList dataRow;
        int Id;
        Id = commentTable->itemAt(a, b)->data(Qt::UserRole).toInt();
        QString s;
        if (!commentFile.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/comment.");
            urgentStop();
            return;
        }else{
            QTextStream commentStream(&commentFile);
            line = commentStream.readLine();
            s.append(line + "\n");
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/comment\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!commentStream.atEnd()  && line != "###")
                {
                    line = commentStream.readLine();
                    s.append(line + "\n");
                }
                while(!commentStream.atEnd())
                {
                    line = commentStream.readLine();
                    dataRow = line.split("\t");
                    if (dataRow.at(0).toInt() != Id)
                    {
                        s.append(line + "\n");
                    }
                }
                commentFile.resize(0);
                commentStream << s;
            }
        }
        commentFile.close();
        commentTable->removeRow(a);
    }
}
