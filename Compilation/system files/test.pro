QT += widgets

SOURCES += \
    main.cpp \
    MainWindow.cpp \
    consultorexportdata.cpp \
    backto0slot.cpp \
    consultdata.cpp \
    urgentstop.cpp \
    whoclickedme.cpp \
    backtoconsultorexport.cpp \
    expanddataconsult.cpp \
    changedatedataconsult.cpp \
    exportdatasSlot.cpp \
    exportallslot.cpp \
    exportfinalslot.cpp \
    preparecampaignslot.cpp \
    savedatacontext.cpp \
    inputdataset.cpp \
    boardinputset.cpp \
    inputdata.cpp \
    expanddatainput.cpp \
    dialogdate.cpp \
    addcomment.cpp \
    deletecomment.cpp \
    addaxis.cpp \
    addaxisclass.cpp \
    updatecommentandmodifymeasure.cpp \
    changecomment.cpp \
    checkvalue.cpp \
    modifiesinputtable.cpp \
    saverecord.cpp \
    modifiesstatus.cpp \
    newstatus.cpp \
    modifiesdatabase.cpp \
    modifiesdatas.cpp \
    changeaxischaracteristics.cpp \
    addparameter.cpp \
    loadpreviousdatas.cpp \
    modifiesdataplot.cpp \
    focuschangepolicy.cpp \
    deleterow.cpp \
    moveintotable.cpp \
    erasecontent.cpp \
    loadboardinput.cpp \
    loaddatas.cpp \
    changelocalisationparameter.cpp \
    changecharacteristics.cpp \
    expandmodifiesdata.cpp \
    deletemeasure.cpp \
    modifiesvaluedata.cpp \
    expandchangecharacteristics.cpp \
    newparameter.cpp \
    deleteparameter.cpp \
    changeparameterclass.cpp \
    updatecommentconsult.cpp

HEADERS += \
    MainWindow.h \
    dialogdate.h \
    addaxisclass.h \
    newstatus.h \
    focuschangepolicy.h \
    changeparameterclass.h

CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11
