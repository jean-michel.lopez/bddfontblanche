#include <MainWindow.h>

void MainWindow::saveRecord()
{
    // we first read the variables
    QFile varFile("bin/dictionary/variables");
    int Id, countLine(-1);
    QMap<QString, int> reproVar;
    QMap<int, QStringList> varMap;
    QFile * tempFile;
    QMap<QString, QMap<QString, int> > factorPosition;
    QMap<QString, QMap<int, QStringList> > factorMap;
    QMap<QString, int> tempPosition;
    QMap<int, QStringList> tempMap;
    QString line;
    QStringList dataRow;
    int tempId;
    int nVarFem(0);
    int GUId, nMeasure, nReproMeasure;
    QDate measureDate;
    measureDate = qobject_cast<QDateEdit *>(mainLayout->itemAtPosition(5, 2)->widget())->date();
    if (!varFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/variables.");
        urgentStop();
        return;
    }else{
        QTextStream varStream(&varFile);
        line = varStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/variables\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!varStream.atEnd()  && line != "###")
            {
                line = varStream.readLine();
                reproVar.insert(line.split("\t").at(0), countLine);
                countLine++;
            }
            while(!varStream.atEnd())
            {
                dataRow = varStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                varMap.insert(Id, dataRow);
                if (varMap[Id].size() > reproVar["TypeVar"])
                {
                    if (varMap[Id].at(reproVar["TypeVar"]).startsWith("eKEY"))
                    {
                        // factor variable
                        if (dataRow.at(reproVar["TypeVar"]).split(":").size() > 1) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproVar["TypeVar"]).split(":").at(1)));
                        countLine = -1;
                        tempPosition.clear();
                        tempMap.clear();
                        if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                            urgentStop();
                            return;
                        }else{
                            QTextStream tempStream(tempFile);
                            line = tempStream.readLine();
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                urgentStop();
                                return;
                            }else{
                                line = "";
                                while (!tempStream.atEnd()  && line != "###")
                                {
                                    line = tempStream.readLine();
                                    tempPosition.insert(line.split("\t").at(0), countLine);
                                    countLine++;
                                }
                                while(!tempStream.atEnd())
                                {
                                    dataRow = tempStream.readLine().split("\t");
                                    tempId = dataRow.at(0).toInt();
                                    dataRow.removeFirst();
                                    tempMap.insert(tempId, dataRow);
                                }
                            }
                        }
                        tempFile->close();
                        if (varMap[Id].size() > reproVar["Name"])
                        {
                            factorPosition.insert(varMap[Id].at(reproVar["Name"]), tempPosition);
                            factorMap.insert(varMap[Id].at(reproVar["Name"]), tempMap);
                        }
                    }
                }
            }
        }
        // then reproduction variables
        QFile reproVarFile("bin/dictionary/reproductionVariables");
        countLine = -1;
        QMap<QString, int> reproreproVar;
        QMap<int, QStringList> reproVarMap;
        if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
            urgentStop();
            return;
        }else{
            QTextStream reproVarStream(&reproVarFile);
            line = reproVarStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!reproVarStream.atEnd()  && line != "###")
                {
                    line = reproVarStream.readLine();
                    reproreproVar.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!reproVarStream.atEnd())
                {
                    dataRow = reproVarStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    reproVarMap.insert(Id, dataRow);
                    if (reproVarMap[Id].size() > reproreproVar["TypeVar"])
                    {
                        if (reproVarMap[Id].at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                        {
                            // factor variable
                            if (dataRow.at(reproreproVar["TypeVar"]).split(":").size() > 1) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproreproVar["TypeVar"]).split(":").at(1)));
                            countLine = -1;
                            tempPosition.clear();
                            tempMap.clear();
                            if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                urgentStop();
                                return;
                            }else{
                                QTextStream tempStream(tempFile);
                                line = tempStream.readLine();
                                if (line != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                    urgentStop();
                                    return;
                                }else{
                                    line = "";
                                    while (!tempStream.atEnd()  && line != "###")
                                    {
                                        line = tempStream.readLine();
                                        tempPosition.insert(line.split("\t").at(0), countLine);
                                        countLine++;
                                    }
                                    while(!tempStream.atEnd())
                                    {
                                        dataRow = tempStream.readLine().split("\t");
                                        tempId = dataRow.at(0).toInt();
                                        dataRow.removeFirst();
                                        tempMap.insert(tempId, dataRow);
                                    }
                                }
                            }
                            tempFile->close();
                            if (reproVarMap[Id].size() > reproreproVar["Name"])
                            {
                                factorPosition.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempPosition);
                                factorMap.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempMap);
                            }
                        }
                    }
                }
                // then reproduction
                QFile reproListFile("bin/list4");
                countLine = -1;
                QMap<QString, int> reproListVar;
                QMap<int, QPair<int, QList<int> > > reproListMap;
                int maxIdRepro;
                QList<QString> reproList;
                if (!reproListFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
                    urgentStop();
                    return;
                }else{
                    QTextStream reproListStream(&reproListFile);
                    line = reproListStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list4\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = "";
                        while (!reproListStream.atEnd()  && line != "###")
                        {
                            line = reproListStream.readLine();
                            reproListVar.insert(line.split("\t").at(0), countLine);
                            reproList.append(line.split("\t").at(0));
                            countLine++;
                        }
                        while(!reproListStream.atEnd())
                        {
                            dataRow = reproListStream.readLine().split("\t");
                            Id = dataRow.at(0).toInt();
                            dataRow.removeFirst();
                            if (dataRow.size() > reproListVar["Id3"]) GUId = dataRow.at(reproListVar["Id3"]).toInt();
                            if (!reproListMap.contains(GUId))
                            {
                                reproListMap.insert(GUId, *(new QPair<int, QList<int> >));
                                reproListMap[GUId].first = -1;
                            }
                            if (dataRow.size() > reproListVar["IdSex"])
                            {
                                if (dataRow.at(reproListVar["IdSex"]) == "1")
                                {
                                    reproListMap[GUId].first = Id;
                                }else{
                                    reproListMap[GUId].second.append(Id);
                                }
                            }
                            maxIdRepro = Id;
                        }
                        // then measures
                        QFile measureFile("bin/measure");
                        countLine = -1;
                        QList<QString> measureList;
                        QList<int> UCsaved;
                        if (!measureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/measure.");
                            urgentStop();
                            return;
                        }else{
                            QTextStream measureStream(&measureFile);
                            line = measureStream.readLine();
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/measure\nMauvaise entête (\"###\" attendu)");
                                urgentStop();
                                return;
                            }else{
                                line = "";
                                while (!measureStream.atEnd()  && line != "###")
                                {
                                    line = measureStream.readLine();
                                    measureList.append(line.split("\t").at(0));
                                    countLine++;
                                }
                                while(!measureStream.atEnd())
                                {
                                    dataRow = measureStream.readLine().split("\t");
                                    if (dataRow.at(measureList.indexOf("DateM")) == measureDate.toString("yyyy.MM.dd") && dataRow.size() > measureList.indexOf("Id3"))
                                    {
                                        UCsaved.append(dataRow.at(measureList.indexOf("Id3")).toInt());
                                    }
                                    Id = dataRow.at(0).toInt();
                                    nMeasure = Id;
                                }
                                // then reproduction measures
                                QFile reproMeasureFile("bin/reproductionMeasure");
                                countLine = -1;
                                QList<QString> reproMeasureList;
                                QList<int> reproSaved;
                                if (!reproMeasureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                    urgentStop();
                                    return;
                                }else{
                                    QTextStream reproMeasureStream(&reproMeasureFile);
                                    line = reproMeasureStream.readLine();
                                    if (line != "###")
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/reproductionMeasure\nMauvaise entête (\"###\" attendu)");
                                        urgentStop();
                                        return;
                                    }else{
                                        line = "";
                                        while (!reproMeasureStream.atEnd()  && line != "###")
                                        {
                                            line = reproMeasureStream.readLine();
                                            reproMeasureList.append(line.split("\t").at(0));
                                            countLine++;
                                        }
                                        while(!reproMeasureStream.atEnd())
                                        {
                                            dataRow = reproMeasureStream.readLine().split("\t");
                                            if (dataRow.size() > reproMeasureList.indexOf("DateRM"))
                                            {
                                                if (dataRow.at(reproMeasureList.indexOf("DateRM")) == measureDate.toString("yyyy.MM.dd") && dataRow.size() > reproMeasureList.indexOf("Id4"))
                                                {
                                                    reproSaved.append(dataRow.at(reproMeasureList.indexOf("Id4")).toInt());
                                                }
                                            }
                                            Id = dataRow.at(0).toInt();
                                            nReproMeasure = Id;
                                        }
                                        // we will browse the table and see if there are records (green or red cells)
                                        bool records(false), alert(false);
                                        QTableWidget *dataTab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(11, 0)->widget());
                                        QSpinBox *intEdit;
                                        QDoubleSpinBox *doubleEdit;
                                        QLineEdit *textEdit;
                                        QComboBox *factorEdit;
                                        QString style;
                                        for (int row(0); row < dataTab->rowCount(); row++)
                                        {
                                            int col(0);
                                            for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                            {
                                                if (var->size() > reproVar["AppearingVar"] && var->size() > reproVar["TypeVar"])
                                                {
                                                    if (var->at(reproVar["AppearingVar"]) == "Y")
                                                    {
                                                        col++;
                                                        if (var->at(reproVar["TypeVar"]) == "INTEGER")
                                                        {
                                                            intEdit = qobject_cast<QSpinBox *>(dataTab->cellWidget(row, col));
                                                            style = intEdit->styleSheet();
                                                            if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                            {
                                                                records = true;
                                                                if (style.contains("background-color: salmon")) alert = true;
                                                            }
                                                        }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                                        {
                                                            doubleEdit = qobject_cast<QDoubleSpinBox *>(dataTab->cellWidget(row, col));
                                                            style = doubleEdit->styleSheet();
                                                            if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                            {
                                                                records = true;
                                                                if (style.contains("background-color: salmon")) alert = true;
                                                            }
                                                        }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                                        {
                                                            textEdit = qobject_cast<QLineEdit *>(dataTab->cellWidget(row, col));
                                                            if (textEdit->text() != "")
                                                            {
                                                                records = true;
                                                            }
                                                        }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                                        {
                                                            factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                            style = factorEdit->styleSheet();
                                                            if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                            {
                                                                records = true;
                                                                if (style.contains("background-color: salmon")) alert = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                            {
                                                if (var->size() > reproreproVar["AppearingVar"] && var->size() > reproreproVar["Target"] && var->size() > reproreproVar["TypeVar"])
                                                {
                                                    if ((var->at(reproreproVar["AppearingVar"]) == "Y") && ((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Male")))
                                                    {
                                                        col++;
                                                        if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                        {
                                                            intEdit = qobject_cast<QSpinBox *>(dataTab->cellWidget(row, col));
                                                            style = intEdit->styleSheet();
                                                            if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                            {
                                                                records = true;
                                                                if (style.contains("background-color: salmon")) alert = true;
                                                            }
                                                        }else if (var->at(reproreproVar["TypeVar"]) == "DOUBLE")
                                                        {
                                                            doubleEdit = qobject_cast<QDoubleSpinBox *>(dataTab->cellWidget(row, col));
                                                            style = doubleEdit->styleSheet();
                                                            if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                            {
                                                                records = true;
                                                                if (style.contains("background-color: salmon")) alert = true;
                                                            }
                                                        }else if (var->at(reproreproVar["TypeVar"]) == "STRING")
                                                        {
                                                            textEdit = qobject_cast<QLineEdit *>(dataTab->cellWidget(row, col));
                                                            if (textEdit->text() != "")
                                                            {
                                                                records = true;
                                                            }
                                                        }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                                        {
                                                            factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                            style = factorEdit->styleSheet();
                                                            if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                            {
                                                                records = true;
                                                                if (style.contains("background-color: salmon")) alert = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            while (col != (dataTab->columnCount() - 1))
                                            {
                                                nVarFem = 0;
                                                for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                                {
                                                    if (var->size() > reproreproVar["AppearingVar"] && var->size() > reproreproVar["Target"] && var->size() > reproreproVar["TypeVar"])
                                                    {
                                                        if ((var->at(reproreproVar["AppearingVar"]) == "Y") && ((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")))
                                                        {
                                                            nVarFem++;
                                                            col++;
                                                            if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                            {
                                                                intEdit = qobject_cast<QSpinBox *>(dataTab->cellWidget(row, col));
                                                                style = intEdit->styleSheet();
                                                                if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                {
                                                                    records = true;
                                                                    if (style.contains("background-color: salmon")) alert = true;
                                                                }
                                                            }else if (var->at(reproreproVar["TypeVar"]) == "DOUBLE")
                                                            {
                                                                doubleEdit = qobject_cast<QDoubleSpinBox *>(dataTab->cellWidget(row, col));
                                                                style = doubleEdit->styleSheet();
                                                                if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                {
                                                                    records = true;
                                                                    if (style.contains("background-color: salmon")) alert = true;
                                                                }
                                                            }else if (var->at(reproreproVar["TypeVar"]) == "STRING")
                                                            {
                                                                textEdit = qobject_cast<QLineEdit *>(dataTab->cellWidget(row, col));
                                                                if (textEdit->text() != "")
                                                                {
                                                                    records = true;
                                                                }
                                                            }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                                            {
                                                                factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                                style = factorEdit->styleSheet();
                                                                if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                {
                                                                    records = true;
                                                                    if (style.contains("background-color: salmon")) alert = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (records)
                                        {
                                            int ASL, ASF;
                                            ASL = dataTab->item(0, 0)->text().split(" . ").at(0).toInt();
                                            if (alert)
                                            {
                                                QMessageBox::critical(NULL, "Attention!", "J'ai repéré quelques erreurs (les cellules en rouge)!");
                                            }
                                            if (QMessageBox::information(NULL, "Confirmation", "Merci de confirmer l'enregistrement", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                                            {
                                                // We first register new GUs, we save new GUs name as well, and new reproductive parts, we also check if there is new factor codes
                                                for (int row(0); row < dataTab->rowCount(); row++)
                                                {
                                                    ASF = dataTab->item(row, 0)->text().split(" . ").at(0).toInt();
                                                    // we check if the GU is new
                                                    if (dataTab->item(row, 0)->font().italic())
                                                    {
                                                        // we add the GU to the table
                                                        QFile GUFile("bin/list3");
                                                        QList<QString> GUPosition;
                                                        GUPosition.clear();
                                                        QString line, cat;
                                                        int GUCode;
                                                        if (!GUFile.open(QIODevice::ReadWrite | QIODevice::Text))
                                                        {
                                                            // Error
                                                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list1.");
                                                            urgentStop();
                                                            return;
                                                        }else{
                                                            QTextStream GUStream(&GUFile);
                                                            line = GUStream.readLine();
                                                            if (line != "###")
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list1\nMauvaise entête (\"###\" attendu)");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                line = GUStream.readLine();
                                                                while (!GUStream.atEnd()  && line != "###")
                                                                {
                                                                    cat = line.split("\t").at(0);
                                                                    GUPosition.append(cat);
                                                                    line = GUStream.readLine();
                                                                }
                                                                while(!GUStream.atEnd())
                                                                {
                                                                    GUCode = GUStream.readLine().split("\t").at(0).toInt();
                                                                }
                                                                GUCode++;
                                                                for (QList<QString>::iterator it(GUPosition.begin()); it != GUPosition.end(); it++)
                                                                {
                                                                    //QMessageBox::information(NULL, "", *it);
                                                                    if (*it == "Id3")
                                                                    {
                                                                        GUStream << GUCode;
                                                                    }else if (*it == "Id2")
                                                                    {
                                                                        if (userData.toList().size() > 3) GUStream << userData.toList().at(3).toInt();
                                                                    }else if (*it == "Year")
                                                                    {
                                                                        if (dataTab->item(row, 0)->text().split(" . ").at(0) == "----")
                                                                        {
                                                                            GUStream << 0;
                                                                        }else{
                                                                            GUStream << dataTab->item(row, 0)->text().split(" . ").at(0);
                                                                        }
                                                                    }else if (*it == "NGU")
                                                                    {
                                                                        if (dataTab->item(row, 0)->text().split(" . ").size() > 1) GUStream << dataTab->item(row, 0)->text().split(" . ").at(1);
                                                                    }
                                                                    if ((*it != "") && (*it != cat))
                                                                    {
                                                                        GUStream << "\t";
                                                                    }
                                                                }
                                                                GUStream << "\n";
                                                                dataTab->item(row, 0)->setData(Qt::UserRole, QVariant(GUCode));
                                                            }
                                                        }
                                                        GUFile.close();
                                                    }
                                                    // we check if the GU changes name
                                                    if (dataTab->item(row, 0)->font().bold() && !dataTab->item(row, 0)->font().italic())
                                                    {
                                                        // we modifies the GU name
                                                        QFile GUFile("bin/list3");
                                                        QList<QString> GUPosition;
                                                        GUPosition.clear();
                                                        QString line, cat;
                                                        int GUCode;
                                                        QString s;
                                                        s.clear();
                                                        if (!GUFile.open(QIODevice::ReadWrite | QIODevice::Text))
                                                        {
                                                            // Error
                                                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list1.");
                                                            urgentStop();
                                                            return;
                                                        }else{
                                                            QTextStream GUStream(&GUFile);
                                                            line = GUStream.readLine();
                                                            s.append(line + "\n");
                                                            if (line != "###")
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list1\nMauvaise entête (\"###\" attendu)");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                line = GUStream.readLine();
                                                                s.append(line + "\n");
                                                                while (!GUStream.atEnd()  && line != "###")
                                                                {
                                                                    cat = line.split("\t").at(0);
                                                                    GUPosition.append(cat);
                                                                    line = GUStream.readLine();
                                                                    s.append(line + "\n");
                                                                }
                                                                while(!GUStream.atEnd())
                                                                {
                                                                    line = GUStream.readLine();
                                                                    GUCode = line.split("\t").at(0).toInt();
                                                                    if (GUCode == dataTab->item(row, 0)->data(Qt::UserRole).toInt())
                                                                    {
                                                                        line = "";
                                                                        for (QList<QString>::iterator it(GUPosition.begin()); it != GUPosition.end(); it++)
                                                                        {
                                                                            //QMessageBox::information(NULL, "", *it);
                                                                            if (*it == "Id3")
                                                                            {
                                                                                line += QString::number(GUCode);
                                                                            }else if (*it == "Id2")
                                                                            {
                                                                                if (userData.toList().size() > 3) line += QString::number(userData.toList().at(3).toInt());
                                                                            }else if (*it == "Year")
                                                                            {
                                                                                if (dataTab->item(row, 0)->text().split(" . ").at(0) == "----")
                                                                                {
                                                                                    line += "0";
                                                                                }else{
                                                                                    line += dataTab->item(row, 0)->text().split(" . ").at(0);
                                                                                }
                                                                            }else if (*it == "NGU")
                                                                            {
                                                                                if (dataTab->item(row, 0)->text().split(" . ").size() > 1) line += dataTab->item(row, 0)->text().split(" . ").at(1);
                                                                            }
                                                                            if ((*it != "") && (*it != cat))
                                                                            {
                                                                                line += "\t";
                                                                            }
                                                                        }
                                                                    }
                                                                    s.append(line + "\n");
                                                                }
                                                                GUFile.resize(0);
                                                                GUStream << s;
                                                            }
                                                        }
                                                        GUFile.close();
                                                    }
                                                    // we browse each data to see if there are new factor codes
                                                    int col(0);
                                                    QString style;
                                                    QComboBox *factorEdit;
                                                    for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                                    {
                                                        if (var->size() > reproVar["AppearingVar"] && var->size() > reproVar["TypeVar"])
                                                        {
                                                            if (var->at(reproVar["AppearingVar"]) == "Y")
                                                            {
                                                                col++;
                                                                if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                                                {
                                                                    factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                                    style = factorEdit->styleSheet();
                                                                    if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                    {
                                                                        if (factorEdit->currentText() != factorEdit->itemText(factorEdit->currentIndex()))
                                                                        {
                                                                            // we check if this factor wasn't add before
                                                                            bool test(false);
                                                                            if (var->size() > reproVar["Name"])
                                                                            {
                                                                                QMap<int, QStringList>::iterator it(factorMap[var->at(reproVar["Name"])].begin());
                                                                                int Id;
                                                                                while (!test && it!=factorMap[var->at(reproVar["Name"])].end())
                                                                                {
                                                                                    if (it->at(factorPosition[var->at(reproVar["Name"])]["Code"]) == factorEdit->currentText())
                                                                                    {
                                                                                        test = true;
                                                                                    }
                                                                                    Id = it->at(0).toInt();
                                                                                    it++;
                                                                                }
                                                                                if (!test)
                                                                                {
                                                                                    if (var->size() > reproVar["LongName"])
                                                                                    {
                                                                                        if (QMessageBox::information(NULL, "Nouvelle catégorie", "J'ai repéré une nouvelle catégorie pour la variable " + var->at(reproVar["LongName"]) + "\nCode : " + factorEdit->currentText() + "\nMerci de me confirmer l'ajout de cette catégorie.", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                                                                                        {
                                                                                            // we add the category
                                                                                            Id++;
                                                                                            QString ExplainFactor;
                                                                                            ExplainFactor = QInputDialog::getText(NULL, "Explicitation du code", "Merci de m'expliciter le code '" + factorEdit->currentText() + "' pour la variable " + var->at(reproVar["LongName"]) + " (qui apparaîtra à l'aide').");
                                                                                            while (ExplainFactor == "")
                                                                                            {
                                                                                                ExplainFactor = QInputDialog::getText(NULL, "Explicitation du code", "Il faut vraiment m'expliciter le code '" + factorEdit->currentText() + "' pour la variable " + var->at(reproVar["LongName"]) + " (qui apparaîtra à l'aide').");
                                                                                            }
                                                                                            if (var->at(reproVar["TypeVar"]).split(":").size() > 1)
                                                                                            {
                                                                                                tempFile = new QFile(QString("bin/%1").arg(var->at(reproVar["TypeVar"]).split(":").at(1)));
                                                                                                if (!tempFile->open(QIODevice::ReadWrite | QIODevice::Text))
                                                                                                {
                                                                                                    // Error
                                                                                                    QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                                                                                    urgentStop();
                                                                                                    return;
                                                                                                }else{
                                                                                                    QTextStream tempStream(tempFile);
                                                                                                    line = tempStream.readLine();
                                                                                                    line = "";
                                                                                                    QList<QString> tempPosition;
                                                                                                    tempPosition.clear();
                                                                                                    int tempCode;
                                                                                                    while (line != "###")
                                                                                                    {
                                                                                                        line = tempStream.readLine();
                                                                                                        tempPosition.append(line.split("\t").at(0));
                                                                                                    }
                                                                                                    while(!tempStream.atEnd())
                                                                                                    {
                                                                                                        line = tempStream.readLine();
                                                                                                    }
                                                                                                    tempCode = line.split("\t").at(0).toInt() + 1;
                                                                                                    for (QList<QString>::iterator it(tempPosition.begin()); it != tempPosition.end(); it++)
                                                                                                    {
                                                                                                        //QMessageBox::information(NULL, "", *it);
                                                                                                        if (*it == var->at(reproVar["Name"]))
                                                                                                        {
                                                                                                            tempStream << tempCode;
                                                                                                        }else if (*it == "Code")
                                                                                                        {
                                                                                                            tempStream << factorEdit->currentText();
                                                                                                        }else if (*it == "Explain")
                                                                                                        {
                                                                                                            tempStream << ExplainFactor;
                                                                                                        }
                                                                                                        if ((*it != "") && (!(*it).startsWith("###")))
                                                                                                        {
                                                                                                            tempStream << "\t";
                                                                                                        }
                                                                                                    }
                                                                                                    tempStream << "\n";
                                                                                                    // we add to the list of the factorEdit, and eventually to the following factorEdits
                                                                                                    factorEdit->addItem(factorEdit->currentText(), QVariant(tempCode));
                                                                                                    factorEdit->setCurrentIndex(factorEdit->count() - 1);
                                                                                                    QComboBox *factorEditTemp;
                                                                                                    for (int r(row + 1); r < dataTab->rowCount(); r++)
                                                                                                    {
                                                                                                        factorEditTemp = qobject_cast<QComboBox *>(dataTab->cellWidget(r, col));
                                                                                                        if (factorEditTemp->currentText() == factorEdit->currentText())
                                                                                                        {
                                                                                                            factorEditTemp->addItem(factorEdit->currentText(), QVariant(tempCode));
                                                                                                            factorEditTemp->setCurrentIndex(factorEditTemp->count() - 1);
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                tempFile->close();
                                                                                            }
                                                                                        }else{
                                                                                            // we don't add it
                                                                                            factorEdit->setCurrentIndex(-1);
                                                                                            factorEdit->setEditText("");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                                    {
                                                        if (var->size() > reproreproVar["AppearingVar"] && var->size() > reproreproVar["Target"] && var->size() > reproreproVar["TypeVar"])
                                                        {
                                                            if ((var->at(reproreproVar["AppearingVar"]) == "Y") && ((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Male")))
                                                            {
                                                                col++;
                                                                if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                                                {
                                                                    factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                                    style = factorEdit->styleSheet();
                                                                    if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                    {
                                                                        if (factorEdit->currentText() != factorEdit->itemText(factorEdit->currentIndex()))
                                                                        {
                                                                            // we check if this factor wasn't add before
                                                                            bool test(false);
                                                                            if (var->size() > reproreproVar["Name"])
                                                                            {
                                                                                QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin());
                                                                                int Id;
                                                                                while (!test && it!=factorMap[var->at(reproreproVar["Name"])].end())
                                                                                {
                                                                                    if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"])
                                                                                    {
                                                                                        if (it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]) == factorEdit->currentText())
                                                                                        {
                                                                                            test = true;
                                                                                        }
                                                                                    }
                                                                                    Id = it->at(0).toInt();
                                                                                    it++;
                                                                                }
                                                                                if (!test)
                                                                                {
                                                                                    QString ExplainFactor;
                                                                                    if (var->size() > reproreproVar["LongName"])
                                                                                    {
                                                                                        ExplainFactor = var->at(reproreproVar["LongName"]);
                                                                                        ExplainFactor.replace("#", "Male");
                                                                                        if (QMessageBox::information(NULL, "Nouvelle catégorie", "J'ai repéré une nouvelle catégorie pour la variable " + ExplainFactor + "\nCode : " + factorEdit->currentText() + "\nMerci de me confirmer l'ajout de cette catégorie.", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                                                                                        {
                                                                                            // we add the category
                                                                                            Id++;
                                                                                            QString ExplainFactor;
                                                                                            ExplainFactor = var->at(reproreproVar["LongName"]);
                                                                                            ExplainFactor.replace("#", "Male");
                                                                                            ExplainFactor = QInputDialog::getText(NULL, "Explicitation du code", "Merci de m'expliciter le code '" + factorEdit->currentText() + "' pour la variable " + ExplainFactor + " (qui apparaîtra à l'aide').");
                                                                                            while (ExplainFactor == "")
                                                                                            {
                                                                                                ExplainFactor = var->at(reproreproVar["LongName"]);
                                                                                                ExplainFactor.replace("#", "Male");
                                                                                                ExplainFactor = QInputDialog::getText(NULL, "Explicitation du code", "Il faut vraiment m'expliciter le code '" + factorEdit->currentText() + "' pour la variable " + ExplainFactor + " (qui apparaîtra à l'aide').");
                                                                                            }
                                                                                            if (var->at(reproreproVar["TypeVar"]).split(":").size() > 1)
                                                                                            {
                                                                                                tempFile = new QFile(QString("bin/%1").arg(var->at(reproreproVar["TypeVar"]).split(":").at(1)));
                                                                                                if (!tempFile->open(QIODevice::ReadWrite | QIODevice::Text))
                                                                                                {
                                                                                                    // Error
                                                                                                    QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                                                                                    urgentStop();
                                                                                                    return;
                                                                                                }else{
                                                                                                    QTextStream tempStream(tempFile);
                                                                                                    line = tempStream.readLine();
                                                                                                    line = "";
                                                                                                    QList<QString> tempPosition;
                                                                                                    tempPosition.clear();
                                                                                                    int tempCode;
                                                                                                    while (line != "###")
                                                                                                    {
                                                                                                        line = tempStream.readLine();
                                                                                                        tempPosition.append(line.split("\t").at(0));
                                                                                                    }
                                                                                                    while(!tempStream.atEnd())
                                                                                                    {
                                                                                                        line = tempStream.readLine();
                                                                                                    }
                                                                                                    tempCode = line.split("\t").at(0).toInt() + 1;
                                                                                                    for (QList<QString>::iterator it(tempPosition.begin()); it != tempPosition.end(); it++)
                                                                                                    {
                                                                                                        //QMessageBox::information(NULL, "", *it);
                                                                                                        if (var->size() > reproreproVar["Name"])
                                                                                                        {
                                                                                                            if (*it == var->at(reproreproVar["Name"]))
                                                                                                            {
                                                                                                                tempStream << tempCode;
                                                                                                            }else if (*it == "IdSex")
                                                                                                            {
                                                                                                                tempStream << "1";
                                                                                                            }else if (*it == "Code")
                                                                                                            {
                                                                                                                tempStream << factorEdit->currentText();
                                                                                                            }else if (*it == "Explain")
                                                                                                            {
                                                                                                                tempStream << ExplainFactor;
                                                                                                            }
                                                                                                        }
                                                                                                        if ((*it != "") && (!(*it).startsWith("###")))
                                                                                                        {
                                                                                                            tempStream << "\t";
                                                                                                        }
                                                                                                    }
                                                                                                    tempStream << "\n";
                                                                                                    // we add to the list of the factorEdit, and eventually to the following factorEdits
                                                                                                    factorEdit->addItem(factorEdit->currentText(), QVariant(tempCode));
                                                                                                    factorEdit->setCurrentIndex(factorEdit->count() - 1);
                                                                                                    QComboBox *factorEditTemp;
                                                                                                    for (int c(col + nVarFem); c < dataTab->columnCount(); c += nVarFem)
                                                                                                    {
                                                                                                        for (int r(row + 1); r < dataTab->rowCount(); r++)
                                                                                                        {
                                                                                                            factorEditTemp = qobject_cast<QComboBox *>(dataTab->cellWidget(r, c));
                                                                                                            if (factorEditTemp->currentText() == factorEdit->currentText())
                                                                                                            {
                                                                                                                factorEditTemp->addItem(factorEdit->currentText(), QVariant(tempCode));
                                                                                                                factorEditTemp->setCurrentIndex(factorEditTemp->count() - 1);
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                tempFile->close();
                                                                                            }
                                                                                        }else{
                                                                                            // we don't add it
                                                                                            factorEdit->setCurrentIndex(-1);
                                                                                            factorEdit->setEditText("");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    while (col != (dataTab->columnCount() - 1))
                                                    {
                                                        for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                                        {
                                                            if (var->size() > reproreproVar["AppearingVar"] && var->size() > reproreproVar["Target"] && var->size() > reproreproVar["TypeVar"] && var->size() > reproreproVar["Name"])
                                                            {
                                                                if ((var->at(reproreproVar["AppearingVar"]) == "Y") && ((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")))
                                                                {
                                                                    col++;
                                                                    if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                                                    {
                                                                        factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                                        style = factorEdit->styleSheet();
                                                                        if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                        {
                                                                            if (factorEdit->currentText() != factorEdit->itemText(factorEdit->currentIndex()))
                                                                            {
                                                                                // we check if this factor wasn't add before
                                                                                bool test(false);
                                                                                QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin());
                                                                                int Id;
                                                                                while (!test && it!=factorMap[var->at(reproreproVar["Name"])].end())
                                                                                {
                                                                                    if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"])
                                                                                    {
                                                                                        if (it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]) == factorEdit->currentText())
                                                                                        {
                                                                                            test = true;
                                                                                        }
                                                                                    }
                                                                                    Id = it->at(0).toInt();
                                                                                    it++;
                                                                                }
                                                                                if (!test)
                                                                                {
                                                                                    QString ExplainFactor;
                                                                                    if (var->size() > reproreproVar["LongName"])
                                                                                    {
                                                                                        ExplainFactor = var->at(reproreproVar["LongName"]);
                                                                                        ExplainFactor.replace("#", "Femelle");
                                                                                        if (QMessageBox::information(NULL, "Nouvelle catégorie", "J'ai repéré une nouvelle catégorie pour la variable " + ExplainFactor + "\nCode : " + factorEdit->currentText() + "\nMerci de me confirmer l'ajout de cette catégorie.", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                                                                                        {
                                                                                            // we add the category
                                                                                            Id++;
                                                                                            QString ExplainFactor;
                                                                                            ExplainFactor = var->at(reproreproVar["LongName"]);
                                                                                            ExplainFactor.replace("#", "Femelle");
                                                                                            ExplainFactor = QInputDialog::getText(NULL, "Explicitation du code", "Merci de m'expliciter le code '" + factorEdit->currentText() + "' pour la variable " + ExplainFactor + " (qui apparaîtra à l'aide').");
                                                                                            while (ExplainFactor == "")
                                                                                            {
                                                                                                ExplainFactor = var->at(reproreproVar["LongName"]);
                                                                                                ExplainFactor.replace("#", "Femelle");
                                                                                                ExplainFactor = QInputDialog::getText(NULL, "Explicitation du code", "Il faut vraiment m'expliciter le code '" + factorEdit->currentText() + "' pour la variable " + ExplainFactor + " (qui apparaîtra à l'aide').");
                                                                                            }
                                                                                            if (var->at(reproreproVar["TypeVar"]).split(":").size() > 1)
                                                                                            {
                                                                                                tempFile = new QFile(QString("bin/%1").arg(var->at(reproreproVar["TypeVar"]).split(":").at(1)));
                                                                                                if (!tempFile->open(QIODevice::ReadWrite | QIODevice::Text))
                                                                                                {
                                                                                                    // Error
                                                                                                    QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                                                                                    urgentStop();
                                                                                                    return;
                                                                                                }else{
                                                                                                    QTextStream tempStream(tempFile);
                                                                                                    line = tempStream.readLine();
                                                                                                    line = "";
                                                                                                    QList<QString> tempPosition;
                                                                                                    tempPosition.clear();
                                                                                                    int tempCode;
                                                                                                    while (line != "###")
                                                                                                    {
                                                                                                        line = tempStream.readLine();
                                                                                                        tempPosition.append(line.split("\t").at(0));
                                                                                                    }
                                                                                                    while(!tempStream.atEnd())
                                                                                                    {
                                                                                                        line = tempStream.readLine();
                                                                                                    }
                                                                                                    tempCode = line.split("\t").at(0).toInt() + 1;
                                                                                                    for (QList<QString>::iterator it(tempPosition.begin()); it != tempPosition.end(); it++)
                                                                                                    {
                                                                                                        //QMessageBox::information(NULL, "", *it);
                                                                                                        if (*it == var->at(reproreproVar["Name"]))
                                                                                                        {
                                                                                                            tempStream << tempCode;
                                                                                                        }else if (*it == "IdSex")
                                                                                                        {
                                                                                                            tempStream << "2";
                                                                                                        }else if (*it == "Code")
                                                                                                        {
                                                                                                            tempStream << factorEdit->currentText();
                                                                                                        }else if (*it == "Explain")
                                                                                                        {
                                                                                                            tempStream << ExplainFactor;
                                                                                                        }
                                                                                                        if ((*it != "") && (!(*it).startsWith("###")))
                                                                                                        {
                                                                                                            tempStream << "\t";
                                                                                                        }
                                                                                                    }
                                                                                                    tempStream << "\n";
                                                                                                    // we add to the list of the factorEdit, and eventually to the following factorEdits
                                                                                                    factorEdit->addItem(factorEdit->currentText(), QVariant(tempCode));
                                                                                                    factorEdit->setCurrentIndex(factorEdit->count() - 1);
                                                                                                    QComboBox *factorEditTemp;
                                                                                                    for (int r(row + 1); r < dataTab->rowCount(); r++)
                                                                                                    {
                                                                                                        factorEditTemp = qobject_cast<QComboBox *>(dataTab->cellWidget(r, col));
                                                                                                        if (factorEditTemp->currentText() == factorEdit->currentText())
                                                                                                        {
                                                                                                            factorEditTemp->addItem(factorEdit->currentText(), QVariant(tempCode));
                                                                                                            factorEditTemp->setCurrentIndex(factorEditTemp->count() - 1);
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                tempFile->close();
                                                                                            }
                                                                                        }else{
                                                                                            // we don't add it
                                                                                            factorEdit->setCurrentIndex(-1);
                                                                                            factorEdit->setEditText("");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                // we browse reproduction and see if reproduction was set and add it if it contains value
                                                QSpinBox *intEdit;
                                                QDoubleSpinBox *doubleEdit;
                                                QLineEdit *textEdit;
                                                //QComboBox *factorEdit;
                                                QTableWidgetItem *cell;
                                                //QString style;
                                                bool records(false);
                                                QMap<QString, QString> recordMap;
                                                int col(0);
                                                for (int row(0); row < dataTab->rowCount(); row++)
                                                {
                                                    GUId = dataTab->item(row, 0)->data(Qt::UserRole).toInt();
                                                    col = 0;
                                                    recordMap.clear();
                                                    records = false;
                                                    for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                                    {
                                                        if (var->size() > reproVar["AppearingVar"] && var->size() > reproVar["Name"])
                                                        {
                                                            if (var->at(reproVar["AppearingVar"]) == "Y")
                                                            {
                                                                col++;
                                                                if (var->at(reproVar["TypeVar"]) == "INTEGER")
                                                                {
                                                                    intEdit = qobject_cast<QSpinBox *>(dataTab->cellWidget(row, col));
                                                                    style = intEdit->styleSheet();
                                                                    if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                    {
                                                                        recordMap.insert(var->at(reproVar["Name"]), QString::number(intEdit->value()));
                                                                        records = true;
                                                                    }
                                                                }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                                                {
                                                                    doubleEdit = qobject_cast<QDoubleSpinBox *>(dataTab->cellWidget(row, col));
                                                                    style = doubleEdit->styleSheet();
                                                                    if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                    {
                                                                        records = true;
                                                                        recordMap.insert(var->at(reproVar["Name"]), QString::number(doubleEdit->value()));
                                                                    }
                                                                }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                                                {
                                                                    textEdit = qobject_cast<QLineEdit *>(dataTab->cellWidget(row, col));
                                                                    if (textEdit->text() != "")
                                                                    {
                                                                        records = true;
                                                                        recordMap.insert(var->at(reproVar["Name"]), textEdit->text());
                                                                    }
                                                                }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                                                {
                                                                    factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                                    style = factorEdit->styleSheet();
                                                                    if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                    {
                                                                        records = true;
                                                                        recordMap.insert(var->at(reproVar["Name"]), QString::number(factorEdit->itemData(factorEdit->currentIndex()).toInt()));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (records)
                                                    {
                                                        // we add the measure to the table
                                                        if (UCsaved.contains(GUId))
                                                        {
                                                            if (QMessageBox::warning(NULL, "Enregistrement existant", "Un enregistrement existe déjà pour cette date pour l'UC " + dataTab->item(row, 0)->text() + "\n Remplacé-je l'ancien enregistrement par celui-ci?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                                                            {
                                                                QFile measureFile("bin/measure");
                                                                QString line, s;
                                                                if (!measureFile.open(QIODevice::ReadWrite | QIODevice::Text))
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/measure.");
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    QTextStream measureStream(&measureFile);
                                                                    nMeasure++;
                                                                    line = measureStream.readLine();
                                                                    s.append(line + "\n");
                                                                    line = measureStream.readLine();
                                                                    s.append(line + "\n");
                                                                    while (!measureStream.atEnd()  && line != "###")
                                                                    {
                                                                        line = measureStream.readLine();
                                                                        s.append(line + "\n");
                                                                    }
                                                                    while(!measureStream.atEnd())
                                                                    {
                                                                        line = measureStream.readLine();
                                                                        //QMessageBox::information(NULL, "", line);
                                                                        if (line.split("\t").size() > measureList.indexOf("Id3") && line.split("\t").size() > measureList.indexOf("DateM"))
                                                                        {
                                                                            if (!((GUId == line.split("\t").at(measureList.indexOf("Id3")).toInt() && (line.split("\t").at(measureList.indexOf("DateM")) == measureDate.toString("yyyy.MM.dd")))))
                                                                            {
                                                                                s.append(line + "\n");
                                                                            }
                                                                        }
                                                                    }
                                                                    measureFile.resize(0);
                                                                    measureStream << s;
                                                                    for (QList<QString>::iterator it(measureList.begin()); it != measureList.end(); it++)
                                                                    {
                                                                        //QMessageBox::information(NULL, "", *it);
                                                                        if (*it == "IdM")
                                                                        {
                                                                            measureStream << nMeasure;
                                                                        }else if (*it == "Id3")
                                                                        {
                                                                            measureStream << GUId;
                                                                        }else if (*it == "DateM")
                                                                        {
                                                                            measureStream << measureDate.toString("yyyy.MM.dd");
                                                                        }else if (*it == "Auteur")
                                                                        {
                                                                            if (userData.toList().size() > 1) measureStream << userData.toList().at(1).toString();
                                                                        }else if ((*it != "") && (*it != "###"))
                                                                        {
                                                                            if (recordMap.contains(*it))
                                                                            {
                                                                                measureStream << recordMap[*it];
                                                                            }
                                                                        }
                                                                        if ((*it != "") && (*it != "###"))
                                                                        {
                                                                            measureStream << "\t";
                                                                        }
                                                                    }
                                                                    measureStream << "\n";
                                                                }
                                                                measureFile.close();
                                                            }
                                                        }else{
                                                            QFile measureFile("bin/measure");
                                                            QString line;
                                                            if (!measureFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/measure.");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                QTextStream measureStream(&measureFile);
                                                                nMeasure++;
                                                                for (QList<QString>::iterator it(measureList.begin()); it != measureList.end(); it++)
                                                                {
                                                                    //QMessageBox::information(NULL, "", *it);
                                                                    if (*it == "IdM")
                                                                    {
                                                                        measureStream << nMeasure;
                                                                    }else if (*it == "Id3")
                                                                    {
                                                                        measureStream << GUId;
                                                                    }else if (*it == "DateM")
                                                                    {
                                                                        measureStream << measureDate.toString("yyyy.MM.dd");
                                                                    }else if (*it == "Auteur")
                                                                    {
                                                                        if (userData.toList().size() > 1) measureStream << userData.toList().at(1).toString();
                                                                    }else if ((*it != "") && (*it != "###"))
                                                                    {
                                                                        if (recordMap.contains(*it))
                                                                        {
                                                                            measureStream << recordMap[*it];
                                                                        }
                                                                    }
                                                                    if ((*it != "") && (*it != "###"))
                                                                    {
                                                                        measureStream << "\t";
                                                                    }
                                                                }
                                                                measureStream << "\n";
                                                            }
                                                            measureFile.close();
                                                        }
                                                    }
                                                    recordMap.clear();
                                                    records = false;
                                                    for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                                    {
                                                        if (var->size() > reproreproVar["AppearingVar"] && var->size() > reproreproVar["Target"] && var->size() > reproreproVar["TypeVar"] && var->size() > reproreproVar["Name"])
                                                        {
                                                            if ((var->at(reproreproVar["AppearingVar"]) == "Y") && ((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Male")))
                                                            {
                                                                col++;
                                                                if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                                {
                                                                    intEdit = qobject_cast<QSpinBox *>(dataTab->cellWidget(row, col));
                                                                    style = intEdit->styleSheet();
                                                                    if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                    {
                                                                        records = true;
                                                                        recordMap.insert(var->at(reproreproVar["Name"]), QString::number(intEdit->value()));
                                                                    }
                                                                }else if (var->at(reproreproVar["TypeVar"]) == "DOUBLE")
                                                                {
                                                                    doubleEdit = qobject_cast<QDoubleSpinBox *>(dataTab->cellWidget(row, col));
                                                                    style = doubleEdit->styleSheet();
                                                                    if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                    {
                                                                        records = true;
                                                                        recordMap.insert(var->at(reproreproVar["Name"]), QString::number(doubleEdit->value()));
                                                                    }
                                                                }else if (var->at(reproreproVar["TypeVar"]) == "STRING")
                                                                {
                                                                    textEdit = qobject_cast<QLineEdit *>(dataTab->cellWidget(row, col));
                                                                    if (textEdit->text() != "")
                                                                    {
                                                                        records = true;
                                                                        recordMap.insert(var->at(reproreproVar["Name"]), textEdit->text());
                                                                    }
                                                                }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                                                {
                                                                    factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                                    style = factorEdit->styleSheet();
                                                                    if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                    {
                                                                        records = true;
                                                                        recordMap.insert(var->at(reproreproVar["Name"]), QString::number(factorEdit->itemData(factorEdit->currentIndex()).toInt()));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (records) // there is a record for male reproduction, we have to check if the male reproduction is set
                                                    {
                                                        // check if this male reproduction is set
                                                        records = false;
                                                        if (reproListMap.contains(GUId))
                                                        {
                                                            if (reproListMap[GUId].first == -1) records = true;
                                                        }else{
                                                            reproListMap.insert(GUId, *(new QPair<int, QList<int> >));
                                                            reproListMap[GUId].first = -1;
                                                            records = true;
                                                        }
                                                        if (records)
                                                        {
                                                            // we add the reproduction to the table
                                                            QFile reproFile("bin/list4");
                                                            QString line;
                                                            if (!reproFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                QTextStream reproStream(&reproFile);
                                                                maxIdRepro++;
                                                                for (QList<QString>::iterator it(reproList.begin()); it != reproList.end(); it++)
                                                                {
                                                                    //QMessageBox::information(NULL, "", *it);
                                                                    if (*it == "Id4")
                                                                    {
                                                                        reproStream << maxIdRepro;
                                                                    }else if (*it == "Id3")
                                                                    {
                                                                        reproStream << GUId;
                                                                    }else if (*it == "IdSex")
                                                                    {
                                                                        reproStream << "1";
                                                                    }
                                                                    if ((*it != "") && (*it != "###"))
                                                                    {
                                                                        reproStream << "\t";
                                                                    }
                                                                }
                                                                reproStream << "\n";
                                                            }
                                                            reproFile.close();
                                                            reproListMap[GUId].first = maxIdRepro;
                                                        }
                                                        // we can add its measures here
                                                        if (reproSaved.contains(reproListMap[GUId].first))
                                                        {
                                                            if (QMessageBox::warning(NULL, "Enregistrement existant", "Un enregistrement existe déjà pour cette date pour la pièce reproductrice mâle'UC " + dataTab->item(row, 0)->text() + "\n Remplacé-je l'ancien enregistrement par celui-ci?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                                                            {
                                                                QFile measureFile("bin/reproductionMeasure");
                                                                QString line, s;
                                                                if (!measureFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    QTextStream measureStream(&measureFile);
                                                                    nReproMeasure++;
                                                                    line = measureStream.readLine();
                                                                    s.append(line + "\n");
                                                                    line = measureStream.readLine();
                                                                    s.append(line + "\n");
                                                                    while (!measureStream.atEnd()  && line != "###")
                                                                    {
                                                                        line = measureStream.readLine();
                                                                        s.append(line + "\n");
                                                                    }
                                                                    while(!measureStream.atEnd())
                                                                    {
                                                                        line = measureStream.readLine();
                                                                        if (line.split("\t").size() > reproMeasureList.indexOf("Id4") && line.split("\t").size() > reproMeasureList.indexOf("DateRM"))
                                                                        {
                                                                            if (!((reproListMap[GUId].first == line.split("\t").at(reproMeasureList.indexOf("Id4")).toInt() && (line.split("\t").at(reproMeasureList.indexOf("DateRM")) == measureDate.toString("yyyy.MM.dd")))))
                                                                            {
                                                                                s.append(line + "\n");
                                                                            }
                                                                        }
                                                                    }
                                                                    measureFile.resize(0);
                                                                    measureStream << s;
                                                                    for (QList<QString>::iterator it(reproMeasureList.begin()); it != reproMeasureList.end(); it++)
                                                                    {
                                                                        //QMessageBox::information(NULL, "", *it);
                                                                        if (*it == "IdRM")
                                                                        {
                                                                            measureStream << nReproMeasure;
                                                                        }else if (*it == "Id4")
                                                                        {
                                                                            measureStream << reproListMap[GUId].first;
                                                                        }else if (*it == "DateRM")
                                                                        {
                                                                            measureStream << measureDate.toString("yyyy.MM.dd");
                                                                        }else if (*it == "Auteur")
                                                                        {
                                                                            if (userData.toList().size() > 1) measureStream << userData.toList().at(1).toString();
                                                                        }else if ((*it != "") && (*it != "###"))
                                                                        {
                                                                            if (recordMap.contains(*it))
                                                                            {
                                                                                measureStream << recordMap[*it];
                                                                            }
                                                                        }
                                                                        if ((*it != "") && (*it != "###"))
                                                                        {
                                                                            measureStream << "\t";
                                                                        }
                                                                    }
                                                                    measureStream << "\n";
                                                                }
                                                                measureFile.close();
                                                            }
                                                        }else{
                                                            QFile measureFile("bin/reproductionMeasure");
                                                            QString line;
                                                            if (!measureFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                QTextStream measureStream(&measureFile);
                                                                nReproMeasure++;
                                                                for (QList<QString>::iterator it(reproMeasureList.begin()); it != reproMeasureList.end(); it++)
                                                                {
                                                                    //QMessageBox::information(NULL, "", *it);
                                                                    if (*it == "IdRM")
                                                                    {
                                                                        measureStream << nReproMeasure;
                                                                    }else if (*it == "Id4")
                                                                    {
                                                                        measureStream << reproListMap[GUId].first;
                                                                    }else if (*it == "DateRM")
                                                                    {
                                                                        measureStream << measureDate.toString("yyyy.MM.dd");
                                                                    }else if (*it == "Auteur")
                                                                    {
                                                                        if (userData.toList().size() > 1) measureStream << userData.toList().at(1).toString();
                                                                    }else if ((*it != "") && (*it != "###"))
                                                                    {
                                                                        if (recordMap.contains(*it))
                                                                        {
                                                                            measureStream << recordMap[*it];
                                                                        }
                                                                    }
                                                                    if ((*it != "") && (*it != "###"))
                                                                    {
                                                                        measureStream << "\t";
                                                                    }
                                                                }
                                                                measureStream << "\n";
                                                            }
                                                            measureFile.close();
                                                        }
                                                    }
                                                    nVarFem = 0;
                                                    while (col != (dataTab->columnCount() - 1))
                                                    {
                                                        recordMap.clear();
                                                        records = false;
                                                        nVarFem++;
                                                        for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                                        {
                                                            if (var->size() > reproreproVar["AppearingVar"] && var->size() > reproreproVar["Target"] && var->size() > reproreproVar["TypeVar"] && var->size() > reproreproVar["Name"])
                                                            {
                                                                if ((var->at(reproreproVar["AppearingVar"]) == "Y") && ((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")))
                                                                {
                                                                    col++;
                                                                    if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                                    {
                                                                        intEdit = qobject_cast<QSpinBox *>(dataTab->cellWidget(row, col));
                                                                        style = intEdit->styleSheet();
                                                                        if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                        {
                                                                            recordMap.insert(var->at(reproreproVar["Name"]), QString::number(intEdit->value()));
                                                                            records = true;
                                                                        }
                                                                    }else if (var->at(reproreproVar["TypeVar"]) == "DOUBLE")
                                                                    {
                                                                        doubleEdit = qobject_cast<QDoubleSpinBox *>(dataTab->cellWidget(row, col));
                                                                        style = doubleEdit->styleSheet();
                                                                        if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                        {
                                                                            recordMap.insert(var->at(reproreproVar["Name"]), QString::number(doubleEdit->value()));
                                                                            records = true;
                                                                        }
                                                                    }else if (var->at(reproreproVar["TypeVar"]) == "STRING")
                                                                    {
                                                                        textEdit = qobject_cast<QLineEdit *>(dataTab->cellWidget(row, col));
                                                                        if (textEdit->text() != "")
                                                                        {
                                                                            recordMap.insert(var->at(reproreproVar["Name"]), textEdit->text());
                                                                            records = true;
                                                                        }
                                                                    }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                                                    {
                                                                        factorEdit = qobject_cast<QComboBox *>(dataTab->cellWidget(row, col));
                                                                        style = factorEdit->styleSheet();
                                                                        if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                                                                        {
                                                                            recordMap.insert(var->at(reproreproVar["Name"]), QString::number(factorEdit->itemData(factorEdit->currentIndex()).toInt()));
                                                                            records = true;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (records) // there is a record for male reproduction, we have to check if the male reproduction is set
                                                        {
                                                            // check if this female reproduction is set
                                                            records = false;
                                                            if (reproListMap.contains(GUId))
                                                            {
                                                                if (reproListMap[GUId].second.size() < nVarFem) records = true;
                                                            }else{
                                                                reproListMap.insert(GUId, *(new QPair<int, QList<int> >));
                                                                reproListMap[GUId].first = -1;
                                                                records = true;
                                                                for (int i(1); i < nVarFem; i++)
                                                                {
                                                                    // we add the reproduction to the table
                                                                    QFile reproFile("bin/list4");
                                                                    QString line;
                                                                    if (!reproFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                                                                    {
                                                                        // Error
                                                                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
                                                                        urgentStop();
                                                                        return;
                                                                    }else{
                                                                        QTextStream reproStream(&reproFile);
                                                                        maxIdRepro++;
                                                                        for (QList<QString>::iterator it(reproList.begin()); it != reproList.end(); it++)
                                                                        {
                                                                            //QMessageBox::information(NULL, "", *it);
                                                                            if (*it == "Id4")
                                                                            {
                                                                                reproStream << maxIdRepro;
                                                                            }else if (*it == "Id3")
                                                                            {
                                                                                reproStream << GUId;
                                                                            }else if (*it == "IdSex")
                                                                            {
                                                                                reproStream << "2";
                                                                            }
                                                                            if ((*it != "") && (*it != "###"))
                                                                            {
                                                                                reproStream << "\t";
                                                                            }
                                                                        }
                                                                        reproStream << "\n";
                                                                    }
                                                                    reproFile.close();
                                                                    reproListMap[GUId].second.append(maxIdRepro);
                                                                }
                                                            }
                                                            if (records)
                                                            {
                                                                // we add the reproduction to the table
                                                                QFile reproFile("bin/list4");
                                                                QString line;
                                                                if (!reproFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    QTextStream reproStream(&reproFile);
                                                                    maxIdRepro++;
                                                                    for (QList<QString>::iterator it(reproList.begin()); it != reproList.end(); it++)
                                                                    {
                                                                        //QMessageBox::information(NULL, "", *it);
                                                                        if (*it == "Id4")
                                                                        {
                                                                            reproStream << maxIdRepro;
                                                                        }else if (*it == "Id3")
                                                                        {
                                                                            reproStream << GUId;
                                                                        }else if (*it == "IdSex")
                                                                        {
                                                                            reproStream << "2";
                                                                        }
                                                                        if ((*it != "") && (*it != "###"))
                                                                        {
                                                                            reproStream << "\t";
                                                                        }
                                                                    }
                                                                    reproStream << "\n";
                                                                }
                                                                reproFile.close();
                                                                reproListMap[GUId].second.append(maxIdRepro);
                                                            }
                                                            // we can add its measures here
                                                            if (reproListMap[GUId].second.size() > nVarFem - 1)
                                                            {
                                                                if (reproSaved.contains(reproListMap[GUId].second.at(nVarFem - 1)))
                                                                {
                                                                    if (QMessageBox::warning(NULL, "Enregistrement existant", "Un enregistrement existe déjà pour cette date pour la pièce reproductrice femelle n°" + QString::number(nVarFem) + " de l'UC " + dataTab->item(row, 0)->text() + "\n Remplacé-je l'ancien enregistrement par celui-ci?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                                                                    {
                                                                        QFile measureFile("bin/reproductionMeasure");
                                                                        QString line, s;
                                                                        if (!measureFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                                                                        {
                                                                            // Error
                                                                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                                                            urgentStop();
                                                                            return;
                                                                        }else{
                                                                            QTextStream measureStream(&measureFile);
                                                                            nReproMeasure++;
                                                                            line = measureStream.readLine();
                                                                            s.append(line + "\n");
                                                                            line = measureStream.readLine();
                                                                            s.append(line + "\n");
                                                                            while (!measureStream.atEnd()  && line != "###")
                                                                            {
                                                                                line = measureStream.readLine();
                                                                                s.append(line + "\n");
                                                                            }
                                                                            while(!measureStream.atEnd())
                                                                            {
                                                                                line = measureStream.readLine();
                                                                                if (line.split("\t").size() > reproMeasureList.indexOf("Id4") && line.split("\t").size() > reproMeasureList.indexOf("DateRM"))
                                                                                {
                                                                                    if (!((reproListMap[GUId].second.at(nVarFem - 1) == line.split("\t").at(reproMeasureList.indexOf("Id4")).toInt() && (line.split("\t").at(reproMeasureList.indexOf("DateRM")) == measureDate.toString("yyyy.MM.dd")))))
                                                                                    {
                                                                                        s.append(line + "\n");
                                                                                    }
                                                                                }
                                                                            }
                                                                            measureFile.resize(0);
                                                                            measureStream << s;
                                                                            for (QList<QString>::iterator it(reproMeasureList.begin()); it != reproMeasureList.end(); it++)
                                                                            {
                                                                                //QMessageBox::information(NULL, "", *it);
                                                                                if (*it == "IdRM")
                                                                                {
                                                                                    measureStream << nReproMeasure;
                                                                                }else if (*it == "Id4")
                                                                                {
                                                                                    measureStream << reproListMap[GUId].second.at(nVarFem - 1);
                                                                                }else if (*it == "DateRM")
                                                                                {
                                                                                    measureStream << measureDate.toString("yyyy.MM.dd");
                                                                                }else if (*it == "Auteur")
                                                                                {
                                                                                    if (userData.toList().size() > 1) measureStream << userData.toList().at(1).toString();
                                                                                }else if ((*it != "") && (*it != "###"))
                                                                                {
                                                                                    if (recordMap.contains(*it))
                                                                                    {
                                                                                        measureStream << recordMap[*it];
                                                                                    }
                                                                                }
                                                                                if ((*it != "") && (*it != "###"))
                                                                                {
                                                                                    measureStream << "\t";
                                                                                }
                                                                            }
                                                                            measureStream << "\n";
                                                                        }
                                                                        measureFile.close();
                                                                    }
                                                                }else{
                                                                    QFile measureFile("bin/reproductionMeasure");
                                                                    QString line;
                                                                    if (!measureFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                                                                    {
                                                                        // Error
                                                                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                                                        urgentStop();
                                                                        return;
                                                                    }else{
                                                                        QTextStream measureStream(&measureFile);
                                                                        nReproMeasure++;
                                                                        for (QList<QString>::iterator it(reproMeasureList.begin()); it != reproMeasureList.end(); it++)
                                                                        {
                                                                            //QMessageBox::information(NULL, "", *it);
                                                                            if (*it == "IdRM")
                                                                            {
                                                                                measureStream << nReproMeasure;
                                                                            }else if (*it == "Id4")
                                                                            {
                                                                                measureStream << reproListMap[GUId].second.at(nVarFem - 1);
                                                                            }else if (*it == "DateRM")
                                                                            {
                                                                                measureStream << measureDate.toString("yyyy.MM.dd");
                                                                            }else if (*it == "Auteur")
                                                                            {
                                                                                if (userData.toList().size() > 1) measureStream << userData.toList().at(1).toString();
                                                                            }else if ((*it != "") && (*it != "###"))
                                                                            {
                                                                                if (recordMap.contains(*it))
                                                                                {
                                                                                    measureStream << recordMap[*it];
                                                                                }
                                                                            }
                                                                            if ((*it != "") && (*it != "###"))
                                                                            {
                                                                                measureStream << "\t";
                                                                            }
                                                                        }
                                                                        measureStream << "\n";
                                                                    }
                                                                    measureFile.close();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                // Then we can reset the window
                                                // first we delete the tables and buttons now useless
                                                QWidget *item;
                                                item = mainLayout->itemAtPosition(10, 0)->widget();
                                                mainLayout->removeWidget(item);
                                                delete item;
                                                item = mainLayout->itemAtPosition(11, 0)->widget();
                                                mainLayout->removeWidget(item);
                                                delete item;
                                                item = mainLayout->itemAtPosition(12, 0)->widget();
                                                mainLayout->removeWidget(item);
                                                delete item;
                                                item = mainLayout->itemAtPosition(13, 0)->widget();
                                                mainLayout->removeWidget(item);
                                                mainLayout->addWidget(item, 10, 0, 1, 4);
                                                item = mainLayout->itemAtPosition(14, 0)->widget();
                                                mainLayout->removeWidget(item);
                                                mainLayout->addWidget(item, 11, 0, 1, 4);
                                                // then we reset the geometry of the mainlayout
                                                QList<QWidget *> itemList;
                                                QList<QList<int> > parameters;
                                                QList<int> rowParameters;
                                                rowParameters.append(0);
                                                rowParameters.append(0);
                                                rowParameters.append(0);
                                                rowParameters.append(0);
                                                int count(0);
                                                int row, column, rowSpan, columnSpan;
                                                while(mainLayout->count() > 0){
                                                    QWidget *widget = mainLayout->itemAt(0)->widget();
                                                    mainLayout->getItemPosition(0, &row, &column, &rowSpan, &columnSpan);
                                                    mainLayout->removeWidget(widget);
                                                    rowParameters[0] = row;
                                                    rowParameters[1] = column;
                                                    rowParameters[2] = rowSpan;
                                                    rowParameters[3] = columnSpan;
                                                    parameters.append(rowParameters);
                                                    itemList.append(widget);
                                                    count++;
                                                }
                                                delete mainLayout;
                                                mainLayout = new QGridLayout;
                                                setLayout(mainLayout);
                                                for (int count(0); count < itemList.size(); count++)
                                                {
                                                    if (itemList.size() > count && parameters.size() > count) if (parameters.at(count).size() > 3) mainLayout->addWidget(itemList.at(count), parameters.at(count).at(0), parameters.at(count).at(1), parameters.at(count).at(2), parameters.at(count).at(3));
                                                }
                                                // Finally, we update axis informations
                                                QList<QVariant> datas(userData.toList());
                                                QFile AxisFile("bin/list2");
                                                QMap<QString, int> AxisPosition;
                                                AxisPosition.clear();
                                                QString line, cat, Phrase;
                                                QStringList rawLine;
                                                count = 0;
                                                Phrase = "";
                                                QDate FirstObse, LastObse;
                                                if (!AxisFile.open(QIODevice::ReadWrite | QIODevice::Text))
                                                {
                                                    // Error
                                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                                                    urgentStop();
                                                    return;
                                                }else{
                                                    QTextStream AxisStream(&AxisFile);
                                                    line = AxisStream.readLine();
                                                    Phrase += line;
                                                    if (line != "###")
                                                    {
                                                        // Error
                                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                                                        urgentStop();
                                                        return;
                                                    }else{
                                                        line = AxisStream.readLine();
                                                        Phrase += "\n" + line;
                                                        while (!AxisStream.atEnd()  && line != "###")
                                                        {
                                                            cat = line.split("\t").at(0);
                                                            AxisPosition.insert(cat, count);
                                                            line = AxisStream.readLine();
                                                            Phrase += "\n" + line;
                                                            count++;
                                                        }
                                                        line = AxisStream.readLine();
                                                        if (datas.size() > 3)
                                                        {
                                                            while(line.split("\t").at(0).toInt() != datas.at(3).toInt())
                                                            {
                                                                Phrase += "\n" + line;
                                                                line = AxisStream.readLine();
                                                            }
                                                        }
                                                        rawLine = line.split("\t");
                                                        rawLine[AxisPosition["ASFirst"]] = QString::number(ASF);
                                                        rawLine[AxisPosition["ASLast"]] = QString::number(ASL);
                                                        if (rawLine[AxisPosition["FirstObserve"]].split(".").size() > 2) FirstObse = QDate(rawLine[AxisPosition["FirstObserve"]].split(".").at(0).toInt(), rawLine[AxisPosition["FirstObserve"]].split(".").at(1).toInt(), rawLine[AxisPosition["FirstObserve"]].split(".").at(2).toInt());
                                                        if (rawLine[AxisPosition["LastObserve"]].split(".").size() > 2) LastObse = QDate(rawLine[AxisPosition["LastObserve"]].split(".").at(0).toInt(), rawLine[AxisPosition["LastObserve"]].split(".").at(1).toInt(), rawLine[AxisPosition["LastObserve"]].split(".").at(2).toInt());
                                                        if (measureDate < FirstObse)
                                                        {
                                                            FirstObse = measureDate;
                                                        }
                                                        if (measureDate > LastObse)
                                                        {
                                                            LastObse = measureDate;
                                                        }
                                                        if (rawLine[AxisPosition["FirstObserve"]] == "0000.00.00")
                                                        {
                                                            FirstObse = measureDate;
                                                        }
                                                        if (rawLine[AxisPosition["LastObserve"]] == "0000.00.00")
                                                        {
                                                            LastObse = measureDate;
                                                        }
                                                        rawLine[AxisPosition["FirstObserve"]] = FirstObse.toString("yyyy.MM.dd");
                                                        rawLine[AxisPosition["LastObserve"]] = LastObse.toString("yyyy.MM.dd");
                                                        line = rawLine.join("\t");
                                                        Phrase += "\n" + line;
                                                        while (!AxisStream.atEnd())
                                                        {
                                                            line = AxisStream.readLine();
                                                            Phrase += "\n" + line;
                                                        }
                                                        Phrase += "\n";
                                                        AxisFile.resize(0);
                                                        AxisStream << Phrase;
                                                    }
                                                }
                                                AxisFile.close();
                                                // Then we set the line in bold to indicate the axis was measured
                                                row = 0;
                                                bool test(false);
                                                QFont font;
                                                while (!test)
                                                {
                                                    if (datas.size() > 3)
                                                    {
                                                        if (qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->item(row, 0)->data(Qt::UserRole).toList().at(0).toInt() != datas.at(3))
                                                        {
                                                            if (qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->rowCount() == (row + 1))
                                                            {
                                                                test = true;
                                                                row = -1;
                                                            }else{
                                                                row++;
                                                            }
                                                        }else{
                                                            test = true;
                                                        }
                                                    }
                                                }
                                                if (row == -1)
                                                {
                                                    test = false;
                                                    row = 0;
                                                    while (!test)
                                                    {
                                                        if (datas.size() > 3)
                                                        {
                                                            if (qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 3)->widget())->item(row, 0)->data(Qt::UserRole).toList().at(0).toInt() != datas.at(3))
                                                            {
                                                                row++;
                                                            }else{
                                                                test = true;
                                                            }
                                                        }
                                                    }
                                                    for (int c(0); c < qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 3)->widget())->columnCount(); c++)
                                                    {
                                                        font = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 3)->widget())->item(row, c)->font();
                                                        font.setBold(true);
                                                        qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 3)->widget())->item(row, c)->setFont(font);
                                                    }
                                                    qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 3)->widget())->item(row, 2)->setText(QString::number(ASF));
                                                    qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 3)->widget())->item(row, 3)->setText(QString::number(ASL));
                                                    qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 3)->widget())->item(row, 4)->setText(FirstObse.toString("dd/MM/yyyy"));
                                                    qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 3)->widget())->item(row, 5)->setText(LastObse.toString("dd/MM/yyyy"));
                                                }else{
                                                    for (int c(0); c < qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->columnCount(); c++)
                                                    {
                                                        font = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->item(row, c)->font();
                                                        font.setBold(true);
                                                        qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->item(row, c)->setFont(font);
                                                    }
                                                    qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->item(row, 2)->setText(QString::number(ASF));
                                                    qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->item(row, 3)->setText(QString::number(ASL));
                                                    qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->item(row, 4)->setText(FirstObse.toString("dd/MM/yyyy"));
                                                    qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget())->item(row, 5)->setText(LastObse.toString("dd/MM/yyyy"));
                                                }
                                                datas.at(3);
                                                datas.removeAt(3);
                                                userData = QVariant(datas);
                                            }
                                        }else{
                                            QMessageBox::information(NULL, "Pas d'enregistrement", "Je n'ai pas trouvé de données à sauvegarder.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
