#include <MainWindow.h>

void MainWindow::moveUp()
{
    //QMessageBox::information(NULL, "", "yo!");
    if (mainLayout->rowCount() == 15)
    {
        QTableWidget *dataTable = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(11, 0)->widget());
        int row(dataTable->currentRow()), col(dataTable->currentColumn());
        if ((row != -1) && (col != -1))
        {
            if (row > 0) dataTable->setCurrentCell(row - 1, col);
        }
    }
}

void MainWindow::moveRight()
{
    if (mainLayout->rowCount() == 15)
    {
        QTableWidget *dataTable = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(11, 0)->widget());
        int row(dataTable->currentRow()), col(dataTable->currentColumn());
        if ((row != -1) && (col != -1))
        {
            if (col < (dataTable->columnCount() - 1)) dataTable->setCurrentCell(row , col + 1);
        }
    }
}

void MainWindow::moveDown()
{
    if (mainLayout->rowCount() == 15)
    {
        QTableWidget *dataTable = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(11, 0)->widget());
        int row(dataTable->currentRow()), col(dataTable->currentColumn());
        if ((row != -1) && (col != -1))
        {
            if (row < (dataTable->rowCount() - 1))
            {
                dataTable->setCurrentCell(row + 1, col);
            }else{
                mainLayout->itemAtPosition(12, 0)->widget()->setFocus();
            }
        }
    }
}

void MainWindow::moveLeft()
{
    if (mainLayout->rowCount() == 15)
    {
        QTableWidget *dataTable = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(11, 0)->widget());
        int row(dataTable->currentRow()), col(dataTable->currentColumn());
        if ((row != -1) && (col != -1))
        {
            if (col > 0) dataTable->setCurrentCell(row, col - 1);
        }
    }
}
