#include <MainWindow.h>

void MainWindow::expandDataConsult(int a, int b)
{
    // we put the table with the datas
    QWidget *dataTab;
    QWidget *title;
    QWidget *dateComboBox;
    if (mainLayout->rowCount()<6)
    {
        QWidget * buttonBackToMain = mainLayout->itemAtPosition(4, 0)->widget();
        mainLayout->removeWidget(buttonBackToMain);
        mainLayout->addWidget(buttonBackToMain, 7, 0, 1, 3);
        QWidget * buttonBackToBack = mainLayout->itemAtPosition(3, 0)->widget();
        mainLayout->removeWidget(buttonBackToBack);
        mainLayout->addWidget(buttonBackToBack, 6, 0, 1, 3);
        dataTab = new QTableWidget(0, 0);
        dataTab->setObjectName("dataTable");
        mainLayout->addWidget(dataTab, 5, 0, 1, 3);
        qobject_cast<QTableWidget *>(dataTab)->verticalHeader()->hide();
        title = new QLabel;
        mainLayout->addWidget(title, 3, 0, 1, 3);
        dateComboBox = new QComboBox;
        mainLayout->addWidget(dateComboBox, 4, 1);
    }else{
        dataTab = mainLayout->itemAtPosition(5, 0)->widget();
        for (int i = qobject_cast<QTableWidget *>(dataTab)->rowCount(); i>0; i--)
        {
            qobject_cast<QTableWidget *>(dataTab)->removeRow(i - 1);
        }
        for (int j = qobject_cast<QTableWidget *>(dataTab)->columnCount(); j>0; j--)
        {
            qobject_cast<QTableWidget *>(dataTab)->removeColumn(j - 1);
        }
        title = mainLayout->itemAtPosition(3, 0)->widget();
        dateComboBox = mainLayout->itemAtPosition(4, 1)->widget();
        qobject_cast<QComboBox *>(dateComboBox)->clear();
        QObject::disconnect(dateComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeDateDataConsult(int)));
    }

    // we need now to know which was the selected Axis
    int axisSelected(-1);
    axisSelected = qobject_cast<QTableWidget *>(QObject::sender())->item(a, b)->data(Qt::UserRole).toList().at(0).toInt();
    qobject_cast<QLabel *>(title)->setText(qobject_cast<QTableWidget *>(QObject::sender())->item(a, 0)->text());
    QFile GUFile("bin/list3");
    int countLine(-1);
    QMap<QString, int> GUPosition;
    QMap<int, QStringList> GUMap;
    QMap<QString, int> GUNames;
    QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > dataFrame;
    QString line, value;
    QStringList dataRow;
    int Id;
    if (!GUFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list3.");
        urgentStop();
        return;
    }else{
        QTextStream GUStream(&GUFile);
        line = GUStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list3\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!GUStream.atEnd()  && line != "###")
            {
                line = GUStream.readLine();
                GUPosition.insert(line.split("\t").at(0), countLine);
                countLine++;
            }
            while(!GUStream.atEnd())
            {
                dataRow = GUStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (dataRow.size() > GUPosition["Id2"])
                {
                    if (dataRow.at(GUPosition["Id2"]).toInt() == axisSelected)
                    {
                        GUMap.insert(Id, dataRow);
                        dataFrame.insert(Id, *(new QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > >));
                        dataFrame[Id].second.first.first = -1; // -1 = No male reproduction
                        if (dataRow.size() > GUPosition["Year"] && dataRow.size() > GUPosition["NGU"])
                        {
                            if ((dataRow.at(GUPosition["Year"]) == "0") || (dataRow.at(GUPosition["Year"]) == ""))
                            {
                                GUNames.insert(QString("---- . %1").arg(dataRow.at(GUPosition["NGU"])), Id);
                            }else{
                                GUNames.insert(QString("%1 . %2").arg(dataRow.at(GUPosition["Year"])).arg(dataRow.at(GUPosition["NGU"])), Id);
                            }
                        }
                    }
                }
            }
        }
        // then list4
        QFile reproFile("bin/list4");
        countLine = -1;
        QMap<QString, int> reproPosition;
        QMap<int, QStringList> reproMap;
        int maxMale(0), maxFemale(0);
        if (!reproFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
            urgentStop();
            return;
        }else{
            QTextStream reproStream(&reproFile);
            line = reproStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list4\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!reproStream.atEnd()  && line != "###")
                {
                    line = reproStream.readLine();
                    reproPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!reproStream.atEnd())
                {
                    dataRow = reproStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    if (GUMap.contains(dataRow.at(reproPosition["Id3"]).toInt()))
                    {
                        reproMap.insert(Id, dataRow);
                        if (dataRow.size() > reproPosition["IdSex"] && dataRow.size() > reproPosition["Id3"])
                        {
                            if (dataRow.at(reproPosition["IdSex"]) == "1")
                            {
                                dataFrame[dataRow.at(reproPosition["Id3"]).toInt()].second.first.first = Id;
                                maxMale = 1;
                            }else{
                                dataFrame[dataRow.at(reproPosition["Id3"]).toInt()].second.second.insert(Id, *(new QMap<QString, int>));
                                if (dataFrame[dataRow.at(reproPosition["Id3"]).toInt()].second.second.size() > maxFemale)
                                {
                                    maxFemale = dataFrame[dataRow.at(reproPosition["Id3"]).toInt()].second.second.size();
                                }
                            }
                        }
                    }
                }
            }
            // then sex
            QFile sexFile("bin/dictionary/sex");
            countLine = -1;
            QMap<QString, int> sexPosition;
            QMap<int, QStringList> sexMap;
            if (!sexFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/sex.");
                urgentStop();
                return;
            }else{
                QTextStream sexStream(&sexFile);
                line = sexStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/sex\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = "";
                    while (!sexStream.atEnd()  && line != "###")
                    {
                        line = sexStream.readLine();
                        sexPosition.insert(line.split("\t").at(0), countLine);
                        countLine++;
                    }
                    while(!sexStream.atEnd())
                    {
                        dataRow = sexStream.readLine().split("\t");
                        Id = dataRow.at(0).toInt();
                        dataRow.removeFirst();
                        sexMap.insert(Id, dataRow);
                    }
                }
                // then variables
                QFile varFile("bin/dictionary/variables");
                countLine = -1;
                QMap<QString, int> varPosition;
                QMap<int, QStringList> varMap;
                QFile * tempFile;
                QMap<QString, QMap<QString, int> > factorPosition;
                QMap<QString, QMap<int, QStringList> > factorMap;
                QMap<QString, int> tempPosition;
                QMap<int, QStringList> tempMap;
                int tempId;
                if (!varFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/variables.");
                    urgentStop();
                    return;
                }else{
                    QTextStream varStream(&varFile);
                    line = varStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/variables\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = "";
                        while (!varStream.atEnd()  && line != "###")
                        {
                            line = varStream.readLine();
                            varPosition.insert(line.split("\t").at(0), countLine);
                            countLine++;
                        }
                        while(!varStream.atEnd())
                        {
                            dataRow = varStream.readLine().split("\t");
                            Id = dataRow.at(0).toInt();
                            dataRow.removeFirst();
                            varMap.insert(Id, dataRow);
                            if (dataRow.size() > varPosition["TypeVar"])
                            {
                                if (varMap[Id].at(varPosition["TypeVar"]).startsWith("eKEY"))
                                {
                                    // factor variable
                                    if (dataRow.at(varPosition["TypeVar"]).split(":").size() > 1)
                                    {
                                        tempFile = new QFile(QString("bin/%1").arg(dataRow.at(varPosition["TypeVar"]).split(":").at(1)));
                                    }
                                    countLine = -1;
                                    tempPosition.clear();
                                    tempMap.clear();
                                    if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                        urgentStop();
                                        return;
                                    }else{
                                        QTextStream tempStream(tempFile);
                                        line = tempStream.readLine();
                                        if (line != "###")
                                        {
                                            // Error
                                            QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                            urgentStop();
                                            return;
                                        }else{
                                            line = "";
                                            while (!tempStream.atEnd()  && line != "###")
                                            {
                                                line = tempStream.readLine();
                                                tempPosition.insert(line.split("\t").at(0), countLine);
                                                countLine++;
                                            }
                                            while(!tempStream.atEnd())
                                            {
                                                dataRow = tempStream.readLine().split("\t");
                                                tempId = dataRow.at(0).toInt();
                                                dataRow.removeFirst();
                                                tempMap.insert(tempId, dataRow);
                                            }
                                        }
                                    }
                                    tempFile->close();
                                    if (varMap[Id].size() > varPosition["Name"])
                                    {
                                        factorPosition.insert(varMap[Id].at(varPosition["Name"]), tempPosition);
                                        factorMap.insert(varMap[Id].at(varPosition["Name"]), tempMap);
                                    }
                                }
                            }
                        }
                    }
                    // then reproduction variables
                    QFile reproVarFile("bin/dictionary/reproductionVariables");
                    countLine = -1;
                    QMap<QString, int> reproVarPosition;
                    QMap<int, QStringList> reproVarMap;
                    if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
                        urgentStop();
                        return;
                    }else{
                        QTextStream reproVarStream(&reproVarFile);
                        line = reproVarStream.readLine();
                        if (line != "###")
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
                            urgentStop();
                            return;
                        }else{
                            line = "";
                            while (!reproVarStream.atEnd()  && line != "###")
                            {
                                line = reproVarStream.readLine();
                                reproVarPosition.insert(line.split("\t").at(0), countLine);
                                countLine++;
                            }
                            while(!reproVarStream.atEnd())
                            {
                                dataRow = reproVarStream.readLine().split("\t");
                                Id = dataRow.at(0).toInt();
                                dataRow.removeFirst();
                                reproVarMap.insert(Id, dataRow);
                                if (reproVarMap[Id].size() > reproVarPosition["TypeVar"])
                                {
                                    if (reproVarMap[Id].at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                    {
                                        // factor variable
                                        if (dataRow.at(reproVarPosition["TypeVar"]).split(":").size() > 1) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproVarPosition["TypeVar"]).split(":").at(1)));
                                        countLine = -1;
                                        tempPosition.clear();
                                        tempMap.clear();
                                        if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                        {
                                            // Error
                                            QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                            urgentStop();
                                            return;
                                        }else{
                                            QTextStream tempStream(tempFile);
                                            line = tempStream.readLine();
                                            if (line != "###")
                                            {
                                                // Error
                                                QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                                urgentStop();
                                                return;
                                            }else{
                                                line = "";
                                                while (!tempStream.atEnd()  && line != "###")
                                                {
                                                    line = tempStream.readLine();
                                                    tempPosition.insert(line.split("\t").at(0), countLine);
                                                    countLine++;
                                                }
                                                while(!tempStream.atEnd())
                                                {
                                                    dataRow = tempStream.readLine().split("\t");
                                                    tempId = dataRow.at(0).toInt();
                                                    dataRow.removeFirst();
                                                    tempMap.insert(tempId, dataRow);
                                                }
                                            }
                                        }
                                        tempFile->close();
                                        if (reproVarMap[Id].size() > reproVarPosition["Name"])
                                        {
                                            factorPosition.insert(reproVarMap[Id].at(reproVarPosition["Name"]), tempPosition);
                                            factorMap.insert(reproVarMap[Id].at(reproVarPosition["Name"]), tempMap);
                                        }
                                    }
                                }
                            }
                        }
                        // then we load the measures
                        QFile measureFile("bin/measure");
                        countLine = -1;
                        QMap<QString, int> measurePosition;
                        QMap<int, QStringList> measureMap;
                        if (!measureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/measure.");
                            urgentStop();
                            return;
                        }else{
                            QTextStream measureStream(&measureFile);
                            line = measureStream.readLine();
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/measure\nMauvaise entête (\"###\" attendu)");
                                urgentStop();
                                return;
                            }else{
                                line = "";
                                while (!measureStream.atEnd()  && line != "###")
                                {
                                    line = measureStream.readLine();
                                    measurePosition.insert(line.split("\t").at(0), countLine);
                                    countLine++;
                                }
                                while(!measureStream.atEnd())
                                {
                                    dataRow = measureStream.readLine().split("\t");
                                    Id = dataRow.at(0).toInt();
                                    dataRow.removeFirst();
                                    if (dataRow.size() > measurePosition["Id3"])
                                    {
                                        if (GUMap.contains(dataRow.at(measurePosition["Id3"]).toInt()))
                                        {
                                            measureMap.insert(Id, dataRow);
                                            if (dataRow.size() > measurePosition["DateM"]) dataFrame[dataRow.at(measurePosition["Id3"]).toInt()].first.insert(dataRow.at(measurePosition["DateM"]), Id);
                                        }
                                    }
                                }
                            }
                            // and then we load the reproduction measures
                            QFile reproMeasureFile("bin/reproductionMeasure");
                            countLine = -1;
                            QMap<QString, int> reproMeasurePosition;
                            QMap<int, QStringList> reproMeasureMap;
                            if (!reproMeasureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                urgentStop();
                                return;
                            }else{
                                QTextStream reproMeasureStream(&reproMeasureFile);
                                line = reproMeasureStream.readLine();
                                if (line != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/reproductionMeasure\nMauvaise entête (\"###\" attendu)");
                                    urgentStop();
                                    return;
                                }else{
                                    line = "";
                                    while (!reproMeasureStream.atEnd()  && line != "###")
                                    {
                                        line = reproMeasureStream.readLine();
                                        reproMeasurePosition.insert(line.split("\t").at(0), countLine);
                                        countLine++;
                                    }
                                    while(!reproMeasureStream.atEnd())
                                    {
                                        dataRow = reproMeasureStream.readLine().split("\t");
                                        Id = dataRow.at(0).toInt();
                                        dataRow.removeFirst();
                                        if (dataRow.size() > reproMeasurePosition["Id4"])
                                        {
                                            if (reproMap.contains(dataRow.at(reproMeasurePosition["Id4"]).toInt()))
                                            {
                                                reproMeasureMap.insert(Id, dataRow);
                                                if (reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["IdSex"]) == "1")
                                                {
                                                    if (dataRow.size() > reproMeasurePosition["DateRM"]) dataFrame[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].second.first.second.insert(dataRow.at(reproMeasurePosition["DateRM"]), Id);
                                                }else{
                                                    if (dataRow.size() > reproMeasurePosition["DateRM"]) dataFrame[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].second.second[dataRow.at(reproMeasurePosition["Id4"]).toInt()].insert(dataRow.at(reproMeasurePosition["DateRM"]), Id);
                                                }
                                            }
                                        }
                                    }
                                }
                                // then we can put the datas
                                QTableWidgetItem * cell;
                                QList<QString> tabHeader;
                                tabHeader.clear();
                                tabHeader << "UC";
                                int col(0);
                                qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                QString varName;
                                for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                {
                                    if (var->at(varPosition["AppearingVar"]) == "Y")
                                    {
                                        col++;
                                        qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        tabHeader << var->at(varPosition["MediumName"]);
                                    }
                                }
                                // then reproduction
                                if (maxMale != 0)
                                {
                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                    {
                                        if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                        {
                                            if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                            {
                                                col++;
                                                qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                if (it->size() > reproVarPosition["MediumName"]) varName = it->at(reproVarPosition["MediumName"]);
                                                varName.replace("#", "Male");
                                                tabHeader << varName;
                                            }
                                        }
                                    }
                                }
                                for (int i(0); i < maxFemale; i++)
                                {
                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                    {
                                        if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                        {
                                            if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                            {
                                                col++;
                                                qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                varName = it->at(reproVarPosition["MediumName"]);
                                                varName.replace("#", "Fem%1").arg(QString::number(i + 1));
                                                tabHeader << varName;
                                            }
                                        }
                                    }
                                }
                                qobject_cast<QTableWidget *>(dataTab)->setHorizontalHeaderLabels(QStringList(tabHeader));
                                int row(0);
                                QMap<QString, QVariant> datas;
                                QList<QString> dates;
                                for (QMap<QString, int>::iterator gu(GUNames.begin()); gu != GUNames.end(); gu++)
                                {
                                    cell = new QTableWidgetItem;
                                    col = 0;
                                    cell->setData(Qt::UserRole, QVariant(gu.value()));
                                    cell->setText(gu.key());
                                    cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                                    if (gu.key().startsWith("----"))
                                    {
                                        row = qobject_cast<QTableWidget *>(dataTab)->rowCount();
                                    }else{
                                        row = 0;
                                    }
                                    qobject_cast<QTableWidget *>(dataTab)->insertRow(row);
                                    qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                    // first the measure
                                    for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                    {
                                        if (var->size() > varPosition["AppearingVar"])
                                        {
                                            if (var->at(varPosition["AppearingVar"]) == "Y")
                                            {
                                                col++;
                                                cell = new QTableWidgetItem;
                                                value = "";
                                                datas = *(new QMap<QString, QVariant>);
                                                for (QMap<QString, int>::iterator measure(dataFrame[gu.value()].first.begin()); measure != dataFrame[gu.value()].first.end(); measure++)
                                                {
                                                    if (var->size() > varPosition["Name"])
                                                    {
                                                        if (measureMap[measure.value()].size() > measurePosition[var->at(varPosition["Name"])])
                                                        {
                                                            if (measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]) != "")
                                                            {
                                                                if (var->size() > varPosition["TypeVar"])
                                                                {
                                                                    if ((var->at(varPosition["TypeVar"]).startsWith("eKEY")))// && (value != ""))
                                                                    {
                                                                        if (measureMap[measure.value()].size() > measurePosition[var->at(varPosition["Name"])])
                                                                        {
                                                                            if (factorMap[var->at(varPosition["Name"])][measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]).toInt()].size() > factorPosition[var->at(varPosition["Name"])]["Code"])
                                                                            {
                                                                                value = factorMap[var->at(varPosition["Name"])][measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]).toInt()].at(factorPosition[var->at(varPosition["Name"])]["Code"]);
                                                                            }
                                                                        }
                                                                    }else{
                                                                        if (measureMap[measure.value()].size() > measurePosition[var->at(varPosition["Name"])]) value = measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]);
                                                                    }
                                                                }
                                                                if (measureMap[measure.value()].size() > measurePosition["DateM"])
                                                                {
                                                                    datas.insert(measureMap[measure.value()].at(measurePosition["DateM"]), value);
                                                                    if (!dates.contains(measureMap[measure.value()].at(measurePosition["DateM"])))
                                                                    {
                                                                        dates.append(measureMap[measure.value()].at(measurePosition["DateM"]));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                cell->setData(Qt::UserRole, QVariant(datas));
                                                cell->setText(value);
                                                cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                            }
                                        }
                                    }
                                    // then reproduction
                                    if (maxMale != 0)
                                    {
                                        Id = dataFrame[gu.value()].second.first.first;
                                        if (Id == -1)
                                        {
                                            datas = *(new QMap<QString, QVariant>);
                                            value = "";
                                            for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                            {
                                                if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                                {
                                                    if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                                    {
                                                        col++;
                                                        cell = new QTableWidgetItem;
                                                        cell->setData(Qt::UserRole, QVariant(datas));
                                                        cell->setText(value);
                                                        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                        qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                    }
                                                }
                                            }
                                        }else{
                                            for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                            {
                                                if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                                {
                                                    if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                                    {
                                                        col++;
                                                        cell = new QTableWidgetItem;
                                                        value = "";
                                                        datas = *(new QMap<QString, QVariant>);
                                                        for (QMap<QString, int>::iterator measure(dataFrame[gu.value()].second.first.second.begin()); measure != dataFrame[gu.value()].second.first.second.end(); measure++)
                                                        {
                                                            if (it->size() > reproVarPosition["Name"])
                                                            {
                                                                if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                {
                                                                    if (reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]) != "")
                                                                    {
                                                                        if (it->size() > reproVarPosition["TypeVar"])
                                                                        {
                                                                            if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (value != ""))
                                                                            {
                                                                                if (factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"]) value = factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]);
                                                                            }else{
                                                                                value = reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                            }
                                                                        }
                                                                        if (reproMeasureMap[measure.value()].size() > reproMeasurePosition["DateRM"])
                                                                        {
                                                                            datas.insert(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]), value);
                                                                            if (!dates.contains(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"])))
                                                                            {
                                                                                dates.append(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        cell->setData(Qt::UserRole, QVariant(datas));
                                                        cell->setText(value);
                                                        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                        qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    for (int i(0); i < maxFemale; i++)
                                    {
                                        if (i < dataFrame[gu.value()].second.second.size())
                                        {
                                            if (dataFrame[gu.value()].second.second.keys().size() > i) tempId = dataFrame[gu.value()].second.second.keys().at(i);
                                            for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                            {
                                                if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                                {
                                                    if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                                    {
                                                        col++;
                                                        cell = new QTableWidgetItem;
                                                        value = "";
                                                        datas = *(new QMap<QString, QVariant>);
                                                        for (QMap<QString, int>::iterator measure(dataFrame[gu.value()].second.second[tempId].begin()); measure != dataFrame[gu.value()].second.second[tempId].end(); measure++)
                                                        {
                                                            if (it->size() > reproVarPosition["Name"])
                                                            {
                                                                if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                {
                                                                    if (reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]) != "")
                                                                    {
                                                                        if (it->size() > reproVarPosition["TypeVar"])
                                                                        {
                                                                            if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (value != ""))
                                                                            {
                                                                                if (factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"]) value = factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]);
                                                                            }else{
                                                                                value = reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                            }
                                                                        }
                                                                        if (reproMeasureMap[measure.value()].size() > reproMeasurePosition["DateRM"])
                                                                        {
                                                                            datas.insert(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]), value);
                                                                            if (!dates.contains(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"])))
                                                                            {
                                                                                dates.append(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        cell->setData(Qt::UserRole, QVariant(datas));
                                                        cell->setText(value);
                                                        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                        qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                    }
                                                }
                                            }
                                        }else{
                                            datas = *(new QMap<QString, QVariant>);
                                            value = "";
                                            for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                            {
                                                if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                                {
                                                    if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                                    {
                                                        col++;
                                                        cell = new QTableWidgetItem;
                                                        cell->setData(Qt::UserRole, QVariant(datas));
                                                        cell->setText(value);
                                                        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                        qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                qSort(dates.begin(), dates.end());
                                QStringList currentDate;
                                for (int i(dates.size() - 1); i >=0 ; i--)
                                {
                                    currentDate = dates.at(i).split(".");
                                    if (currentDate.size() > 2)
                                    {
                                        qobject_cast<QComboBox *>(dateComboBox)->addItem(QString("%3/%2/%1").arg(currentDate.at(0)).arg(currentDate.at(1)).arg(currentDate.at(2)), QVariant(dates.at(i)));
                                    }
                                }
                                // Connexion of the signals to the slots
                                QObject::connect(dateComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeDateDataConsult(int)));
                                QObject::connect(qobject_cast<QTableWidget *>(dataTab), SIGNAL(cellClicked(int, int)), this, SLOT(updateCommentConsult(int, int)));
                            }
                            reproMeasureFile.close();
                        }
                        measureFile.close();
                    }
                    reproVarFile.close();
                }
                varFile.close();
            }
            sexFile.close();
        }
        reproFile.close();
    }
    GUFile.close();
}
