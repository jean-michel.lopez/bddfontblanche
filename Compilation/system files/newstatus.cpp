#include <newstatus.h>

newStatus::newStatus(): QDialog()
{
    prevStatus = "";
    statusList = *(new QMap<QString, int>);
    title = new QLabel;
    validateButton = new QPushButton;
    cancelButton = new QPushButton;
    statusText = new QLineEdit;
    QGridLayout *mainLayout = new QGridLayout;

    title->setText("quel est le nouveau status du rameau ?");
    validateButton->setText("Valider");
    cancelButton->setText("Annuler");

    // first list1
    QFile statusFile("bin/dictionary/status");
    int countLine(-1);
    QMap<QString, int> statusMap;
    QStringList dataRow;
    int Id;
    QString line;
    if (!statusFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/status.");
        urgentStop();
        return;
    }else{
        QTextStream statusStream(&statusFile);
        line = statusStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/status\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!statusStream.atEnd()  && line != "###")
            {
                line = statusStream.readLine();
                statusMap.insert(line.split("\t").at(0), countLine);
                if (line != "###" && line !="")
                {
                    statusPosition.append(line.split("\t").at(0));
                }
                countLine++;
            }
            while(!statusStream.atEnd())
            {
                dataRow = statusStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (dataRow.size() > statusMap["NameStatus"]) statusList.insert(dataRow.at(statusMap["NameStatus"]), Id);
            }
        }
    }
    statusFile.close();

    // connect Signals to slots
    QObject::connect(statusText, SIGNAL(textChanged(QString)), this, SLOT(guessStatus(QString)));
    QObject::connect(validateButton, SIGNAL(clicked()), this, SLOT(validation()));
    QObject::connect(cancelButton, SIGNAL(clicked()), this, SLOT(close()));

    mainLayout->addWidget(title, 0, 0, 1, 2);
    mainLayout->addWidget(statusText, 1, 0, 1, 2);
    mainLayout->addWidget(validateButton, 2, 0);
    mainLayout->addWidget(cancelButton, 2, 1);

    setLayout(mainLayout);
    setModal(true);
    show();
}

void newStatus::guessStatus(QString currentStatus)
{
    if ((currentStatus != "") && (currentStatus.startsWith(prevStatus)) && (currentStatus.length() > prevStatus.length()))
    {
        if (statusText->cursorPosition() == (currentStatus.size()))
        {
            int pos(-1);
            QString statusGuessed;
            for (QMap<QString, int>::iterator it(statusList.begin()); it != statusList.end(); it++)
            {
                if (it.key().startsWith(currentStatus))
                {
                    if (pos == -1)
                    {
                        pos = it.value();
                        statusGuessed = it.key();
                    }else{
                        pos = -2;
                    }
                }
            }
            if (pos >= 0)
            {
                statusText->setText(statusGuessed);
                statusText->setSelection(currentStatus.size(), statusGuessed.size());
            }
        }
    }
    prevStatus = currentStatus;
}

void newStatus::validation()
{
    if (statusText->text() == "")
    {
        close();
    }else{
        if (!statusList.contains(statusText->text()))
        {
            // We have to save this status
            QFile statusFile("bin/dictionary/status");
            int IdStat(-1);
            for (QMap<QString, int>::iterator it(statusList.begin()); it != statusList.end(); it++)
            {
                if (IdStat < it.value())
                {
                    IdStat = it.value();
                }
            }
            IdStat++;
            if (!statusFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/status.");
                urgentStop();
                return;
            }else{
                QTextStream statusStream(&statusFile);
                QString line;
                for (QList<QString>::iterator it(statusPosition.begin()); it != statusPosition.end(); it++)
                {
                    if (*it == "IdStat")
                    {
                        line += QString::number(IdStat);
                    }else if (*it == "NameStatus")
                    {
                        line += statusText->text();
                    }else if (*it == "Followed")
                    {
                        if (statusText->text().toLower() == "suivi")
                        {
                            line += "T";
                        }else{
                            line += "F";
                        }
                    }
                    if (*it != statusPosition.last())
                    {
                        line += "\t";
                    }
                }
                statusStream << line << "\n";
            }
            statusFile.close();
            statusList.insert(statusText->text(), IdStat);
        }
        accept();
    }
}

int newStatus::getStatusCode()
{
    if (statusList.contains(statusText->text()))
    {
        return statusList[statusText->text()];
    }else{
        return -1;
    }
}

QString newStatus::getStatusName()
{
    return statusText->text();
}

int newStatus::getNewStatus(QWidget *parent, QString *statusName, bool *followed)
{
    newStatus *window = new newStatus;
    window->setParent(parent);
    int value(-1);

    if(window->exec())
    {
        value = window->getStatusCode();
        *statusName = window->getStatusName();
        if ((*statusName).toLower() == "suivi")
        {
            *followed = true;
        }
        return value;
    }else
    {
        return value;
    }
}
