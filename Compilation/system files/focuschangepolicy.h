#include <QDoubleSpinBox>
#include <QObject>
#include <QEvent>
#include <QMessageBox>
#include <QSpinBox>
#include <QLineEdit>

#ifndef FOCUSCHANGEPOLICY
#define FOCUSCHANGEPOLICY

class SelectFocus : public QObject
{
    Q_OBJECT

protected:
    bool eventFilter(QObject *obj, QEvent *event);
};


#endif // FOCUSCHANGEPOLICY
