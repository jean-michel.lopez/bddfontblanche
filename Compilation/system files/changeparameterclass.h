#ifndef CHANGEPARAMETERCLASS
#define CHANGEPARAMETERCLASS

#include <QDialog>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QList>
#include <QVariant>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QMap>
#include <QStringList>
#include <QString>
#include <QRegExp>
#include <QValidator>
#include <QInputDialog>
#include <QDate>
#include <QDateEdit>

class changeParameterClass : public QDialog
{
    Q_OBJECT

public:
    changeParameterClass(QVariant param, QString previousName, QDate mini);
    static QList<QVariant> newParameters(QWidget *parent, QVariant param, QString previousName, QDate mini);
    QString getAxisName();
    QString getPosCode();
    QString getExpCode();
    QString getDomCode();
    QString getPrevPosCode();
    QString getPrevExpCode();
    QString getPrevDomCode();
    int getParamCode();
    QDate getDate();

private:
    void urgentStop();

public slots:
    void validation();
    void cancelling();

private:
    QComboBox *position;
    QComboBox *exposition;
    QComboBox *dominance;
    QLineEdit *axisName;
    QLabel *prevName;
    QLabel *prevPos;
    QLabel *prevExp;
    QLabel *prevDom;
    QPushButton *validate;
    QPushButton *cancel;
    QList<QString> axisNames;
    int posCode;
    int expCode;
    int domCode;
    QVariant parameters;
    QDateEdit *dateEdit;
    int paramCode;
};

#endif // CHANGEPARAMETERCLASS

