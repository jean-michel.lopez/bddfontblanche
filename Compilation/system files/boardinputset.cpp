#include <MainWindow.h>

void MainWindow::boardInputSet()
{
    QString userName;
    userName = QInputDialog::getText(NULL, "Nom d'utilisateur", "Dis moi quel est ton nom");
    if (userName != "")
    {
        // We ask the user to give a folder to export the database
        QString folderName = QFileDialog::getExistingDirectory(NULL, "Dossier où exporter la base de données pour le terrain", "", QFileDialog::ShowDirsOnly);
        if (folderName != "")
        {
            folderName += "/terrain-" + QDate::currentDate().toString("dd.MM.yyyy");
            QDir().mkdir(folderName);

            // We save the database
            QString folderToSave;
            folderToSave = QString("%1-%2.BF").arg(QDateTime::currentDateTime().toString("dd.MM.yyyy.hh.mm.ss.zzz")).arg(userName);
            QDir originDir("bin"), targetDir("historic");
            targetDir.mkdir(folderToSave);
            targetDir.cd(folderToSave);
            QStringList fileList;
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("bin/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
            }
            originDir.cd("dictionary");
            targetDir.mkdir("dictionary");
            targetDir.cd("dictionary");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("bin/dictionary/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
            }

            // We duplicate the database (except historic)
            originDir = QDir("");
            targetDir.cd(folderName);
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(*it, QString("%1/%2").arg(targetDir.path()).arg(*it));
            }
            QFile stateFile(QString("%1/%2").arg(targetDir.path()).arg("state"));
            stateFile.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream in(&stateFile);
            in << 4;
            stateFile.close();
            originDir.cd("bin");
            targetDir.mkdir("bin");
            targetDir.cd("bin");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("bin/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
            }
            originDir.cd("dictionary");
            targetDir.mkdir("dictionary");
            targetDir.cd("dictionary");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("bin/dictionary/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
            }
            originDir.cd("../../platforms");
            targetDir.mkdir("../../platforms");
            targetDir.cd("../../platforms");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("platforms/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
            }

            // status change : 3 and 4
            *state = "3";
            stateFile.close();
            stateFile.reset();
            stateFile.setFileName("state");
            stateFile.open(QIODevice::WriteOnly | QIODevice::Text);
            in.reset();
            in.setDevice(&stateFile);
            in << 3;
            stateFile.close();

            // back to main
            backTo0Slot();
        }
    }
}
