#include <MainWindow.h>

void MainWindow::deleteParameter(int a, int b)
{
    if ((a!= -1) && (b!=-1))
    {
        if (QMessageBox::critical(NULL, "Attention", "Supprimer ce changement de paramètres.", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
        {
            QFile axisFile("bin/axisCharacteristics");
            int IdAxis;
            IdAxis = qobject_cast<QTableWidget*>(QObject::sender())->item(a, 0)->text().toInt();
            QString s;
            QStringList rawLine;
            bool ok(false);
            if (!axisFile.open(QIODevice::ReadWrite | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/axisCharacteristics.");
                urgentStop();
                return;
            }else{
                QTextStream axisStream(&axisFile);
                while (!axisStream.atEnd())
                {
                    rawLine = axisStream.readLine().split("\t");
                    if (rawLine.at(0).toInt(&ok) != IdAxis)
                    {
                        s += rawLine.join("\t") + "\n";
                    }else{
                        if (ok == false)
                        {
                            s += rawLine.join("\t") + "\n";
                        }
                    }
                }
                axisFile.resize(0);
                axisStream << s;
            }
            axisFile.close();
            qobject_cast<QTableWidget*>(QObject::sender())->removeRow(a);
        }
    }
}
