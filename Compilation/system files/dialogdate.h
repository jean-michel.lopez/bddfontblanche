#include <QDialog>
#include <QDate>
#include <QWidget>
#include <QString>
#include <QDate>
#include <QLabel>
#include <QPushButton>
#include <QCalendarWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>

#ifndef DIALOGDATE
#define DIALOGDATE

class DialogueDate : public QDialog
{
    Q_OBJECT

public:
    DialogueDate();

    static QDate getDate(QWidget *parent, QString title, QString text, QDate defaultDate, bool* ok);
    void setText(QString text);
    void setTitle(QString title);
    void setDate(QDate date);
    QDate getDate();

public slots:
    void majDate();

private:
    QLabel *m_text;
    QPushButton *buttonOk;
    QPushButton *buttonCancel;
    QCalendarWidget *calendar;
    QDate dateToSend;
};

#endif // DIALOGDATE
