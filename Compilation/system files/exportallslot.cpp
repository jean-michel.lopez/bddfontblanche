#include <MainWindow.h>

void MainWindow::exportAllSlot()
{
    QString fileName = QFileDialog::getSaveFileName(NULL, "Fichier de sortie", "", "Fichier CSV (*csv)");
    if (fileName != "")
    {
        if (!fileName.endsWith(".csv", Qt::CaseInsensitive))
        {
            fileName = QString("%1.csv").arg(fileName);
        }
        QMap<int, QMap<int, QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > > > > dataFrame;
        // Now we browse each data file
        // first list0
        QFile plotFile("bin/list0");
        int countLine(-1);
        QMap<QString, int> plotPosition;
        QMap<int, QStringList> plotMap;
        int Id;
        QString line;
        QStringList dataRow;
        if (!plotFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list0.");
            urgentStop();
            return;
        }else{
            QTextStream plotStream(&plotFile);
            line = plotStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list0\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!plotStream.atEnd()  && line != "###")
                {
                    line = plotStream.readLine();
                    plotPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!plotStream.atEnd())
                {
                    dataRow = plotStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    plotMap.insert(Id, dataRow);
                    dataFrame.insert(Id, *(new QMap<int, QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > > >));
                }
            }
            // then list1
            QFile treeFile("bin/list1");
            countLine = -1;
            QMap<QString, int> treePosition;
            QMap<int, QStringList> treeMap;
            if (!treeFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list1.");
                urgentStop();
                return;
            }else{
                QTextStream treeStream(&treeFile);
                line = treeStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list1\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = "";
                    while (!treeStream.atEnd()  && line != "###")
                    {
                        line = treeStream.readLine();
                        treePosition.insert(line.split("\t").at(0), countLine);
                        countLine++;
                    }
                    while(!treeStream.atEnd())
                    {
                        dataRow = treeStream.readLine().split("\t");
                        Id = dataRow.at(0).toInt();
                        dataRow.removeFirst();
                        treeMap.insert(Id, dataRow);
                        if (dataRow.size() > treePosition["Id0"]) dataFrame[dataRow.at(treePosition["Id0"]).toInt()].insert(Id, *(new QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > >));
                    }
                }
                // then list2
                QFile axisFile("bin/list2");
                countLine = -1;
                QMap<QString, int> axisPosition;
                QMap<int, QStringList> axisMap;
                if (!axisFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                    urgentStop();
                    return;
                }else{
                    QTextStream axisStream(&axisFile);
                    line = axisStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = "";
                        while (!axisStream.atEnd()  && line != "###")
                        {
                            line = axisStream.readLine();
                            axisPosition.insert(line.split("\t").at(0), countLine);
                            countLine++;
                        }
                        while(!axisStream.atEnd())
                        {
                            dataRow = axisStream.readLine().split("\t");
                            Id = dataRow.at(0).toInt();
                            dataRow.removeFirst();
                            axisMap.insert(Id, dataRow);
                            if (dataRow.size() > axisPosition["Id1"])
                            {
                                if (treeMap[dataRow.at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"])
                                {
                                    dataFrame[treeMap[dataRow.at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][dataRow.at(axisPosition["Id1"]).toInt()].insert(Id, *(new QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > >));
                                }
                            }
                        }
                    }
                    // then position
                    QFile posFile("bin/dictionary/position");
                    countLine = -1;
                    QMap<QString, int> posPosition;
                    QMap<int, QStringList> posMap;
                    if (!posFile.open(QIODevice::ReadOnly | QIODevice::Text))
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/position.");
                        urgentStop();
                        return;
                    }else{
                        QTextStream posStream(&posFile);
                        line = posStream.readLine();
                        if (line != "###")
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/position\nMauvaise entête (\"###\" attendu)");
                            urgentStop();
                            return;
                        }else{
                            line = "";
                            while (!posStream.atEnd()  && line != "###")
                            {
                                line = posStream.readLine();
                                posPosition.insert(line.split("\t").at(0), countLine);
                                countLine++;
                            }
                            while(!posStream.atEnd())
                            {
                                dataRow = posStream.readLine().split("\t");
                                Id = dataRow.at(0).toInt();
                                dataRow.removeFirst();
                                posMap.insert(Id, dataRow);
                            }
                        }
                        // then exposition
                        QFile expFile("bin/dictionary/exposition");
                        countLine = -1;
                        QMap<QString, int> expPosition;
                        QMap<int, QStringList> expMap;
                        if (!expFile.open(QIODevice::ReadOnly | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/exposition.");
                            urgentStop();
                            return;
                        }else{
                            QTextStream expStream(&expFile);
                            line = expStream.readLine();
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/exposition\nMauvaise entête (\"###\" attendu)");
                                urgentStop();
                                return;
                            }else{
                                line = "";
                                while (!expStream.atEnd()  && line != "###")
                                {
                                    line = expStream.readLine();
                                    expPosition.insert(line.split("\t").at(0), countLine);
                                    countLine++;
                                }
                                while(!expStream.atEnd())
                                {
                                    dataRow = expStream.readLine().split("\t");
                                    Id = dataRow.at(0).toInt();
                                    dataRow.removeFirst();
                                    expMap.insert(Id, dataRow);
                                }
                            }
                            // then dominance
                            QFile domFile("bin/dictionary/dominance");
                            countLine = -1;
                            QMap<QString, int> domPosition;
                            QMap<int, QStringList> domMap;
                            if (!domFile.open(QIODevice::ReadOnly | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/dominance.");
                                urgentStop();
                                return;
                            }else{
                                QTextStream domStream(&domFile);
                                line = domStream.readLine();
                                if (line != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/dominance\nMauvaise entête (\"###\" attendu)");
                                    urgentStop();
                                    return;
                                }else{
                                    line = "";
                                    while (!domStream.atEnd()  && line != "###")
                                    {
                                        line = domStream.readLine();
                                        domPosition.insert(line.split("\t").at(0), countLine);
                                        countLine++;
                                    }
                                    while(!domStream.atEnd())
                                    {
                                        dataRow = domStream.readLine().split("\t");
                                        Id = dataRow.at(0).toInt();
                                        dataRow.removeFirst();
                                        domMap.insert(Id, dataRow);
                                    }
                                }
                                // then status
                                QFile statFile("bin/dictionary/status");
                                countLine = -1;
                                QMap<QString, int> statPosition;
                                QMap<int, QStringList> statMap;
                                if (!statFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/status.");
                                    urgentStop();
                                    return;
                                }else{
                                    QTextStream statStream(&statFile);
                                    line = statStream.readLine();
                                    if (line != "###")
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/status\nMauvaise entête (\"###\" attendu)");
                                        urgentStop();
                                        return;
                                    }else{
                                        line = "";
                                        while (!statStream.atEnd()  && line != "###")
                                        {
                                            line = statStream.readLine();
                                            statPosition.insert(line.split("\t").at(0), countLine);
                                            countLine++;
                                        }
                                        while(!statStream.atEnd())
                                        {
                                            dataRow = statStream.readLine().split("\t");
                                            Id = dataRow.at(0).toInt();
                                            dataRow.removeFirst();
                                            statMap.insert(Id, dataRow);
                                        }
                                    }
                                    // then list3
                                    QFile GUFile("bin/list3");
                                    countLine = -1;
                                    QMap<QString, int> GUPosition;
                                    QMap<int, QStringList> GUMap;
                                    if (!GUFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list3.");
                                        urgentStop();
                                        return;
                                    }else{
                                        QTextStream GUStream(&GUFile);
                                        line = GUStream.readLine();
                                        if (line != "###")
                                        {
                                            // Error
                                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list3\nMauvaise entête (\"###\" attendu)");
                                            urgentStop();
                                            return;
                                        }else{
                                            line = "";
                                            while (!GUStream.atEnd()  && line != "###")
                                            {
                                                line = GUStream.readLine();
                                                GUPosition.insert(line.split("\t").at(0), countLine);
                                                countLine++;
                                            }
                                            while(!GUStream.atEnd())
                                            {
                                                dataRow = GUStream.readLine().split("\t");
                                                Id = dataRow.at(0).toInt();
                                                dataRow.removeFirst();
                                                GUMap.insert(Id, dataRow);
                                                if (dataRow.size() > GUPosition["Id2"])
                                                {
                                                    if (axisMap[dataRow.at(GUPosition["Id2"]).toInt()].size() > axisPosition["Id1"])
                                                    {
                                                        if (treeMap[axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"])
                                                        {
                                                            dataFrame[treeMap[axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][dataRow.at(GUPosition["Id2"]).toInt()].insert(Id, *(new QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > >));
                                                            dataFrame[treeMap[axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][dataRow.at(GUPosition["Id2"]).toInt()][Id].second.first.first = -1; // -1 = No male reproduction
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // then list4
                                        QFile reproFile("bin/list4");
                                        countLine = -1;
                                        QMap<QString, int> reproPosition;
                                        QMap<int, QStringList> reproMap;
                                        int maxMale(0), maxFemale(0);
                                        if (!reproFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                        {
                                            // Error
                                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
                                            urgentStop();
                                            return;
                                        }else{
                                            QTextStream reproStream(&reproFile);
                                            line = reproStream.readLine();
                                            if (line != "###")
                                            {
                                                // Error
                                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list4\nMauvaise entête (\"###\" attendu)");
                                                urgentStop();
                                                return;
                                            }else{
                                                line = "";
                                                while (!reproStream.atEnd()  && line != "###")
                                                {
                                                    line = reproStream.readLine();
                                                    reproPosition.insert(line.split("\t").at(0), countLine);
                                                    countLine++;
                                                }
                                                while(!reproStream.atEnd())
                                                {
                                                    dataRow = reproStream.readLine().split("\t");
                                                    Id = dataRow.at(0).toInt();
                                                    dataRow.removeFirst();
                                                    reproMap.insert(Id, dataRow);
                                                    if (dataRow.size() > reproPosition["IdSex"] && dataRow.size() > reproPosition["Id3"])
                                                    {
                                                        if (GUMap[dataRow.at(reproPosition["Id3"]).toInt()].size() > GUPosition["Id2"])
                                                        {
                                                            if (axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].size() > axisPosition["Id1"])
                                                            {
                                                                if (treeMap[axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"])
                                                                {
                                                                    if (dataRow.at(reproPosition["IdSex"]) == "1")
                                                                    {
                                                                        dataFrame[treeMap[axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][dataRow.at(reproPosition["Id3"]).toInt()].second.first.first = Id;
                                                                        maxMale = 1;
                                                                    }else{
                                                                        dataFrame[treeMap[axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][dataRow.at(reproPosition["Id3"]).toInt()].second.second.insert(Id, *(new QMap<QString, int>));
                                                                        if (dataFrame[treeMap[axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][dataRow.at(reproPosition["Id3"]).toInt()].second.second.size() > maxFemale) maxFemale = dataFrame[treeMap[axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][dataRow.at(reproPosition["Id3"]).toInt()].second.second.size();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            // then sex
                                            QFile sexFile("bin/dictionary/sex");
                                            countLine = -1;
                                            QMap<QString, int> sexPosition;
                                            QMap<int, QStringList> sexMap;
                                            if (!sexFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                            {
                                                // Error
                                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/sex.");
                                                urgentStop();
                                                return;
                                            }else{
                                                QTextStream sexStream(&sexFile);
                                                line = sexStream.readLine();
                                                if (line != "###")
                                                {
                                                    // Error
                                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/sex\nMauvaise entête (\"###\" attendu)");
                                                    urgentStop();
                                                    return;
                                                }else{
                                                    line = "";
                                                    while (!sexStream.atEnd()  && line != "###")
                                                    {
                                                        line = sexStream.readLine();
                                                        sexPosition.insert(line.split("\t").at(0), countLine);
                                                        countLine++;
                                                    }
                                                    while(!sexStream.atEnd())
                                                    {
                                                        dataRow = sexStream.readLine().split("\t");
                                                        Id = dataRow.at(0).toInt();
                                                        dataRow.removeFirst();
                                                        sexMap.insert(Id, dataRow);
                                                    }
                                                }
                                                // then variables
                                                QFile varFile("bin/dictionary/variables");
                                                countLine = -1;
                                                QMap<QString, int> varPosition;
                                                QMap<int, QStringList> varMap;
                                                QFile * tempFile;
                                                QMap<QString, QMap<QString, int> > factorPosition;
                                                QMap<QString, QMap<int, QStringList> > factorMap;
                                                QMap<QString, int> tempPosition;
                                                QMap<int, QStringList> tempMap;
                                                int tempId;
                                                if (!varFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                {
                                                    // Error
                                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/variables.");
                                                    urgentStop();
                                                    return;
                                                }else{
                                                    QTextStream varStream(&varFile);
                                                    line = varStream.readLine();
                                                    if (line != "###")
                                                    {
                                                        // Error
                                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/variables\nMauvaise entête (\"###\" attendu)");
                                                        urgentStop();
                                                        return;
                                                    }else{
                                                        line = "";
                                                        while (!varStream.atEnd()  && line != "###")
                                                        {
                                                            line = varStream.readLine();
                                                            varPosition.insert(line.split("\t").at(0), countLine);
                                                            countLine++;
                                                        }
                                                        while(!varStream.atEnd())
                                                        {
                                                            dataRow = varStream.readLine().split("\t");
                                                            Id = dataRow.at(0).toInt();
                                                            dataRow.removeFirst();
                                                            varMap.insert(Id, dataRow);
                                                            if (varMap[Id].at(varPosition["TypeVar"]).startsWith("eKEY"))
                                                            {
                                                                // factor variable
                                                                if (dataRow.size() > varPosition["TypeVar"]) if (dataRow.at(varPosition["TypeVar"]).split(":").size() > 1) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(varPosition["TypeVar"]).split(":").at(1)));
                                                                countLine = -1;
                                                                tempPosition.clear();
                                                                tempMap.clear();
                                                                if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    QTextStream tempStream(tempFile);
                                                                    line = tempStream.readLine();
                                                                    if (line != "###")
                                                                    {
                                                                        // Error
                                                                        QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                                                        urgentStop();
                                                                        return;
                                                                    }else{
                                                                        line = "";
                                                                        while (!tempStream.atEnd()  && line != "###")
                                                                        {
                                                                            line = tempStream.readLine();
                                                                            tempPosition.insert(line.split("\t").at(0), countLine);
                                                                            countLine++;
                                                                        }
                                                                        while(!tempStream.atEnd())
                                                                        {
                                                                            dataRow = tempStream.readLine().split("\t");
                                                                            tempId = dataRow.at(0).toInt();
                                                                            dataRow.removeFirst();
                                                                            tempMap.insert(tempId, dataRow);
                                                                        }
                                                                    }
                                                                }
                                                                tempFile->close();
                                                                if (varMap[Id].size() > varPosition["Name"])
                                                                {
                                                                    factorPosition.insert(varMap[Id].at(varPosition["Name"]), tempPosition);
                                                                    factorMap.insert(varMap[Id].at(varPosition["Name"]), tempMap);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // then reproduction variables
                                                    QFile reproVarFile("bin/dictionary/reproductionVariables");
                                                    countLine = -1;
                                                    QMap<QString, int> reproVarPosition;
                                                    QMap<int, QStringList> reproVarMap;
                                                    if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                    {
                                                        // Error
                                                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
                                                        urgentStop();
                                                        return;
                                                    }else{
                                                        QTextStream reproVarStream(&reproVarFile);
                                                        line = reproVarStream.readLine();
                                                        if (line != "###")
                                                        {
                                                            // Error
                                                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
                                                            urgentStop();
                                                            return;
                                                        }else{
                                                            line = "";
                                                            while (!reproVarStream.atEnd()  && line != "###")
                                                            {
                                                                line = reproVarStream.readLine();
                                                                reproVarPosition.insert(line.split("\t").at(0), countLine);
                                                                countLine++;
                                                            }
                                                            while(!reproVarStream.atEnd())
                                                            {
                                                                dataRow = reproVarStream.readLine().split("\t");
                                                                Id = dataRow.at(0).toInt();
                                                                dataRow.removeFirst();
                                                                reproVarMap.insert(Id, dataRow);
                                                                if (reproVarMap[Id].size() > reproVarPosition["TypeVar"])
                                                                {
                                                                    if (reproVarMap[Id].at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                    {
                                                                        // factor variable
                                                                        if (dataRow.size() > reproVarPosition["TypeVar"]) if (dataRow.at(reproVarPosition["TypeVar"]).split(":").size() > 1) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproVarPosition["TypeVar"]).split(":").at(1)));
                                                                        countLine = -1;
                                                                        tempPosition.clear();
                                                                        tempMap.clear();
                                                                        if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                                                        {
                                                                            // Error
                                                                            QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                                                            urgentStop();
                                                                            return;
                                                                        }else{
                                                                            QTextStream tempStream(tempFile);
                                                                            line = tempStream.readLine();
                                                                            if (line != "###")
                                                                            {
                                                                                // Error
                                                                                QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                                                                urgentStop();
                                                                                return;
                                                                            }else{
                                                                                line = "";
                                                                                while (!tempStream.atEnd()  && line != "###")
                                                                                {
                                                                                    line = tempStream.readLine();
                                                                                    tempPosition.insert(line.split("\t").at(0), countLine);
                                                                                    countLine++;
                                                                                }
                                                                                while(!tempStream.atEnd())
                                                                                {
                                                                                    dataRow = tempStream.readLine().split("\t");
                                                                                    tempId = dataRow.at(0).toInt();
                                                                                    dataRow.removeFirst();
                                                                                    tempMap.insert(tempId, dataRow);
                                                                                }
                                                                            }
                                                                        }
                                                                        tempFile->close();
                                                                        if (reproVarMap[Id].size() > reproVarPosition["Name"])
                                                                        {
                                                                            factorPosition.insert(reproVarMap[Id].at(reproVarPosition["Name"]), tempPosition);
                                                                            factorMap.insert(reproVarMap[Id].at(reproVarPosition["Name"]), tempMap);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        // then we load the measures
                                                        QFile measureFile("bin/measure");
                                                        countLine = -1;
                                                        QMap<QString, int> measurePosition;
                                                        QMap<int, QStringList> measureMap;
                                                        if (!measureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                        {
                                                            // Error
                                                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/measure.");
                                                            urgentStop();
                                                            return;
                                                        }else{
                                                            QTextStream measureStream(&measureFile);
                                                            line = measureStream.readLine();
                                                            if (line != "###")
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/measure\nMauvaise entête (\"###\" attendu)");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                line = "";
                                                                while (!measureStream.atEnd()  && line != "###")
                                                                {
                                                                    line = measureStream.readLine();
                                                                    measurePosition.insert(line.split("\t").at(0), countLine);
                                                                    countLine++;
                                                                }
                                                                while(!measureStream.atEnd())
                                                                {
                                                                    dataRow = measureStream.readLine().split("\t");
                                                                    Id = dataRow.at(0).toInt();
                                                                    dataRow.removeFirst();
                                                                    measureMap.insert(Id, dataRow);
                                                                    if (dataRow.size() > measurePosition["Id3"])
                                                                    {
                                                                        if (GUMap[dataRow.at(measurePosition["Id3"]).toInt()].size() > GUPosition["Id2"])
                                                                        {
                                                                            if (axisMap[GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].size() > axisPosition["Id1"])
                                                                            {
                                                                                if (treeMap[axisMap[GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"])
                                                                                {
                                                                                    if (dataRow.size() > measurePosition["DateM"]) dataFrame[treeMap[axisMap[GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][dataRow.at(measurePosition["Id3"]).toInt()].first.insert(dataRow.at(measurePosition["DateM"]), Id);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            // and then we load the reproduction measures
                                                            QFile reproMeasureFile("bin/reproductionMeasure");
                                                            countLine = -1;
                                                            QMap<QString, int> reproMeasurePosition;
                                                            QMap<int, QStringList> reproMeasureMap;
                                                            if (!reproMeasureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                QTextStream reproMeasureStream(&reproMeasureFile);
                                                                line = reproMeasureStream.readLine();
                                                                if (line != "###")
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/reproductionMeasure\nMauvaise entête (\"###\" attendu)");
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    line = "";
                                                                    while (!reproMeasureStream.atEnd()  && line != "###")
                                                                    {
                                                                        line = reproMeasureStream.readLine();
                                                                        reproMeasurePosition.insert(line.split("\t").at(0), countLine);
                                                                        countLine++;
                                                                    }
                                                                    while(!reproMeasureStream.atEnd())
                                                                    {
                                                                        dataRow = reproMeasureStream.readLine().split("\t");
                                                                        Id = dataRow.at(0).toInt();
                                                                        dataRow.removeFirst();
                                                                        reproMeasureMap.insert(Id, dataRow);
                                                                        if (dataRow.size() > reproMeasurePosition["Id4"])
                                                                        {
                                                                            if (reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].size() > reproPosition["IdSex"])
                                                                            {
                                                                                if (GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].size() > GUPosition["Id2"])
                                                                                {
                                                                                    if (axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].size() > axisPosition["Id1"])
                                                                                    {
                                                                                        if (treeMap[axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"])
                                                                                        {
                                                                                            if (dataRow.size() > reproMeasurePosition["DateRM"])
                                                                                            {
                                                                                                if (reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["IdSex"]) == "1")
                                                                                                {
                                                                                                    dataFrame[treeMap[axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].second.first.second.insert(dataRow.at(reproMeasurePosition["DateRM"]), Id);
                                                                                                }else{
                                                                                                    dataFrame[treeMap[axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].second.second[dataRow.at(reproMeasurePosition["Id4"]).toInt()].insert(dataRow.at(reproMeasurePosition["DateRM"]), Id);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                // -> now we load each characteristics modification for the axises concerned
                                                                QFile paramFile("bin/axisCharacteristics");
                                                                QMap<int, QMap<QString, QStringList > > charactMap;
                                                                QMap<QString, int> charactPosition;
                                                                countLine = -1;
                                                                if (!paramFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/axisCharacteristics.");
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    QTextStream paramStream(&paramFile);
                                                                    line = paramStream.readLine();
                                                                    if (line != "###")
                                                                    {
                                                                        // Error
                                                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/axisCharacteristics\nMauvaise entête (\"###\" attendu)");
                                                                        urgentStop();
                                                                        return;
                                                                    }else{
                                                                        line = "";
                                                                        while (!paramStream.atEnd()  && line != "###")
                                                                        {
                                                                            line = paramStream.readLine();
                                                                            if (line != "###")
                                                                            {
                                                                                charactPosition.insert(line.split("\t").at(0), countLine);
                                                                                countLine++;
                                                                            }
                                                                        }
                                                                        while(!paramStream.atEnd())
                                                                        {
                                                                            dataRow = paramStream.readLine().split("\t");
                                                                            dataRow.removeFirst();
                                                                            if (dataRow.size() > charactPosition["Id2"]) Id = dataRow.at(charactPosition["Id2"]).toInt();
                                                                            if (!charactMap.keys().contains(Id)) charactMap.insert(Id,*(new QMap<QString, QStringList>));
                                                                            if (dataRow.size() > charactPosition["Date"]) charactMap[Id].insert(dataRow.at(charactPosition["Date"]), dataRow);
                                                                        }
                                                                    }
                                                                    paramFile.close();
                                                                }
                                                                // then we can write the file
                                                                QFile outputFile(fileName);
                                                                if (!outputFile.open(QIODevice::WriteOnly))
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à écrire le fichier demandé : %1.").arg(fileName));
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    // file header
                                                                    QTextStream outputStream(&outputFile);
                                                                    outputStream << "Placette\tEspece\tNumeroArbre\tPosition\tExposition\tDominance\tNumeroRameau\tStatut\tIdRameau\tAnUC\tNumeroUC\tDate";
                                                                    for (QMap<int, QStringList>::iterator it(varMap.begin()); it != varMap.end(); it++)
                                                                    {
                                                                        if (it->size() > varPosition["TypeVar"] && it->size() > varPosition["MediumName"])
                                                                        {
                                                                            if (it->at(varPosition["TypeVar"]).startsWith("eKEY"))
                                                                            {
                                                                                if (it->size() > varPosition["ShortName"]) outputStream << "\t" << it->at(varPosition["MediumName"]) << "\tExplication" << it->at(varPosition["ShortName"]);
                                                                            }else{
                                                                                outputStream << "\t" << it->at(varPosition["MediumName"]);
                                                                            }
                                                                        }
                                                                    }
                                                                    QString tempName;
                                                                    if (maxMale > 0)
                                                                    {
                                                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                        {
                                                                            if (it->size() > reproVarPosition["Target"] & it->size() > reproVarPosition["MediumName"])
                                                                            {
                                                                                if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male"))
                                                                                {
                                                                                    if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                    {
                                                                                        tempName = it->at(reproVarPosition["MediumName"]);
                                                                                        tempName.replace("#", "Male");
                                                                                        outputStream << "\t" << tempName << "\tExplication";
                                                                                        if (it->size() > reproVarPosition["ShortName"])
                                                                                        {
                                                                                            tempName = it->at(reproVarPosition["ShortName"]);
                                                                                            tempName.replace("#", "Male");
                                                                                            outputStream << tempName;
                                                                                        }
                                                                                    }else{
                                                                                        tempName = it->at(reproVarPosition["MediumName"]);
                                                                                        tempName.replace("#", "Male");
                                                                                        outputStream << "\t" << tempName;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    for (int i(0); i < maxFemale; i++)
                                                                    {
                                                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                        {
                                                                            if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["Typevar"] && it->size() > reproVarPosition["MediumName"])
                                                                            {
                                                                                if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                {
                                                                                    if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                    {
                                                                                        tempName = it->at(reproVarPosition["MediumName"]);
                                                                                        tempName.replace("#", QString("Fem%1").arg(i + 1));
                                                                                        outputStream << "\t" << tempName << "\tExplication";
                                                                                        if (it->size() > reproVarPosition["ShortName"])
                                                                                        {
                                                                                            tempName = it->at(reproVarPosition["ShortName"]);
                                                                                            tempName.replace("#", QString("Fem%1").arg(i + 1));
                                                                                            outputStream << tempName;
                                                                                        }
                                                                                    }else{
                                                                                        tempName = it->at(reproVarPosition["MediumName"]);
                                                                                        tempName.replace("#", QString("Fem%1").arg(i + 1));
                                                                                        outputStream << "\t" << tempName;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    QString currDate;
                                                                    for (QMap<int, QMap<int, QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > > > >::iterator plot(dataFrame.begin()); plot != dataFrame.end(); plot++)
                                                                    {
                                                                        for (QMap<int, QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > > >::iterator tree(plot->begin()); tree != plot->end(); tree++)
                                                                        {
                                                                            for (QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > >::iterator axis(tree->begin()); axis != tree->end(); axis++)
                                                                            {
                                                                                for (QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > >::iterator gu(axis->begin()); gu != axis->end(); gu++)
                                                                                {
                                                                                    // first we write each measure (browsed by its date)
                                                                                    for (QMap<QString, int>::iterator measure(gu->first.begin()); measure != gu->first.end(); measure++)
                                                                                    {
                                                                                        if (plotMap[plot.key()].size() > plotPosition["Name"])
                                                                                        {
                                                                                            outputStream << "\n" << plotMap[plot.key()].at(plotPosition["Name"]) << "\tPin d'Alep\t";
                                                                                        }else{
                                                                                            outputStream << "\n" << "" << "\tPin d'Alep\t";
                                                                                        }
                                                                                        if (treeMap[tree.key()].size() > treePosition["NumArb"])
                                                                                        {
                                                                                            outputStream << treeMap[tree.key()].at(treePosition["NumArb"]) << "\t";
                                                                                        }else{
                                                                                            outputStream << "" << "\t";
                                                                                        }
                                                                                        if (charactMap.keys().contains(axis.key()))
                                                                                        {
                                                                                            currDate = "";
                                                                                            for (QMap<QString, QStringList>::iterator it(charactMap[axis.key()].begin()); it != charactMap[axis.key()].end(); it++)
                                                                                            {
                                                                                                if (it.key().split(".").size() > 2 && measure.key().split(".").size() > 2)
                                                                                                {
                                                                                                    if (QDate(it.key().split(".").at(0).toInt(), it.key().split(".").at(1).toInt(), it.key().split(".").at(2).toInt()) <= QDate(measure.key().split(".").at(0).toInt(), measure.key().split(".").at(1).toInt(), measure.key().split(".").at(2).toInt()))
                                                                                                    {
                                                                                                        currDate = it.key();
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            if (currDate != "")
                                                                                            {
                                                                                                if (charactMap[axis.key()][currDate].size() > charactPosition["CurrPos"])
                                                                                                {
                                                                                                    if (posMap[charactMap[axis.key()][currDate].at(charactPosition["CurrPos"]).toInt()].size() > posPosition["LongNamePosition"])
                                                                                                    {
                                                                                                        outputStream << posMap[charactMap[axis.key()][currDate].at(charactPosition["CurrPos"]).toInt()].at(posPosition["LongNamePosition"]) << "\t";
                                                                                                    }else{
                                                                                                        outputStream << "" << "\t";
                                                                                                    }
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                                if (charactMap[axis.key()][currDate].size() > charactPosition["CurrExp"])
                                                                                                {
                                                                                                    if (expMap[charactMap[axis.key()][currDate].at(charactPosition["CurrExp"]).toInt()].size() > expPosition["LongNameExposition"])
                                                                                                    {
                                                                                                        outputStream << "" << "\t";
                                                                                                    }else{
                                                                                                        outputStream << "" << "\t";
                                                                                                    }
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                                if (charactMap[axis.key()][currDate].size() > charactPosition["CurrDom"])
                                                                                                {
                                                                                                    if (domMap[charactMap[axis.key()][currDate].at(charactPosition["CurrDom"]).toInt()].size() > domPosition["LongNameDominance"])
                                                                                                    {
                                                                                                        outputStream << domMap[charactMap[axis.key()][currDate].at(charactPosition["CurrDom"]).toInt()].at(domPosition["LongNameDominance"]) << "\t";
                                                                                                    }else{
                                                                                                        outputStream << "" << "\t";
                                                                                                    }
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                            }else{
                                                                                                if (axisMap[axis.key()].size() > charactPosition["IdPos"])
                                                                                                {
                                                                                                    if (posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].size() > posPosition["LongNamePosition"])
                                                                                                    {
                                                                                                        outputStream << posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].at(posPosition["LongNamePosition"]) << "\t";
                                                                                                    }else{
                                                                                                        outputStream << "" << "\t";
                                                                                                    }
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                                if (axisMap[axis.key()].size() > axisPosition["IdExp"])
                                                                                                {
                                                                                                    if (expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].size() > expPosition["LongNameExposition"])
                                                                                                    {
                                                                                                        outputStream << expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].at(expPosition["LongNameExposition"]) << "\t";
                                                                                                    }else{
                                                                                                        outputStream << "" << "\t";
                                                                                                    }
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                                if (axisMap[axis.key()].size() > axisPosition["IdDom"])
                                                                                                {
                                                                                                    if (domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].size() > domPosition["LongNameDominance"])
                                                                                                    {
                                                                                                        outputStream << domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].at(domPosition["LongNameDominance"]) << "\t";
                                                                                                    }else{
                                                                                                        outputStream << "" << "\t";
                                                                                                    }
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                            }
                                                                                        }else{
                                                                                            if (axisMap[axis.key()].size() > axisPosition["IdPos"])
                                                                                            {
                                                                                                if (posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].size() > posPosition["LongNamePosition"])
                                                                                                {
                                                                                                    outputStream << posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].at(posPosition["LongNamePosition"]) << "\t";
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                            }else{
                                                                                                outputStream << "" << "\t";
                                                                                            }
                                                                                            if (axisMap[axis.key()].size() > axisPosition["IdExp"])
                                                                                            {
                                                                                                if (expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].size() > expPosition["LongNameExposition"])
                                                                                                {
                                                                                                    outputStream << expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].at(expPosition["LongNameExposition"]) << "\t";
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                            }else{
                                                                                                outputStream << "" << "\t";
                                                                                            }
                                                                                            if (axisMap[axis.key()].size() > axisPosition["IdDom"])
                                                                                            {
                                                                                                if (domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].size() > domPosition["LongNameDominance"])
                                                                                                {
                                                                                                    outputStream << domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].at(domPosition["LongNameDominance"]) << "\t";
                                                                                                }else{
                                                                                                    outputStream << "" << "\t";
                                                                                                }
                                                                                            }else{
                                                                                                outputStream << "" << "\t";
                                                                                            }
                                                                                        }
                                                                                        if (axisMap[axis.key()].size() > axisPosition["NRam"])
                                                                                        {
                                                                                            outputStream << axisMap[axis.key()].at(axisPosition["NRam"]) << "\t";
                                                                                        }else{
                                                                                            outputStream << "" << "\t";
                                                                                        }
                                                                                        if (axisMap[axis.key()].size() > axisPosition["IdStat"])
                                                                                        {
                                                                                            if (statMap[axisMap[axis.key()].at(axisPosition["IdStat"]).toInt()].size() > statPosition["LongNameStatus"])
                                                                                            {
                                                                                                outputStream << statMap[axisMap[axis.key()].at(axisPosition["IdStat"]).toInt()].at(statPosition["LongNameStatus"]) << "\t";
                                                                                            }else{
                                                                                                outputStream << "" << "\t";
                                                                                            }
                                                                                        }else{
                                                                                            outputStream << "" << "\t";
                                                                                        }
                                                                                        if (charactMap.keys().contains(axis.key()))
                                                                                        {
                                                                                            currDate = charactMap[axis.key()].keys().last();
                                                                                            outputStream << charactMap[axis.key()][currDate].at(charactPosition["CurrName"]) << "\t";
                                                                                        }else{
                                                                                            outputStream << "PA" << treeMap[tree.key()].at(treePosition["NumArb"]) << " " << posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].at(posPosition["ShortNamePosition"]) << expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].at(expPosition["ShortNameExposition"]) << " " << domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].at(domPosition["ShortNameDominance"]) << axisMap[axis.key()].at(axisPosition["NRam"]) << "\t";
                                                                                        }
                                                                                        if (GUMap[gu.key()].size() > GUPosition["Year"])
                                                                                        {
                                                                                            outputStream << GUMap[gu.key()].at(GUPosition["Year"]) << "\t";
                                                                                        }else{
                                                                                            outputStream << "" << "\t";
                                                                                        }
                                                                                        if (GUMap[gu.key()].size() > GUPosition["NGU"])
                                                                                        {
                                                                                            outputStream << GUMap[gu.key()].at(GUPosition["NGU"]) << "\t";
                                                                                        }else{
                                                                                            outputStream << "" << "\t";
                                                                                        }
                                                                                        outputStream << measure.key();
                                                                                        for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                                                                        {
                                                                                            if (var->size() > varPosition["TypeVar"] && var->size() > varPosition["Name"])
                                                                                            {
                                                                                                if (measureMap[measure.value()].size() > measurePosition[var->at(varPosition["Name"])])
                                                                                                {
                                                                                                    if ((var->at(varPosition["TypeVar"]).startsWith("eKEY")) && (measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]) != ""))
                                                                                                    {
                                                                                                        outputStream << "\t" << factorMap[var->at(varPosition["Name"])][measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]).toInt()].at(factorPosition[var->at(varPosition["Name"])]["Code"]) << "\t" << factorMap[var->at(varPosition["Name"])][measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]).toInt()].at(factorPosition[var->at(varPosition["Name"])]["Explain"]);
                                                                                                    }else{
                                                                                                        outputStream << "\t" << measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]);
                                                                                                    }
                                                                                                }else{
                                                                                                    outputStream << "\t" << "";
                                                                                                }
                                                                                            }else{
                                                                                                outputStream << "\t" << "";
                                                                                            }
                                                                                        }
                                                                                        if (maxMale > 0)
                                                                                        {
                                                                                            Id = gu->second.first.first;
                                                                                            if (Id == -1)
                                                                                            {
                                                                                                for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                {
                                                                                                    if (it->size() > reproVarPosition["Target"])
                                                                                                    {
                                                                                                        if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male"))
                                                                                                        {
                                                                                                            outputStream << "\t";
                                                                                                            if (it->size() > varPosition["TypeVar"])
                                                                                                            {
                                                                                                                if (it->at(varPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                {
                                                                                                                    outputStream << "\t";
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }else{
                                                                                                        outputStream << "\t";
                                                                                                    }
                                                                                                }
                                                                                            }else{
                                                                                                if (gu->second.first.second.contains(measure.key()))
                                                                                                {
                                                                                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                    {
                                                                                                        if (it->size() > reproVarPosition["Target"])
                                                                                                        {
                                                                                                            if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male"))
                                                                                                            {
                                                                                                                if (it->size() > reproVarPosition["TypeVar"] && it->size() > reproVarPosition["Name"])
                                                                                                                {
                                                                                                                    if (reproMeasureMap[gu->second.first.second[measure.key()]].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                                    {
                                                                                                                        if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (reproMeasureMap[gu->second.first.second[measure.key()]].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]) != ""))
                                                                                                                        {
                                                                                                                            if (factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[gu->second.first.second[measure.key()]].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"] && factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[gu->second.first.second[measure.key()]].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Explain"]) outputStream << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[gu->second.first.second[measure.key()]].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]) << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[gu->second.first.second[measure.key()]].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Explain"]);
                                                                                                                        }else{
                                                                                                                            outputStream << "\t" << reproMeasureMap[gu->second.first.second[measure.key()]].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }else{
                                                                                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                    {
                                                                                                        if (it->size() > reproVarPosition["Target"])
                                                                                                        {
                                                                                                            if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male"))
                                                                                                            {
                                                                                                                outputStream << "\t";
                                                                                                                if (it->size() > varPosition["TypeVar"])
                                                                                                                {
                                                                                                                    if (it->at(varPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                    {
                                                                                                                        outputStream << "\t";
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        for (int i(0); i < maxFemale; i++)
                                                                                        {
                                                                                            if (gu->second.second.size() > i)
                                                                                            {
                                                                                                if (gu->second.second.keys().size() > i) tempId = gu->second.second.keys().at(i);
                                                                                                if (gu->second.second[tempId].contains(measure.key()))
                                                                                                {
                                                                                                    Id = gu->second.second[tempId][measure.key()];
                                                                                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                    {
                                                                                                        if (it->size() > reproVarPosition["Target"])
                                                                                                        {
                                                                                                            if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                            {
                                                                                                                if (it->size() > reproVarPosition["TypeVar"] && it->size() > reproVarPosition["Name"])
                                                                                                                {
                                                                                                                    if (reproMeasureMap[Id].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                                    {
                                                                                                                        if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]) != ""))
                                                                                                                        {
                                                                                                                            if (reproMeasureMap[Id].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                                            {
                                                                                                                                if (factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"] && factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Explain"])
                                                                                                                                {
                                                                                                                                    outputStream << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]) << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Explain"]);
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }else{
                                                                                                                            outputStream << "\t" << reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }else{
                                                                                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                    {
                                                                                                        if (it->size() > reproVarPosition["Target"])
                                                                                                        {
                                                                                                            if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                            {
                                                                                                                outputStream << "\t";
                                                                                                                if (it->size() > reproVarPosition["TypeVar"])
                                                                                                                {
                                                                                                                    if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                    {
                                                                                                                        outputStream << "\t";
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }else{
                                                                                                for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                {
                                                                                                    if (it->size() > reproVarPosition["Target"])
                                                                                                    {
                                                                                                        if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                        {
                                                                                                            outputStream << "\t";
                                                                                                            if (it->size() > reproVarPosition["TypeVar"])
                                                                                                            if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                            {
                                                                                                                outputStream << "\t";
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    // then we write each male measure if missing
                                                                                    Id = gu->second.first.first;
                                                                                    if (Id != -1)
                                                                                    {
                                                                                        for (QMap<QString, int>::iterator measureRepro(gu->second.first.second.begin()); measureRepro != gu->second.first.second.end(); measureRepro++)
                                                                                        {
                                                                                            if (!gu->first.contains(measureRepro.key()))
                                                                                            {
                                                                                                // we add the measure
                                                                                                if (plotMap[plot.key()].size() > plotPosition["Name"]) outputStream << "\n" << plotMap[plot.key()].at(plotPosition["Name"]) << "\tPin d'Alep\t";
                                                                                                if (treeMap[tree.key()].size() > treePosition["NumArb"]) outputStream << treeMap[tree.key()].at(treePosition["NumArb"]) << "\t";
                                                                                                if (axisMap[axis.key()].size() > axisPosition["IdPos"])
                                                                                                {
                                                                                                    if (posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].size() > posPosition["LongNamePosition"]) outputStream << posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].at(posPosition["LongNamePosition"]) << "\t";
                                                                                                }
                                                                                                if (axisMap[axis.key()].size() > axisPosition["IdExp"])
                                                                                                {
                                                                                                    if (expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].size() > posPosition["LongNameExposition"]) outputStream << expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].at(expPosition["LongNameExposition"]) << "\t";
                                                                                                }
                                                                                                if (axisMap[axis.key()].size() > axisPosition["IdDom"])
                                                                                                {
                                                                                                    if (domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].size() > posPosition["LongNameDominance"]) outputStream << domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].at(domPosition["LongNameDominance"]) << "\t";
                                                                                                }
                                                                                                if (axisMap[axis.key()].size() > axisPosition["NRam"]) outputStream << axisMap[axis.key()].at(axisPosition["NRam"]) << "\t";
                                                                                                if (axisMap[axis.key()].size() > axisPosition["IdStat"])
                                                                                                {
                                                                                                    if (statMap[axisMap[axis.key()].at(axisPosition["IdStat"]).toInt()].size() > statPosition["LongNameStatus"]) outputStream << statMap[axisMap[axis.key()].at(axisPosition["IdStat"]).toInt()].at(statPosition["LongNameStatus"]) << "\t";
                                                                                                }
                                                                                                if (treeMap[tree.key()].size() > treePosition["NumArb"] && axisMap[axis.key()].size() > axisPosition["IdPos"] && axisMap[axis.key()].size() > axisPosition["IdExp"] && axisMap[axis.key()].size() > axisPosition["IdDom"] && axisMap[axis.key()].size() > axisPosition["NRam"])
                                                                                                {
                                                                                                    if (posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].size() > posPosition["ShortNamePosition"] && posMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].size() > posPosition["ShortNameExposition"] && posMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].size() > posPosition["ShortNameDominance"])
                                                                                                    {
                                                                                                        outputStream << "PA" << treeMap[tree.key()].at(treePosition["NumArb"]) << " " << posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].at(posPosition["ShortNamePosition"]) << expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].at(expPosition["ShortNameExposition"]) << " " << domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].at(domPosition["ShortNameDominance"]) << axisMap[axis.key()].at(axisPosition["NRam"]) << "\t";
                                                                                                    }
                                                                                                }
                                                                                                if (GUMap[gu.key()].size() > GUPosition["Year"]) outputStream << GUMap[gu.key()].at(GUPosition["Year"]) << "\t";
                                                                                                if (GUMap[gu.key()].size() > GUPosition["NGU"]) outputStream << GUMap[gu.key()].at(GUPosition["NGU"]) << "\t";
                                                                                                outputStream << measureRepro.key();
                                                                                                for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                                                                                {
                                                                                                    if (var->size() > varPosition["TypeVar"])
                                                                                                    {
                                                                                                        if (var->at(varPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                        {
                                                                                                            outputStream << "\t\t";
                                                                                                        }else{
                                                                                                            outputStream << "\t";
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                {
                                                                                                    if (it->size() > reproVarPosition["Target"])
                                                                                                    {
                                                                                                        if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male"))
                                                                                                        {
                                                                                                            if (it->size() > reproVarPosition["TypeVar"] && it->size() > reproVarPosition["Name"])
                                                                                                            {
                                                                                                                if (reproMeasureMap[measureRepro.value()].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                                {
                                                                                                                    if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (reproMeasureMap[measureRepro.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]) != ""))
                                                                                                                    {
                                                                                                                        if (factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[measureRepro.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"] && factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[measureRepro.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Explain"])
                                                                                                                        {
                                                                                                                            outputStream << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[measureRepro.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]) << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[measureRepro.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Explain"]);
                                                                                                                        }
                                                                                                                    }else{
                                                                                                                        outputStream << "\t" << reproMeasureMap[measureRepro.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                for (int i(0); i < maxFemale; i++)
                                                                                                {
                                                                                                    if (gu->second.second.size() > i)
                                                                                                    {
                                                                                                        if (gu->second.second.keys().size() > i) tempId = gu->second.second.keys().at(i);
                                                                                                        if (gu->second.second[tempId].contains(measureRepro.key()))
                                                                                                        {
                                                                                                            Id = gu->second.second[tempId][measureRepro.key()];
                                                                                                            for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                            {
                                                                                                                if (it->size() > reproVarPosition["Target"])
                                                                                                                {
                                                                                                                    if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                                    {
                                                                                                                        if (it->size() > reproVarPosition["TypeVar"] && it->size() > reproVarPosition["Name"])
                                                                                                                        {
                                                                                                                            if (reproMeasureMap[Id].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                                            {
                                                                                                                                if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]) != ""))
                                                                                                                                {
                                                                                                                                    if (factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"] && factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Explain"]) outputStream << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]) << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Explain"]);
                                                                                                                                }else{
                                                                                                                                    outputStream << "\t" << reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }else{
                                                                                                            for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                            {
                                                                                                                if (it->size() > reproVarPosition["Target"])
                                                                                                                {
                                                                                                                    if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                                    {
                                                                                                                        outputStream << "\t";
                                                                                                                        if (it->size() > reproVarPosition["TypeVar"])
                                                                                                                        {
                                                                                                                            if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                            {
                                                                                                                                outputStream << "\t";
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }else{
                                                                                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                        {
                                                                                                            if (it->size() > reproVarPosition["Target"])
                                                                                                            {
                                                                                                                if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                                {
                                                                                                                    outputStream << "\t";
                                                                                                                    if (it->size() > reproVarPosition["TypeVar"])
                                                                                                                    {
                                                                                                                        if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                        {
                                                                                                                            outputStream << "\t";
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    // then we write each female measure if missing
                                                                                    for (int i(0); i < maxFemale; i++)
                                                                                    {
                                                                                        if (gu->second.second.size() > i)
                                                                                        {
                                                                                            if (gu->second.second.keys().size() > i) tempId = gu->second.second.keys().at(i);
                                                                                            for (QMap<QString, int>::iterator measureRepro(gu->second.second[tempId].begin()); measureRepro != gu->second.second[tempId].end(); measureRepro++)
                                                                                            {
                                                                                                if ((!gu->first.contains(measureRepro.key())) && (!gu->second.first.second.contains(measureRepro.key())))
                                                                                                {
                                                                                                    bool recursive = false;
                                                                                                    for (int j(0); j < i; j++)
                                                                                                    {
                                                                                                        if (gu->second.second.keys().size() > j)
                                                                                                        {
                                                                                                            if (gu->second.second[gu->second.second.keys().at(j)].contains(measureRepro.key()))
                                                                                                            {
                                                                                                                recursive = true;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                    if (!recursive)
                                                                                                    {
                                                                                                        // we add the measure
                                                                                                        if (plotMap[plot.key()].size() > plotPosition["Name"]) outputStream << "\n" << plotMap[plot.key()].at(plotPosition["Name"]) << "\tPin d'Alep\t";
                                                                                                        if (treeMap[tree.key()].size() > treePosition["NumArb"]) outputStream << treeMap[tree.key()].at(treePosition["NumArb"]) << "\t";
                                                                                                        if (axisMap[axis.key()].size() > axisPosition["IdPos"])
                                                                                                        {
                                                                                                            if (posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].size() > posPosition["LongNamePosition"])
                                                                                                            {
                                                                                                                outputStream << posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].at(posPosition["LongNamePosition"]) << "\t";
                                                                                                            }
                                                                                                        }
                                                                                                        if (axisMap[axis.key()].size() > axisPosition["IdExp"])
                                                                                                        {
                                                                                                            if (posMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].size() > posPosition["LongNameExposition"])
                                                                                                            {
                                                                                                                outputStream << expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].at(expPosition["LongNameExposition"]) << "\t";
                                                                                                            }
                                                                                                        }
                                                                                                        if (axisMap[axis.key()].size() > axisPosition["IdDom"])
                                                                                                        {
                                                                                                            if (posMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].size() > posPosition["LongNameDominance"])
                                                                                                            {
                                                                                                                outputStream << domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].at(domPosition["LongNameDominance"]) << "\t";
                                                                                                            }
                                                                                                        }
                                                                                                        if (axisMap[axis.key()].size() > axisPosition["NRam"]) outputStream << axisMap[axis.key()].at(axisPosition["NRam"]) << "\t";
                                                                                                        if (axisMap[axis.key()].size() > axisPosition["IdStat"])
                                                                                                        {
                                                                                                            if (statMap[axisMap[axis.key()].at(axisPosition["IdStat"]).toInt()].size() > statPosition["LongNameStatus"])
                                                                                                            {
                                                                                                                outputStream << statMap[axisMap[axis.key()].at(axisPosition["IdStat"]).toInt()].at(statPosition["LongNameStatus"]) << "\t";
                                                                                                            }
                                                                                                        }
                                                                                                        if (treeMap[tree.key()].size() > treePosition["NumArb"] && axisMap[axis.key()].size() > axisPosition["IdPos"] && axisMap[axis.key()].size() > axisPosition["IdExp"] && axisMap[axis.key()].size() > axisPosition["IdDom"] && axisMap[axis.key()].size() > axisPosition["NRam"])
                                                                                                        {
                                                                                                            if (posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].size() > posPosition["ShortNamePosition"] && expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].size() > expPosition["ShortNameExposition"] && domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].size() > domPosition["ShortNameDominance"])
                                                                                                            {
                                                                                                                outputStream << "PA" << treeMap[tree.key()].at(treePosition["NumArb"]) << " " << posMap[axisMap[axis.key()].at(axisPosition["IdPos"]).toInt()].at(posPosition["ShortNamePosition"]) << expMap[axisMap[axis.key()].at(axisPosition["IdExp"]).toInt()].at(expPosition["ShortNameExposition"]) << " " << domMap[axisMap[axis.key()].at(axisPosition["IdDom"]).toInt()].at(domPosition["ShortNameDominance"]) << axisMap[axis.key()].at(axisPosition["NRam"]) << "\t";
                                                                                                            }
                                                                                                        }
                                                                                                        if (GUMap[gu.key()].size() > GUPosition["Year"]) outputStream << GUMap[gu.key()].at(GUPosition["Year"]) << "\t";
                                                                                                        if (GUMap[gu.key()].size() > GUPosition["NGU"]) outputStream << GUMap[gu.key()].at(GUPosition["NGU"]) << "\t";
                                                                                                        outputStream << measureRepro.key();
                                                                                                        for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                                                                                        {
                                                                                                            if (var->size() > varPosition["TypeVar"])
                                                                                                            {
                                                                                                                if (var->at(varPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                {
                                                                                                                    outputStream << "\t\t";
                                                                                                                }else{
                                                                                                                    outputStream << "\t";
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                        {
                                                                                                            if (it->size() > reproVarPosition["Target"])
                                                                                                            {
                                                                                                                if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male"))
                                                                                                                {
                                                                                                                    if (it->size() > reproVarPosition["TypeVar"])
                                                                                                                    {
                                                                                                                        if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                        {
                                                                                                                            outputStream << "\t\t";
                                                                                                                        }else{
                                                                                                                            outputStream << "\t";
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                        for (int i(0); i < maxFemale; i++)
                                                                                                        {
                                                                                                            if (gu->second.second.size() > i)
                                                                                                            {
                                                                                                                if (gu->second.second.keys().size() > i) tempId = gu->second.second.keys().at(i);
                                                                                                                if (gu->second.second[tempId].contains(measureRepro.key()))
                                                                                                                {
                                                                                                                    Id = gu->second.second[tempId][measureRepro.key()];
                                                                                                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                                    {
                                                                                                                        if (it->size() > reproVarPosition["Target"])
                                                                                                                        {
                                                                                                                            if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                                            {
                                                                                                                                if (it->size() > reproVarPosition["TypeVar"] && it->size() > reproVarPosition["Name"])
                                                                                                                                {
                                                                                                                                    if (reproMeasureMap[Id].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                                                    {
                                                                                                                                        if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]) != ""))
                                                                                                                                        {
                                                                                                                                            if (factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"] && factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Explain"])
                                                                                                                                            {
                                                                                                                                                outputStream << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]) << "\t" << factorMap[it->at(reproVarPosition["Name"])][reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]).toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Explain"]);
                                                                                                                                            }
                                                                                                                                        }else{
                                                                                                                                            outputStream << "\t" << reproMeasureMap[Id].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                                    {
                                                                                                                        if (it->size() > reproVarPosition["Target"])
                                                                                                                        {
                                                                                                                            if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                                            {
                                                                                                                                outputStream << "\t";
                                                                                                                                if (it->size() > reproVarPosition["TypeVar"])
                                                                                                                                {
                                                                                                                                    if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                                    {
                                                                                                                                        outputStream << "\t";
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }else{
                                                                                                                for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                                                {
                                                                                                                    if (it->size() > reproVarPosition["Target"])
                                                                                                                    {
                                                                                                                        if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                                                        {
                                                                                                                            outputStream << "\t";
                                                                                                                            if (it->size() > reproVarPosition["TypeVar"])
                                                                                                                            {
                                                                                                                                if (it->at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                                                                                {
                                                                                                                                    outputStream << "\t";
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    QMessageBox::information(NULL, "Export terminé", "L'export des données est terminé, il s'est correctement déroulé");
                                                                }
                                                                outputFile.close();
                                                            }
                                                            reproMeasureFile.close();
                                                        }
                                                        measureFile.close();
                                                    }
                                                    reproVarFile.close();
                                                }
                                                varFile.close();
                                            }
                                            sexFile.close();
                                        }
                                        reproFile.close();
                                    }
                                    GUFile.close();
                                }
                                statFile.close();
                            }
                            domFile.close();
                        }
                        expFile.close();
                    }
                    posFile.close();
                }
                axisFile.close();
            }
            treeFile.close();
        }
        plotFile.close();
    }
}
