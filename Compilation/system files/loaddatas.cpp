#include <MainWindow.h>

void MainWindow::loadDatas()
{
    // We first need to know which save was selected
    QTableWidget *saveTable;
    saveTable = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(1, 0)->widget());
    if (saveTable->currentRow() > -1)
    {
        QString folder(saveTable->item(saveTable->currentRow(), 0)->data(Qt::UserRole).toString());

        if (QMessageBox::information(NULL, "Charger la sauvegarde?", "Merci de me confirmer le chargement de la sauvegarde sélectionnée.", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
        {
            // We save the database
            QString userName("");
            userName = QInputDialog::getText(NULL, "Nom de l'opérateur", "Dis moi quel est ton nom?");
            userName.remove("\n");
            userName.remove("\t");
            QString folderName;
            folderName = QString("%1-%2.BCV").arg(QDateTime::currentDateTime().toString("dd.MM.yyyy.hh.mm.ss.zzz")).arg(userName);
            QDir originDir("bin"), targetDir("historic");
            targetDir.mkdir(folderName);
            targetDir.cd(folderName);
            QStringList fileList;
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("bin/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
            }
            originDir.cd("dictionary");
            targetDir.mkdir("dictionary");
            targetDir.cd("dictionary");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("bin/dictionary/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
            }

            // Then we delete the current contents of bin and dictionary
            originDir.setPath("bin");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::remove("bin/" + *it);
            }
            originDir.cd("dictionary");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::remove("bin/dictionary/" + *it);
            }

            // Then we copy the content of bin and dictionary
            originDir.setPath("historic/" + folder);
            targetDir.setPath("bin");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("%1/%2").arg(originDir.path()).arg(*it), QString("bin/%1").arg(*it));
            }
            originDir.cd("dictionary");
            targetDir.cd("dictionary");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("%1/%2").arg(originDir.path()).arg(*it), QString("bin/dictionary/%1").arg(*it));
            }

            // Then we update the view
            backTo0Slot();
        }
    }
}
