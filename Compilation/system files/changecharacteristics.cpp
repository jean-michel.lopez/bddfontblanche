#include <MainWindow.h>

void MainWindow::changeCharacteristics()
{
    // We need to know which plot was selected to fill the tables
    QString plot(QObject::sender()->objectName());

    // Empty mainLayout
    while(mainLayout->count() > 0){
        QLayoutItem *item = mainLayout->takeAt(0);
        delete item->widget();
        delete item;
    }
    delete mainLayout;
    mainLayout = new QGridLayout;
    setLayout(mainLayout);

    // Three tables : Followed, Comment and abandonned
    QLabel *title = new QLabel(plot);
    QLabel *labelTab0 = new QLabel("Axes suivis");
    QLabel *labelTab1 = new QLabel("Commentaires");
    QLabel *labelTab2 = new QLabel("Axes abandonnés");
    QTableWidget *tab0;
    QTableWidget *tab2;

    // Button back to previous and back to main
    QPushButton *buttonBackToBack = new QPushButton("Retour à la fenêtre précédente");
    QPushButton *buttonBackToMain = new QPushButton("Retour à la fenêtre principale");

    // Filling the tables
    // -> we first find the Id corresponding to the plot, on the file list0
    QFile plotFile("bin/list0");
    QString line;
    int countLine(0);
    QStringList dataRow;
    int plotCode(-1);
    int namePosition(-1);
    if (!plotFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list0.");
        urgentStop();
        return;
    }else{
        QTextStream plotStream(&plotFile);
        line = plotStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list0\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!plotStream.atEnd() && line != "###")
            {
                line = plotStream.readLine();
                if (line == "Name\tSTRING")
                {
                    namePosition = countLine;
                }
                countLine++;
            }
            if (namePosition == -1)
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/list0\nLe nom des placettes n'est pas trouvé dans le fichier.");
                urgentStop();
                return;
            }else{
                while (!plotStream.atEnd() && plotCode == -1)
                {
                    dataRow = plotStream.readLine().split("\t");
                    if (dataRow.size() > namePosition)
                    {
                        if (dataRow.at(namePosition) == plot)
                        {
                            plotCode = dataRow.at(0).toInt();
                        }
                    }
                }
                if (plotCode == -1)
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture du fichier : bin/list0\nLa placette %1 n'a pas été trouvée").arg(plot));
                    urgentStop();
                    return;
                }
            }
        }
        plotFile.close();
    }

    // -> then we keep, from the file list1, only the trees located on the plot selected
    QFile treeFile("bin/list1");
    QMap<int, int> treeMap;
    int treePosition(-1);
    if (!treeFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list1.");
        urgentStop();
        return;
    }else{
        QTextStream treeStream(&treeFile);
        line = treeStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list1\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            countLine = 0;
            int plotPosition(-1);
            while (!treeStream.atEnd() && line != "###")
            {
                line = treeStream.readLine();
                if (line == "NumArb\tINTEGER")
                {
                    treePosition = countLine;
                }else{
                    if (line == "Id0\teKEY:list0")
                    {
                        plotPosition = countLine;
                    }
                }
                countLine++;
            }
            if ((treePosition == -1) | (plotPosition == -1))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/list1\nLe numéro des arbres ou de la placette n'est pas trouvé dans le fichier.");
                urgentStop();
                return;
            }else{ // Map of the tree, first value = its key, second value = number of the tree, then the trees will be sorted ascending by their key
                while (!treeStream.atEnd())
                {
                    dataRow = treeStream.readLine().split("\t");
                    if (dataRow.size() > plotPosition)
                    {
                        if (dataRow.at(plotPosition).toInt() == plotCode)
                        {
                            treeMap[dataRow.at(0).toInt()] = dataRow.at(treePosition).toInt();
                        }
                    }
                }
            }
        }
        treeFile.close();
    }

    // -> then we read the file dictionary/status to bo able to split with axis followed and axis non followed
    QFile statFile("bin/dictionary/status");
    QMap<int, QString> statusFollowed, statusAbandonned;
    countLine = 0;
    if (!statFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/status.");
        urgentStop();
        return;
    }else{
        QTextStream statStream(&statFile);
        line = statStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/status\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            namePosition = -1;
            int statPosition(-1);
            while (!statStream.atEnd() && line != "###")
            {
                line = statStream.readLine();
                if (line == "NameStatus\tSTRING")
                {
                    namePosition = countLine;
                }else{
                    if (line == "Followed\tBOOLEAN")
                    {
                        statPosition = countLine;
                    }
                }
                countLine++;
            }
            if ((namePosition == -1) | (statPosition == -1))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/dictionary/status\nLe nom des status, ou le booléen indiquant les status suivis n'est pas trouvé dans le fichier.");
                urgentStop();
                return;
            }else{
                while (!statStream.atEnd())
                {
                    dataRow = statStream.readLine().split("\t");
                    if ((dataRow.size() > statPosition) && (dataRow.size() > namePosition))
                    {
                        if (dataRow.at(statPosition) == "T")
                        {
                            statusFollowed[dataRow.at(0).toInt()] = dataRow.at(namePosition);
                        }else{
                            statusAbandonned[dataRow.at(0).toInt()] = dataRow.at(namePosition);
                        }
                    }
                }
            }
        }
        statFile.close();
    }

    // -> we need to read position to be able to construct the axis names
    QFile positionFile("bin/dictionary/position");
    QMap<int, QString> positionMap;
    countLine = 0;
    if (!positionFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/position.");
        urgentStop();
        return;
    }else{
        QTextStream positionStream(&positionFile);
        line = positionStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/position\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            namePosition = -1;
            while (!positionStream.atEnd() && line != "###")
            {
                line = positionStream.readLine();
                if (line == "ShortNamePosition\tSTRING")
                {
                    namePosition = countLine;
                }
                countLine++;
            }
            if (namePosition == -1)
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/dictionary/position\nLe nom court des position n'est pas trouvé dans le fichier.");
                urgentStop();
                return;
            }else{
                while (!positionStream.atEnd())
                {
                    dataRow = positionStream.readLine().split("\t");
                    if (dataRow.size() > namePosition)
                    {
                        positionMap[dataRow.at(0).toInt()] = dataRow.at(namePosition);
                    }
                }
            }
        }
        positionFile.close();
    }

    // -> we need to read exposition to be able to construct the axis names
    QFile expositionFile("bin/dictionary/exposition");
    QMap<int, QString> expositionMap;
    countLine = 0;
    if (!expositionFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/exposition.");
        urgentStop();
        return;
    }else{
        QTextStream expositionStream(&expositionFile);
        line = expositionStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/exposition\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            namePosition = -1;
            while (!expositionStream.atEnd() && line != "###")
            {
                line = expositionStream.readLine();
                if (line == "ShortNameExposition\tSTRING")
                {
                    namePosition = countLine;
                }
                countLine++;
            }
            if (namePosition == -1)
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/dictionary/exposition\nLe nom court des exposition n'est pas trouvé dans le fichier.");
                urgentStop();
                return;
            }else{
                while (!expositionStream.atEnd())
                {
                    dataRow = expositionStream.readLine().split("\t");
                    if (dataRow.size() > namePosition) expositionMap[dataRow.at(0).toInt()] = dataRow.at(namePosition);
                }
            }
        }
        expositionFile.close();
    }

    // -> we need to read dominance to be able to construct the axis names
    QFile dominanceFile("bin/dictionary/dominance");
    QMap<int, QString> dominanceMap;
    countLine = 0;
    if (!dominanceFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/dominance.");
        urgentStop();
        return;
    }else{
        QTextStream dominanceStream(&dominanceFile);
        line = dominanceStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/dominance\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            namePosition = -1;
            while (!dominanceStream.atEnd() && line != "###")
            {
                line = dominanceStream.readLine();
                if (line == "ShortNameDominance\tSTRING")
                {
                    namePosition = countLine;
                }
                countLine++;
            }
            if (namePosition == -1)
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/dictionary/dominance\nLe nom court des dominances d'axes' n'est pas trouvé dans le fichier.");
                urgentStop();
                return;
            }else{
                while (!dominanceStream.atEnd())
                {
                    dataRow = dominanceStream.readLine().split("\t");
                    if (dataRow.size() > namePosition) dominanceMap[dataRow.at(0).toInt()] = dataRow.at(namePosition);
                }
            }
        }
        dominanceFile.close();
    }

    // -> then we keep, from the file list2, only the axis located on the trees, splitted in followed/abandonned
    QFile axisFile("bin/list2");
    QMap<QString, QList<QVariant> > axisFMap, axisAMap;
    if (!axisFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
        urgentStop();
        return;
    }else{
        QTextStream axisStream(&axisFile);
        line = axisStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            countLine = 0;
            treePosition = -1;
            int posPosition(-1), expPosition(-1), domPosition(-1), nRamPosition(-1), statPosition(-1), ASFPosition(-1), ASLPosition(-1), FObsePosition(-1), LObsePosition(-1);
            while (!axisStream.atEnd() && line != "###")
            {
                line = axisStream.readLine();
                if (line == "Id1\teKEY:list1")
                {
                    treePosition = countLine;
                }else if (line == "IdPos\teKEY:dictionary/position")
                {
                    posPosition = countLine;
                }else if (line == "IdExp\teKEY:dictionary/exposition")
                {
                    expPosition = countLine;
                }else if (line == "IdDom\teKEY:dictionary/dominance")
                {
                    domPosition = countLine;
                }else if (line == "NRam\tSTRING") {
                    nRamPosition = countLine;
                }else if (line == "IdStat\teKEY:dictionary/status")
                {
                    statPosition = countLine;
                }else if (line == "ASFirst\tINTEGER")
                {
                    ASFPosition = countLine;
                }else if (line == "ASLast\tINTEGER")
                {
                    ASLPosition = countLine;
                }else if (line == "FirstObserve\tDATE")
                {
                    FObsePosition = countLine;
                }else if (line == "LastObserve\tDATE")
                {
                    LObsePosition = countLine;
                }
                countLine++;
            }
            if ((treePosition == -1) | (posPosition == -1) | (expPosition == -1) | (expPosition == -1) | (domPosition == -1) | (nRamPosition == -1) | (statPosition == -1) | (statPosition == -1) | (ASFPosition == -1) | (ASLPosition == -1) | (FObsePosition == -1) | (LObsePosition == -1))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/list2\nUne des variables suivantes n'a pas été trouvée dans le fichier :\n"
                                                      "- clé de l'arbre\n- clé de position\n- clé d'exposition\n- clé de dominance\n- numéro de l'axe\n- clé de status\n"
                                                      "- première PA\n- dernière PA\n- date de première observation\n- date de dernière observation");
                urgentStop();
                return;
            }else{ // Map of the axis, first value = axis, second value = its key, then the axis will be sorted ascending by their name
                QString axisName;
                QList<QVariant> axisProperties;
                QStringList observe;
                QDate observeDate;
                while (!axisStream.atEnd())
                {
                    dataRow = axisStream.readLine().split("\t");
                    if (treeMap.keys().contains(dataRow.at(treePosition).toInt()))
                    {
                        axisProperties.insert(0, QVariant(dataRow.at(0).toInt()));
                        if (dataRow.size() > treePosition) axisProperties.insert(1, QVariant(dataRow.at(treePosition).toInt()));
                        if (dataRow.size() > posPosition) axisProperties.insert(2, QVariant(dataRow.at(posPosition).toInt()));
                        if (dataRow.size() > expPosition) axisProperties.insert(3, QVariant(dataRow.at(expPosition).toInt()));
                        if (dataRow.size() > domPosition) axisProperties.insert(4, QVariant(dataRow.at(domPosition).toInt()));
                        if (dataRow.size() > nRamPosition) axisProperties.insert(5, QVariant(dataRow.at(nRamPosition).toInt()));
                        if (dataRow.size() > statPosition) axisProperties.insert(6, QVariant(dataRow.at(statPosition).toInt()));
                        if (dataRow.size() > ASFPosition) axisProperties.insert(7, QVariant(dataRow.at(ASFPosition).toInt()));
                        if (dataRow.size() > ASLPosition) axisProperties.insert(8, QVariant(dataRow.at(ASLPosition).toInt()));
                        if (dataRow.size() > FObsePosition)
                        {
                            observe = dataRow.at(FObsePosition).split(".");
                            if (observe.size() > 2)
                            {
                                observeDate = QDate(observe.at(0).toInt(), observe.at(1).toInt(), observe.at(2).toInt());
                                axisProperties.insert(9, QVariant(observeDate));
                            }
                        }
                        if (dataRow.size() > LObsePosition)
                        {
                            observe = dataRow.at(LObsePosition).split(".");
                            if (observe.size() > 2)
                            {
                                observeDate = QDate(observe.at(0).toInt(), observe.at(1).toInt(), observe.at(2).toInt());
                                axisProperties.insert(10, QVariant(observeDate));
                            }
                        }
                        if ((dataRow.size() > treePosition) && (dataRow.size() > posPosition) && (dataRow.size() > expPosition) && (dataRow.size() > domPosition) && (dataRow.size() > nRamPosition)) axisName = QString("PA%1 %2%3 %4%5").arg(treeMap[dataRow.at(treePosition).toInt()]).arg(positionMap[dataRow.at(posPosition).toInt()]).arg(expositionMap[dataRow.at(expPosition).toInt()]).arg(dominanceMap[dataRow.at(domPosition).toInt()]).arg(dataRow.at(nRamPosition));
                        if (dataRow.size()> statPosition)
                        {
                            if (statusFollowed.contains(dataRow.at(statPosition).toInt()))
                            {
                                axisFMap.insert(axisName, axisProperties);
                            }else{
                                axisAMap.insert(axisName, axisProperties);
                            }
                        }
                    }
                }
            }
        }
        axisFile.close();
    }

    // -> now we load each characteristics modification for the axises concerned
    QFile paramFile("bin/axisCharacteristics");
    QMap<int, QMap<QString, QStringList > > charactMap;
    QMap<QString, int> charactPosition;
    int Id;
    countLine = -1;
    if (!paramFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/axisCharacteristics.");
        urgentStop();
        return;
    }else{
        QTextStream paramStream(&paramFile);
        line = paramStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/axisCharacteristics\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!paramStream.atEnd()  && line != "###")
            {
                line = paramStream.readLine();
                if (line != "###")
                {
                    charactPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
            }
            while(!paramStream.atEnd())
            {
                dataRow = paramStream.readLine().split("\t");
                dataRow.removeFirst();
                if (dataRow.size() > charactPosition["Id2"]) Id = dataRow.at(charactPosition["Id2"]).toInt();
                if (!charactMap.keys().contains(Id)) charactMap.insert(Id,*(new QMap<QString, QStringList>));
                if (dataRow.size() > charactPosition["Date"]) charactMap[Id].insert(dataRow.at(charactPosition["Date"]), dataRow);
            }
        }
        paramFile.close();
    }

    // Filling the tables
    tab0 = new QTableWidget(axisFMap.size(), 6, this);
    tab0->verticalHeader()->hide();
    QList<QString> tabHeader;
    tabHeader << "Nom de l'axe" << "Status" << "Prem. PA" << "Dern. PA" << "Prem. Enreg." << "Dern. Enreg.";
    tab0->setHorizontalHeaderLabels(QStringList(tabHeader));
    tabHeader.clear();
    tab0->setSelectionBehavior(QAbstractItemView::SelectRows);
    tab0->setSelectionMode(QAbstractItemView::SingleSelection);
    QTableWidgetItem *cell;
    countLine = 0;
    QList<QVariant> axisProperties;
    QString currDate;
    for (QMap<QString, QList<QVariant> >::iterator it = axisFMap.begin(); it != axisFMap.end(); it++)
    {
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        cell->setText(it.key());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab0->setItem(countLine, 0, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 6) cell->setText(statusFollowed[it.value().at(6).toInt()]);
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab0->setItem(countLine, 1, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 7) cell->setText(it.value().at(7).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab0->setItem(countLine, 2, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 8) cell->setText(it.value().at(8).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab0->setItem(countLine, 3, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 9) cell->setText(it.value().at(9).toDate().toString("dd/MM/yyyy"));
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab0->setItem(countLine, 4, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 10) cell->setText(it.value().at(10).toDate().toString("dd/MM/yyyy"));
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab0->setItem(countLine, 5, cell);
        if (charactMap.keys().contains(it.value().at(0).toInt()))
        {
            axisProperties = it.value();
            currDate = charactMap[axisProperties.at(0).toInt()].keys().at(charactMap[axisProperties.at(0).toInt()].size() - 1);
            if (charactMap[axisProperties.at(0).toInt()][currDate].size() > charactPosition["CurrPos"]) axisProperties[2] = QVariant(charactMap[axisProperties.at(0).toInt()][currDate].at(charactPosition["CurrPos"]).toInt());
            if (charactMap[axisProperties.at(0).toInt()][currDate].size() > charactPosition["CurrExp"]) axisProperties[3] = QVariant(charactMap[axisProperties.at(0).toInt()][currDate].at(charactPosition["CurrExp"]).toInt());
            if (charactMap[axisProperties.at(0).toInt()][currDate].size() > charactPosition["CurrDom"]) axisProperties[4] = QVariant(charactMap[axisProperties.at(0).toInt()][currDate].at(charactPosition["CurrDom"]).toInt());
            if (charactMap[axisProperties.at(0).toInt()][currDate].size() > charactPosition["CurrName"]) tab0->item(countLine, 0)->setText(charactMap[axisProperties.at(0).toInt()][currDate].at(charactPosition["CurrName"]));
            tab0->item(countLine, 0)->setData(Qt::UserRole, axisProperties);
            tab0->item(countLine, 1)->setData(Qt::UserRole, axisProperties);
            tab0->item(countLine, 2)->setData(Qt::UserRole, axisProperties);
            tab0->item(countLine, 3)->setData(Qt::UserRole, axisProperties);
            tab0->item(countLine, 4)->setData(Qt::UserRole, axisProperties);
            tab0->item(countLine, 5)->setData(Qt::UserRole, axisProperties);
        }
        countLine++;
    }
    tab0->setSortingEnabled(true);
    tab0->sortByColumn(0, Qt::AscendingOrder);
    tab2 = new QTableWidget(axisAMap.size(), 6, this);
    tab2->verticalHeader()->hide();
    tabHeader << "Nom de l'axe" << "Status" << "Prem. PA" << "Dern. PA" << "Prem. Enreg." << "Dern. Enreg.";
    tab2->setHorizontalHeaderLabels(QStringList(tabHeader));
    tabHeader.clear();
    tab2->setSelectionBehavior(QAbstractItemView::SelectRows);
    tab2->setSelectionMode(QAbstractItemView::SingleSelection);
    countLine = 0;
    for (QMap<QString, QList<QVariant> >::iterator it = axisAMap.begin(); it != axisAMap.end(); it++)
    {
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        cell->setText(it.key());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab2->setItem(countLine, 0, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 6) cell->setText(statusAbandonned[it.value().at(6).toInt()]);
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab2->setItem(countLine, 1, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 7) cell->setText(it.value().at(7).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab2->setItem(countLine, 2, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 8) cell->setText(it.value().at(8).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab2->setItem(countLine, 3, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 9) cell->setText(it.value().at(9).toDate().toString("dd/MM/yyyy"));
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab2->setItem(countLine, 4, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, it.value());
        if (it.value().size() > 10) cell->setText(it.value().at(10).toDate().toString("dd/MM/yyyy"));
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab2->setItem(countLine, 5, cell);
        if (charactMap.keys().contains(it.value().at(0).toInt()))
        {
            axisProperties = it.value();
            if (charactMap[axisProperties.at(0).toInt()].keys().size() > (charactMap[axisProperties.at(0).toInt()].size() - 1)) currDate = charactMap[axisProperties.at(0).toInt()].keys().at(charactMap[axisProperties.at(0).toInt()].size() - 1);
            if (charactMap[axisProperties.at(0).toInt()][currDate].size () > charactPosition["CurrPos"]) axisProperties[2] = QVariant(charactMap[axisProperties.at(0).toInt()][currDate].at(charactPosition["CurrPos"]).toInt());
            if (charactMap[axisProperties.at(0).toInt()][currDate].size () > charactPosition["CurrExp"]) axisProperties[3] = QVariant(charactMap[axisProperties.at(0).toInt()][currDate].at(charactPosition["CurrExp"]).toInt());
            if (charactMap[axisProperties.at(0).toInt()][currDate].size () > charactPosition["CurrDom"]) axisProperties[4] = QVariant(charactMap[axisProperties.at(0).toInt()][currDate].at(charactPosition["CurrDom"]).toInt());
            if (charactMap[axisProperties.at(0).toInt()][currDate].size () > charactPosition["CurrName"]) tab2->item(countLine, 0)->setText(charactMap[axisProperties.at(0).toInt()][currDate].at(charactPosition["CurrName"]));
            tab2->item(countLine, 0)->setData(Qt::UserRole, axisProperties);
            tab2->item(countLine, 1)->setData(Qt::UserRole, axisProperties);
            tab2->item(countLine, 2)->setData(Qt::UserRole, axisProperties);
            tab2->item(countLine, 3)->setData(Qt::UserRole, axisProperties);
            tab2->item(countLine, 4)->setData(Qt::UserRole, axisProperties);
            tab2->item(countLine, 5)->setData(Qt::UserRole, axisProperties);
        }
        countLine++;
    }
    tab2->setSortingEnabled(true);
    tab2->sortByColumn(0, Qt::AscendingOrder);

    // Connexion of the signals to the slots
    QObject::connect(tab0, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(expandchangeCharacteristics(int, int)));
    QObject::connect(tab2, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(expandchangeCharacteristics(int, int)));
    QObject::connect(buttonBackToBack, SIGNAL(clicked()), this, SLOT(changeAxisCharacteristics()));
    QObject::connect(buttonBackToMain, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

    // Filling mainlayout
    mainLayout->addWidget(title, 0, 0, 1, 2);
    mainLayout->addWidget(labelTab0, 1, 0);
    mainLayout->addWidget(labelTab2, 1, 1);
    mainLayout->addWidget(tab0, 2, 0);
    mainLayout->addWidget(tab2, 2, 1);
    mainLayout->addWidget(buttonBackToBack, 3, 0, 1, 2);
    mainLayout->addWidget(buttonBackToMain, 4, 0, 1, 2);
    setObjectName("changeCharacteristics");
}
