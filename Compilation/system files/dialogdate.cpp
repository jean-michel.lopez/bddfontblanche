#include <dialogdate.h>

DialogueDate::DialogueDate(): QDialog()
{
    m_text = new QLabel();
    buttonCancel = new QPushButton("Annuler");
    buttonOk = new QPushButton("Ok");
    calendar = new QCalendarWidget();
    calendar->setMaximumDate(QDate::currentDate());

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(m_text);
    mainLayout->addWidget(calendar);

    QHBoxLayout *layoutButton = new QHBoxLayout;
    layoutButton->addStretch();
    layoutButton->addWidget(buttonOk);
    layoutButton->addWidget(buttonCancel);

    mainLayout->addLayout(layoutButton);

    QObject::connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
    QObject::connect(buttonOk, SIGNAL(clicked()), this, SLOT(accept()));
    QObject::connect(calendar, SIGNAL(selectionChanged()), this, SLOT(majDate()));

    setLayout(mainLayout);
    setModal(true);
}

QDate DialogueDate::getDate(QWidget *parent, QString title, QString text, QDate defaultDate, bool *ok)
{
    DialogueDate *window = new DialogueDate();
    window->setParent(parent);
    window->setText(text);
    window->setTitle(title);
    window->setDate(defaultDate);

    if(window->exec())
    {
        *ok = true;
        return window->getDate();
    }
    else
    {
        *ok = false;
        return defaultDate;
    }
}

void DialogueDate::setText(QString text)
{
    m_text->setText(text);
}

void DialogueDate::setTitle(QString title)
{
    setWindowTitle(title);
}

void DialogueDate::setDate(QDate date)
{
    calendar->setSelectedDate(date);
    dateToSend = date;
}

void DialogueDate::majDate()
{
    dateToSend = calendar->selectedDate();
}

QDate DialogueDate::getDate()
{
    return dateToSend;
}
