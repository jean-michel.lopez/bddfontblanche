#include <MainWindow.h>

void MainWindow::prepareCampaignSlot()
{
    QString folderName = QFileDialog::getExistingDirectory(NULL, "Dossier où enregistrer les fichiers", "", QFileDialog::ShowDirsOnly);
    if (folderName != "")
    {
        QFile::copy("bin/Modele.xlsm", QString("%1/Modele.xlsm").arg(folderName));
        QMap<int, QMap<int, QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > > > > dataFrame;
        // Now we browse each data file
        // first list0
        QFile plotFile("bin/list0");
        int countLine(-1);
        QMap<QString, int> plotPosition;
        QMap<int, QStringList> plotMap;
        int Id;
        QString line;
        QStringList dataRow;
        QMap<int, QMap<QString, int> > treeNames;
        if (!plotFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list0.");
            urgentStop();
            return;
        }else{
            QTextStream plotStream(&plotFile);
            line = plotStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list0\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!plotStream.atEnd()  && line != "###")
                {
                    line = plotStream.readLine();
                    plotPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!plotStream.atEnd())
                {
                    dataRow = plotStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    plotMap.insert(Id, dataRow);
                    dataFrame.insert(Id, *(new QMap<int, QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > > >));
                    treeNames.insert(Id, *(new QMap<QString, int>));
                }
            }
            // then list1
            QFile treeFile("bin/list1");
            countLine = -1;
            QMap<QString, int> treePosition;
            QMap<int, QStringList> treeMap;
            QMap<int, QMap<QString, int> > axisNames;
            if (!treeFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list1.");
                urgentStop();
                return;
            }else{
                QTextStream treeStream(&treeFile);
                line = treeStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list1\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = "";
                    while (!treeStream.atEnd()  && line != "###")
                    {
                        line = treeStream.readLine();
                        treePosition.insert(line.split("\t").at(0), countLine);
                        countLine++;
                    }
                    while(!treeStream.atEnd())
                    {
                        dataRow = treeStream.readLine().split("\t");
                        Id = dataRow.at(0).toInt();
                        dataRow.removeFirst();
                        treeMap.insert(Id, dataRow);
                        if (dataRow.size() > treePosition["Id0"])
                        {
                            dataFrame[dataRow.at(treePosition["Id0"]).toInt()].insert(Id, *(new QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > >));
                            if (dataRow.size() > treePosition["NumArb"]) treeNames[dataRow.at(treePosition["Id0"]).toInt()].insert(QString("PA %1").arg(dataRow.at(treePosition["NumArb"])), Id);
                        }
                        axisNames.insert(Id, *(new(QMap<QString, int>)));
                    }
                }
                // then status
                QFile statFile("bin/dictionary/status");
                countLine = -1;
                QMap<QString, int> statPosition;
                QMap<int, QStringList> statMap;
                QList<int> statusFollowed;
                if (!statFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/status.");
                    urgentStop();
                    return;
                }else{
                    QTextStream statStream(&statFile);
                    line = statStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/status\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = "";
                        while (!statStream.atEnd()  && line != "###")
                        {
                            line = statStream.readLine();
                            statPosition.insert(line.split("\t").at(0), countLine);
                            countLine++;
                        }
                        while(!statStream.atEnd())
                        {
                            dataRow = statStream.readLine().split("\t");
                            Id = dataRow.at(0).toInt();
                            dataRow.removeFirst();
                            statMap.insert(Id, dataRow);
                            if (dataRow.size() > statPosition["Followed"])
                            {
                                if (dataRow.at(statPosition["Followed"]) == "T")
                                {
                                    statusFollowed.append(Id);
                                }
                            }
                        }
                    }
                    // then position
                    QFile posFile("bin/dictionary/position");
                    countLine = -1;
                    QMap<QString, int> posPosition;
                    QMap<int, QStringList> posMap;
                    if (!posFile.open(QIODevice::ReadOnly | QIODevice::Text))
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/position.");
                        urgentStop();
                        return;
                    }else{
                        QTextStream posStream(&posFile);
                        line = posStream.readLine();
                        if (line != "###")
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/position\nMauvaise entête (\"###\" attendu)");
                            urgentStop();
                            return;
                        }else{
                            line = "";
                            while (!posStream.atEnd()  && line != "###")
                            {
                                line = posStream.readLine();
                                posPosition.insert(line.split("\t").at(0), countLine);
                                countLine++;
                            }
                            while(!posStream.atEnd())
                            {
                                dataRow = posStream.readLine().split("\t");
                                Id = dataRow.at(0).toInt();
                                dataRow.removeFirst();
                                posMap.insert(Id, dataRow);
                            }
                        }
                        // then exposition
                        QFile expFile("bin/dictionary/exposition");
                        countLine = -1;
                        QMap<QString, int> expPosition;
                        QMap<int, QStringList> expMap;
                        if (!expFile.open(QIODevice::ReadOnly | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/exposition.");
                            urgentStop();
                            return;
                        }else{
                            QTextStream expStream(&expFile);
                            line = expStream.readLine();
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/exposition\nMauvaise entête (\"###\" attendu)");
                                urgentStop();
                                return;
                            }else{
                                line = "";
                                while (!expStream.atEnd()  && line != "###")
                                {
                                    line = expStream.readLine();
                                    expPosition.insert(line.split("\t").at(0), countLine);
                                    countLine++;
                                }
                                while(!expStream.atEnd())
                                {
                                    dataRow = expStream.readLine().split("\t");
                                    Id = dataRow.at(0).toInt();
                                    dataRow.removeFirst();
                                    expMap.insert(Id, dataRow);
                                }
                            }
                            // then dominance
                            QFile domFile("bin/dictionary/dominance");
                            countLine = -1;
                            QMap<QString, int> domPosition;
                            QMap<int, QStringList> domMap;
                            if (!domFile.open(QIODevice::ReadOnly | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/dominance.");
                                urgentStop();
                                return;
                            }else{
                                QTextStream domStream(&domFile);
                                line = domStream.readLine();
                                if (line != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/dominance\nMauvaise entête (\"###\" attendu)");
                                    urgentStop();
                                    return;
                                }else{
                                    line = "";
                                    while (!domStream.atEnd()  && line != "###")
                                    {
                                        line = domStream.readLine();
                                        domPosition.insert(line.split("\t").at(0), countLine);
                                        countLine++;
                                    }
                                    while(!domStream.atEnd())
                                    {
                                        dataRow = domStream.readLine().split("\t");
                                        Id = dataRow.at(0).toInt();
                                        dataRow.removeFirst();
                                        domMap.insert(Id, dataRow);
                                    }
                                }
                                // then list2
                                QFile axisFile("bin/list2");
                                countLine = -1;
                                QMap<QString, int> axisPosition;
                                QMap<int, QStringList> axisMap;
                                QMap<int, QMap<QString, int> > GUNames;
                                QString tempName;
                                if (!axisFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                                    urgentStop();
                                    return;
                                }else{
                                    QTextStream axisStream(&axisFile);
                                    line = axisStream.readLine();
                                    if (line != "###")
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                                        urgentStop();
                                        return;
                                    }else{
                                        line = "";
                                        while (!axisStream.atEnd()  && line != "###")
                                        {
                                            line = axisStream.readLine();
                                            axisPosition.insert(line.split("\t").at(0), countLine);
                                            countLine++;
                                        }
                                        while(!axisStream.atEnd())
                                        {
                                            dataRow = axisStream.readLine().split("\t");
                                            Id = dataRow.at(0).toInt();
                                            dataRow.removeFirst();
                                            if (dataRow.size() > axisPosition["IdStat"])
                                            {
                                                if (statusFollowed.contains(dataRow.at(axisPosition["IdStat"]).toInt()))
                                                {
                                                    axisMap.insert(Id, dataRow);
                                                    if (dataRow.size() > axisPosition["Id1"]) if (treeMap[dataRow.at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"])
                                                    {
                                                        dataFrame[treeMap[dataRow.at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][dataRow.at(axisPosition["Id1"]).toInt()].insert(Id, *(new QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > >));
                                                        if (dataRow.size() > axisPosition["IdPos"] && dataRow.size() > axisPosition["IdExp"] && dataRow.size() > axisPosition["IdDom"] && dataRow.size() > axisPosition["NRam"])
                                                        {
                                                            if (posMap[dataRow.at(axisPosition["IdPos"]).toInt()].size() > posPosition["ShortNamePosition"] && expMap[dataRow.at(axisPosition["IdExp"]).toInt()].size() > expPosition["ShortNameExposition"] && domMap[dataRow.at(axisPosition["IdDom"]).toInt()].size() > domPosition["ShortNameDominance"])
                                                            {
                                                                axisNames[dataRow.at(axisPosition["Id1"]).toInt()].insert(QString("%1%2 %3%4").arg(posMap[dataRow.at(axisPosition["IdPos"]).toInt()].at(posPosition["ShortNamePosition"])).arg(expMap[dataRow.at(axisPosition["IdExp"]).toInt()].at(expPosition["ShortNameExposition"])).arg(domMap[dataRow.at(axisPosition["IdDom"]).toInt()].at(domPosition["ShortNameDominance"])).arg(dataRow.at(axisPosition["NRam"])), Id);
                                                            }
                                                        }
                                                    }
                                                    GUNames.insert(Id, *(new QMap<QString, int>));
                                                }
                                            }
                                        }
                                    }
                                    // then list3
                                    QFile GUFile("bin/list3");
                                    countLine = -1;
                                    QMap<QString, int> GUPosition;
                                    QMap<int, QStringList> GUMap;
                                    if (!GUFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list3.");
                                        urgentStop();
                                        return;
                                    }else{
                                        QTextStream GUStream(&GUFile);
                                        line = GUStream.readLine();
                                        if (line != "###")
                                        {
                                            // Error
                                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list3\nMauvaise entête (\"###\" attendu)");
                                            urgentStop();
                                            return;
                                        }else{
                                            line = "";
                                            while (!GUStream.atEnd()  && line != "###")
                                            {
                                                line = GUStream.readLine();
                                                GUPosition.insert(line.split("\t").at(0), countLine);
                                                countLine++;
                                            }
                                            while(!GUStream.atEnd())
                                            {
                                                dataRow = GUStream.readLine().split("\t");
                                                Id = dataRow.at(0).toInt();
                                                dataRow.removeFirst();
                                                if (dataRow.size() > GUPosition["Id2"])
                                                {
                                                    if (axisMap.contains(dataRow.at(GUPosition["Id2"]).toInt()))
                                                    {
                                                        GUMap.insert(Id, dataRow);
                                                        if (axisMap[dataRow.at(GUPosition["Id2"]).toInt()].size() > axisPosition["Id1"])
                                                        {
                                                            if (treeMap[axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"])
                                                            {
                                                                dataFrame[treeMap[axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][dataRow.at(GUPosition["Id2"]).toInt()].insert(Id, *(new QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > >));
                                                                dataFrame[treeMap[axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[dataRow.at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][dataRow.at(GUPosition["Id2"]).toInt()][Id].second.first.first = -1; // -1 = No male reproduction
                                                            }
                                                        }
                                                        if (dataRow.size() > GUPosition["Year"] && dataRow.size() > GUPosition["NGU"])
                                                        {
                                                            if ((dataRow.at(GUPosition["Year"]) == "0") || (dataRow.at(GUPosition["Year"]) == ""))
                                                            {
                                                                GUNames[dataRow.at(GUPosition["Id2"]).toInt()].insert(QString("---- . %1").arg(dataRow.at(GUPosition["NGU"])), Id);
                                                            }else{
                                                                GUNames[dataRow.at(GUPosition["Id2"]).toInt()].insert(QString("%1 . %2").arg(dataRow.at(GUPosition["Year"])).arg(dataRow.at(GUPosition["NGU"])), Id);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // then list4
                                        QFile reproFile("bin/list4");
                                        countLine = -1;
                                        QMap<QString, int> reproPosition;
                                        QMap<int, QStringList> reproMap;
                                        if (!reproFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                        {
                                            // Error
                                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
                                            urgentStop();
                                            return;
                                        }else{
                                            QTextStream reproStream(&reproFile);
                                            line = reproStream.readLine();
                                            if (line != "###")
                                            {
                                                // Error
                                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list4\nMauvaise entête (\"###\" attendu)");
                                                urgentStop();
                                                return;
                                            }else{
                                                line = "";
                                                while (!reproStream.atEnd()  && line != "###")
                                                {
                                                    line = reproStream.readLine();
                                                    reproPosition.insert(line.split("\t").at(0), countLine);
                                                    countLine++;
                                                }
                                                while(!reproStream.atEnd())
                                                {
                                                    dataRow = reproStream.readLine().split("\t");
                                                    Id = dataRow.at(0).toInt();
                                                    dataRow.removeFirst();
                                                    if (dataRow.size() > reproPosition["Id3"])
                                                    {
                                                        if (GUMap.contains(dataRow.at(reproPosition["Id3"]).toInt()))
                                                        {
                                                            reproMap.insert(Id, dataRow);
                                                            if (dataRow.size() > reproPosition["IdSex"] && GUMap[dataRow.at(reproPosition["Id3"]).toInt()].size() > GUPosition["Id2"])
                                                            {
                                                                if (axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].size() > axisPosition["Id1"])
                                                                {
                                                                    if (treeMap[axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"])
                                                                    {
                                                                        if (dataRow.at(reproPosition["IdSex"]) == "1")
                                                                        {
                                                                            dataFrame[treeMap[axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][dataRow.at(reproPosition["Id3"]).toInt()].second.first.first = Id;
                                                                        }else{
                                                                            dataFrame[treeMap[axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[dataRow.at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][dataRow.at(reproPosition["Id3"]).toInt()].second.second.insert(Id, *(new QMap<QString, int>));
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            // then sex
                                            QFile sexFile("bin/dictionary/sex");
                                            countLine = -1;
                                            QMap<QString, int> sexPosition;
                                            QMap<int, QStringList> sexMap;
                                            if (!sexFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                            {
                                                // Error
                                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/sex.");
                                                urgentStop();
                                                return;
                                            }else{
                                                QTextStream sexStream(&sexFile);
                                                line = sexStream.readLine();
                                                if (line != "###")
                                                {
                                                    // Error
                                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/sex\nMauvaise entête (\"###\" attendu)");
                                                    urgentStop();
                                                    return;
                                                }else{
                                                    line = "";
                                                    while (!sexStream.atEnd()  && line != "###")
                                                    {
                                                        line = sexStream.readLine();
                                                        sexPosition.insert(line.split("\t").at(0), countLine);
                                                        countLine++;
                                                    }
                                                    while(!sexStream.atEnd())
                                                    {
                                                        dataRow = sexStream.readLine().split("\t");
                                                        Id = dataRow.at(0).toInt();
                                                        dataRow.removeFirst();
                                                        sexMap.insert(Id, dataRow);
                                                    }
                                                }
                                                // then variables
                                                QFile varFile("bin/dictionary/variables");
                                                countLine = -1;
                                                QMap<QString, int> varPosition;
                                                QMap<int, QStringList> varMap;
                                                QFile * tempFile;
                                                QMap<QString, QMap<QString, int> > factorPosition;
                                                QMap<QString, QMap<int, QStringList> > factorMap;
                                                QMap<QString, int> tempPosition;
                                                QMap<int, QStringList> tempMap;
                                                int tempId;
                                                if (!varFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                {
                                                    // Error
                                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/variables.");
                                                    urgentStop();
                                                    return;
                                                }else{
                                                    QTextStream varStream(&varFile);
                                                    line = varStream.readLine();
                                                    if (line != "###")
                                                    {
                                                        // Error
                                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/variables\nMauvaise entête (\"###\" attendu)");
                                                        urgentStop();
                                                        return;
                                                    }else{
                                                        line = "";
                                                        while (!varStream.atEnd()  && line != "###")
                                                        {
                                                            line = varStream.readLine();
                                                            varPosition.insert(line.split("\t").at(0), countLine);
                                                            countLine++;
                                                        }
                                                        while(!varStream.atEnd())
                                                        {
                                                            dataRow = varStream.readLine().split("\t");
                                                            Id = dataRow.at(0).toInt();
                                                            dataRow.removeFirst();
                                                            varMap.insert(Id, dataRow);
                                                            if (varMap[Id].size() > varPosition["TypeVar"])
                                                            {
                                                                if (varMap[Id].at(varPosition["TypeVar"]).startsWith("eKEY"))
                                                                {
                                                                    // factor variable
                                                                    tempFile = new QFile(QString("bin/%1").arg(dataRow.at(varPosition["TypeVar"]).split(":").at(1)));
                                                                    countLine = -1;
                                                                    tempPosition.clear();
                                                                    tempMap.clear();
                                                                    if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                                                    {
                                                                        // Error
                                                                        QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                                                        urgentStop();
                                                                        return;
                                                                    }else{
                                                                        QTextStream tempStream(tempFile);
                                                                        line = tempStream.readLine();
                                                                        if (line != "###")
                                                                        {
                                                                            // Error
                                                                            QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                                                            urgentStop();
                                                                            return;
                                                                        }else{
                                                                            line = "";
                                                                            while (!tempStream.atEnd()  && line != "###")
                                                                            {
                                                                                line = tempStream.readLine();
                                                                                tempPosition.insert(line.split("\t").at(0), countLine);
                                                                                countLine++;
                                                                            }
                                                                            while(!tempStream.atEnd())
                                                                            {
                                                                                dataRow = tempStream.readLine().split("\t");
                                                                                tempId = dataRow.at(0).toInt();
                                                                                dataRow.removeFirst();
                                                                                tempMap.insert(tempId, dataRow);
                                                                            }
                                                                        }
                                                                    }
                                                                    tempFile->close();
                                                                    if (varMap[Id].size() > varPosition["Name"])
                                                                    {
                                                                        factorPosition.insert(varMap[Id].at(varPosition["Name"]), tempPosition);
                                                                        factorMap.insert(varMap[Id].at(varPosition["Name"]), tempMap);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    // then reproduction variables
                                                    QFile reproVarFile("bin/dictionary/reproductionVariables");
                                                    countLine = -1;
                                                    QMap<QString, int> reproVarPosition;
                                                    QMap<int, QStringList> reproVarMap;
                                                    if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                    {
                                                        // Error
                                                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
                                                        urgentStop();
                                                        return;
                                                    }else{
                                                        QTextStream reproVarStream(&reproVarFile);
                                                        line = reproVarStream.readLine();
                                                        if (line != "###")
                                                        {
                                                            // Error
                                                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
                                                            urgentStop();
                                                            return;
                                                        }else{
                                                            line = "";
                                                            while (!reproVarStream.atEnd()  && line != "###")
                                                            {
                                                                line = reproVarStream.readLine();
                                                                reproVarPosition.insert(line.split("\t").at(0), countLine);
                                                                countLine++;
                                                            }
                                                            while(!reproVarStream.atEnd())
                                                            {
                                                                dataRow = reproVarStream.readLine().split("\t");
                                                                Id = dataRow.at(0).toInt();
                                                                dataRow.removeFirst();
                                                                reproVarMap.insert(Id, dataRow);
                                                                if (reproVarMap[Id].size() > reproVarPosition["TypeVar"])
                                                                {
                                                                    if (reproVarMap[Id].at(reproVarPosition["TypeVar"]).startsWith("eKEY"))
                                                                    {
                                                                        // factor variable
                                                                        tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproVarPosition["TypeVar"]).split(":").at(1)));
                                                                        countLine = -1;
                                                                        tempPosition.clear();
                                                                        tempMap.clear();
                                                                        if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                                                        {
                                                                            // Error
                                                                            QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                                                            urgentStop();
                                                                            return;
                                                                        }else{
                                                                            QTextStream tempStream(tempFile);
                                                                            line = tempStream.readLine();
                                                                            if (line != "###")
                                                                            {
                                                                                // Error
                                                                                QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                                                                urgentStop();
                                                                                return;
                                                                            }else{
                                                                                line = "";
                                                                                while (!tempStream.atEnd()  && line != "###")
                                                                                {
                                                                                    line = tempStream.readLine();
                                                                                    tempPosition.insert(line.split("\t").at(0), countLine);
                                                                                    countLine++;
                                                                                }
                                                                                while(!tempStream.atEnd())
                                                                                {
                                                                                    dataRow = tempStream.readLine().split("\t");
                                                                                    tempId = dataRow.at(0).toInt();
                                                                                    dataRow.removeFirst();
                                                                                    tempMap.insert(tempId, dataRow);
                                                                                }
                                                                            }
                                                                        }
                                                                        tempFile->close();
                                                                        if (reproVarMap[Id].size() > reproVarPosition["Name"])
                                                                        {
                                                                            factorPosition.insert(reproVarMap[Id].at(reproVarPosition["Name"]), tempPosition);
                                                                            factorMap.insert(reproVarMap[Id].at(reproVarPosition["Name"]), tempMap);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        // then we load the measures
                                                        QFile measureFile("bin/measure");
                                                        countLine = -1;
                                                        QMap<QString, int> measurePosition;
                                                        QMap<int, QStringList> measureMap;
                                                        if (!measureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                        {
                                                            // Error
                                                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/measure.");
                                                            urgentStop();
                                                            return;
                                                        }else{
                                                            QTextStream measureStream(&measureFile);
                                                            line = measureStream.readLine();
                                                            if (line != "###")
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/measure\nMauvaise entête (\"###\" attendu)");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                line = "";
                                                                while (!measureStream.atEnd()  && line != "###")
                                                                {
                                                                    line = measureStream.readLine();
                                                                    measurePosition.insert(line.split("\t").at(0), countLine);
                                                                    countLine++;
                                                                }
                                                                while(!measureStream.atEnd())
                                                                {
                                                                    dataRow = measureStream.readLine().split("\t");
                                                                    Id = dataRow.at(0).toInt();
                                                                    dataRow.removeFirst();
                                                                    if (dataRow.size() > measurePosition["Id3"])
                                                                    {
                                                                        if (GUMap.contains(dataRow.at(measurePosition["Id3"]).toInt()))
                                                                        {
                                                                            measureMap.insert(Id, dataRow);
                                                                            if (GUMap[dataRow.at(measurePosition["Id3"]).toInt()].size() > GUPosition["Id2"])
                                                                            {
                                                                                if (axisMap[GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].size() > axisPosition["Id1"])
                                                                                {
                                                                                    if (treeMap[axisMap[GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"] && dataRow.size() > measurePosition["DateM"])
                                                                                    {
                                                                                        dataFrame[treeMap[axisMap[GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[dataRow.at(measurePosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][dataRow.at(measurePosition["Id3"]).toInt()].first.insert(dataRow.at(measurePosition["DateM"]), Id);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            // and then we load the reproduction measures
                                                            QFile reproMeasureFile("bin/reproductionMeasure");
                                                            countLine = -1;
                                                            QMap<QString, int> reproMeasurePosition;
                                                            QMap<int, QStringList> reproMeasureMap;
                                                            if (!reproMeasureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                            {
                                                                // Error
                                                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                                                urgentStop();
                                                                return;
                                                            }else{
                                                                QTextStream reproMeasureStream(&reproMeasureFile);
                                                                line = reproMeasureStream.readLine();
                                                                if (line != "###")
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/reproductionMeasure\nMauvaise entête (\"###\" attendu)");
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    line = "";
                                                                    while (!reproMeasureStream.atEnd()  && line != "###")
                                                                    {
                                                                        line = reproMeasureStream.readLine();
                                                                        reproMeasurePosition.insert(line.split("\t").at(0), countLine);
                                                                        countLine++;
                                                                    }
                                                                    while(!reproMeasureStream.atEnd())
                                                                    {
                                                                        dataRow = reproMeasureStream.readLine().split("\t");
                                                                        Id = dataRow.at(0).toInt();
                                                                        dataRow.removeFirst();
                                                                        if (dataRow.size() > reproMeasurePosition["Id4"])
                                                                        {
                                                                            if (reproMap.contains(dataRow.at(reproMeasurePosition["Id4"]).toInt()))
                                                                            {
                                                                                reproMeasureMap.insert(Id, dataRow);
                                                                                if (reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].size() > reproPosition["IdSex"])
                                                                                {
                                                                                    if (reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].size() > reproPosition["Id3"])
                                                                                    {
                                                                                        if (GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].size() > GUPosition["Id2"])
                                                                                        {
                                                                                            if (axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].size() > axisPosition["Id1"])
                                                                                            {
                                                                                                if (treeMap[axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].size() > treePosition["Id0"] && dataRow.size() > reproMeasurePosition["DateRM"])
                                                                                                {
                                                                                                    if (reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["IdSex"]) == "1")
                                                                                                    {
                                                                                                        dataFrame[treeMap[axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].second.first.second.insert(dataRow.at(reproMeasurePosition["DateRM"]), Id);
                                                                                                    }else{
                                                                                                        dataFrame[treeMap[axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()].at(treePosition["Id0"]).toInt()][axisMap[GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()].at(axisPosition["Id1"]).toInt()][GUMap[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].at(GUPosition["Id2"]).toInt()][reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].second.second[dataRow.at(reproMeasurePosition["Id4"]).toInt()].insert(dataRow.at(reproMeasurePosition["DateRM"]), Id);
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                // -> now we load each characteristics modification for the axises concerned
                                                                QFile paramFile("bin/axisCharacteristics");
                                                                QMap<int, QMap<QString, QStringList > > charactMap;
                                                                QMap<QString, int> charactPosition;
                                                                countLine = -1;
                                                                if (!paramFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                                                {
                                                                    // Error
                                                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/axisCharacteristics.");
                                                                    urgentStop();
                                                                    return;
                                                                }else{
                                                                    QTextStream paramStream(&paramFile);
                                                                    line = paramStream.readLine();
                                                                    if (line != "###")
                                                                    {
                                                                        // Error
                                                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/axisCharacteristics\nMauvaise entête (\"###\" attendu)");
                                                                        urgentStop();
                                                                        return;
                                                                    }else{
                                                                        line = "";
                                                                        while (!paramStream.atEnd()  && line != "###")
                                                                        {
                                                                            line = paramStream.readLine();
                                                                            if (line != "###")
                                                                            {
                                                                                charactPosition.insert(line.split("\t").at(0), countLine);
                                                                                countLine++;
                                                                            }
                                                                        }
                                                                        while(!paramStream.atEnd())
                                                                        {
                                                                            dataRow = paramStream.readLine().split("\t");
                                                                            dataRow.removeFirst();
                                                                            if (dataRow.size() > charactPosition["Id2"])
                                                                            {
                                                                                Id = dataRow.at(charactPosition["Id2"]).toInt();
                                                                                if (!charactMap.keys().contains(Id)) charactMap.insert(Id,*(new QMap<QString, QStringList>));
                                                                                if (dataRow.size() > charactPosition["Date"]) charactMap[Id].insert(dataRow.at(charactPosition["Date"]), dataRow);
                                                                            }
                                                                        }
                                                                    }
                                                                    paramFile.close();
                                                                }
                                                                // then we can write the file (47 lines by page)
                                                                QFile * outputFile;
                                                                QString name;
                                                                for (QMap<int, QMap<int, QMap<int, QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > > > >::iterator plot(dataFrame.begin()); plot != dataFrame.end(); plot++)
                                                                {
                                                                    if (plotMap[plot.key()].size() > plotPosition["Name"]) outputFile = new QFile(QString("%1/%2.csv").arg(folderName).arg(plotMap[plot.key()].at(plotPosition["Name"])));
                                                                    if (!outputFile->open(QIODevice::WriteOnly))
                                                                    {
                                                                        // Error
                                                                        QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à écrire le fichier : %1.").arg(outputFile->fileName()));
                                                                        urgentStop();
                                                                        return;
                                                                    }else{
                                                                        // file header
                                                                        QTextStream outputStream(outputFile);
                                                                        outputStream << "Ind;AnUC;N";
                                                                        for (QMap<int, QStringList>::iterator it(varMap.begin()); it != varMap.end(); it++)
                                                                        {
                                                                            if (it->size() > varPosition["AppearingVar"])
                                                                            {
                                                                                if (it->at(varPosition["AppearingVar"]) == "Y" && it->size() > varPosition["ShortName"])
                                                                                {
                                                                                    outputStream << ";" << it->at(varPosition["ShortName"]) << ";";
                                                                                }
                                                                            }
                                                                        }
                                                                        QString tempName;
                                                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                        {
                                                                            if (it->size() > reproVarPosition["Target"])
                                                                            {
                                                                                if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male"))
                                                                                {
                                                                                    if (it->size() > reproVarPosition["AppearingVar"] && it->size() > reproVarPosition["ShortName"])
                                                                                    {
                                                                                        if (it->at(reproVarPosition["AppearingVar"]) == "Y")
                                                                                        {
                                                                                            tempName = it->at(reproVarPosition["ShortName"]);
                                                                                            tempName.replace("#", "Male");
                                                                                            outputStream << ";" << tempName << ";";
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                        {
                                                                            if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                                                            {
                                                                                if ((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female"))
                                                                                {
                                                                                    if (it->at(reproVarPosition["AppearingVar"]) == "Y" && it->size() > reproVarPosition["ShortName"])
                                                                                    {
                                                                                        tempName = it->at(reproVarPosition["ShortName"]);
                                                                                        tempName.replace("#", QString("Fem"));
                                                                                        outputStream << ";" << tempName << ";";
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        outputStream << "\n";
                                                                        countLine = 0;
                                                                        for (QMap<QString, int>::iterator tree(treeNames[plot.key()].begin()); tree != treeNames[plot.key()].end(); tree++)
                                                                        {
                                                                            for (QMap<QString, int>::iterator axis(axisNames[tree.value()].begin()); axis != axisNames[tree.value()].end(); axis++)
                                                                            {
                                                                                if ((countLine + GUNames[axis.value()].size()) > 46)
                                                                                {
                                                                                    for (int i(countLine); i <= 47; i++)
                                                                                    {
                                                                                        outputStream << "\n";
                                                                                    }
                                                                                    countLine = 0;
                                                                                }
                                                                                outputStream << tree.key() << "\n";
                                                                                countLine++;
                                                                                if (charactMap.contains(axis.value()))
                                                                                {
                                                                                    if (charactMap[axis.value()][charactMap[axis.value()].keys().last()].size() > charactPosition["CurrName"]) name = charactMap[axis.value()][charactMap[axis.value()].keys().last()].at(charactPosition["CurrName"]);
                                                                                    if (name.startsWith(tree.key()))
                                                                                    {
                                                                                        name.remove(0, tree.key().length());
                                                                                    }
                                                                                    outputStream << name << "\n";
                                                                                }else{
                                                                                    outputStream << axis.key() << "\n";
                                                                                }
                                                                                for (QMap<QString, int>::iterator gu(GUNames[axis.value()].begin()); gu != GUNames[axis.value()].end(); gu++)
                                                                                {
                                                                                    if (GUMap[gu.value()].size() > GUPosition["Year"] && GUMap[gu.value()].size() > GUPosition["NGU"]) outputStream << ";" << GUMap[gu.value()].at(GUPosition["Year"]) << ";" << GUMap[gu.value()].at(GUPosition["NGU"]);
                                                                                    // first we write measure
                                                                                    QString value("");
                                                                                    for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                                                                    {
                                                                                        if (var->size() > varPosition["AppearingVar"])
                                                                                        {
                                                                                            if (var->at(varPosition["AppearingVar"]) == "Y")
                                                                                            {
                                                                                                QMap<QString, int>::iterator measure(plot.value()[tree.value()][axis.value()][gu.value()].first.end());
                                                                                                measure--;
                                                                                                if (var->size() > varPosition["Name"]) if (measureMap[measure.value()].size() > measurePosition[var->at(varPosition["Name"])])
                                                                                                {
                                                                                                    value = measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]);
                                                                                                }
                                                                                                while ((value == "") && (measure != plot.value()[tree.value()][axis.value()][gu.value()].first.begin()))
                                                                                                {
                                                                                                    measure--;
                                                                                                    if (var->size() > varPosition["Name"]) if (measureMap[measure.value()].size() > measurePosition[var->at(varPosition["Name"])])
                                                                                                    {
                                                                                                        value = measureMap[measure.value()].at(measurePosition[var->at(varPosition["Name"])]);
                                                                                                    }
                                                                                                }
                                                                                                if (var->size() > varPosition["TypeVar"])
                                                                                                {
                                                                                                    if ((var->at(varPosition["TypeVar"]).startsWith("eKEY")) && (value != ""))
                                                                                                    {
                                                                                                        if (var->size() > varPosition["Name"]) if (factorMap[var->at(varPosition["Name"])][value.toInt()].size() > factorPosition[var->at(varPosition["Name"])]["Code"])
                                                                                                        {
                                                                                                            outputStream << ";" << factorMap[var->at(varPosition["Name"])][value.toInt()].at(factorPosition[var->at(varPosition["Name"])]["Code"]) << ";";
                                                                                                        }
                                                                                                    }else{
                                                                                                        outputStream << ";" << value << ";";
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    // then reproduction
                                                                                    Id = plot.value()[tree.value()][axis.value()][gu.value()].second.first.first;
                                                                                    if (Id == -1)
                                                                                    {
                                                                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                        {
                                                                                            if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                                                                            {
                                                                                                if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                                                                                {
                                                                                                    outputStream << ";;";
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }else{
                                                                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                        {
                                                                                            QMap<QString, int>::iterator measure(plot.value()[tree.value()][axis.value()][gu.value()].second.first.second.end());
                                                                                            measure--;
                                                                                            if (it->size() > reproVarPosition["Name"]) if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                            {
                                                                                                value = reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                            }
                                                                                            while ((value == "") && (measure != plot.value()[tree.value()][axis.value()][gu.value()].second.first.second.begin()))
                                                                                            {
                                                                                                measure--;
                                                                                                if (it->size() > reproVarPosition["Name"]) if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                {
                                                                                                    value = reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                                }
                                                                                            }
                                                                                            if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                                                                            {
                                                                                                if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Male")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                                                                                {
                                                                                                    if (it->size() > reproVarPosition["TypeVar"])
                                                                                                    {
                                                                                                        if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (value != ""))
                                                                                                        {
                                                                                                            if (it->size() > reproVarPosition["Name"]) if (factorMap[it->at(reproVarPosition["Name"])][value.toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"])
                                                                                                            {
                                                                                                                outputStream << ";" << factorMap[it->at(reproVarPosition["Name"])][value.toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]) << ";";
                                                                                                            }
                                                                                                        }else{
                                                                                                            outputStream << ";" << value << ";";
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                                                                    {
                                                                                        outputStream << ";";
                                                                                        for (int i(0); i < plot.value()[tree.value()][axis.value()][gu.value()].second.second.size(); i++)
                                                                                        {
                                                                                            if (plot.value()[tree.value()][axis.value()][gu.value()].second.second.keys().size() > i)
                                                                                            {
                                                                                                tempId = plot.value()[tree.value()][axis.value()][gu.value()].second.second.keys().at(i);
                                                                                                QMap<QString, int>::iterator measure(plot.value()[tree.value()][axis.value()][gu.value()].second.second[tempId].end());
                                                                                                measure--;
                                                                                                if (it->size() > reproVarPosition["Name"]) if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                {
                                                                                                    value = reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                                    while ((value == "") && (measure != plot.value()[tree.value()][axis.value()][gu.value()].second.second[tempId].begin()))
                                                                                                    {
                                                                                                        measure--;
                                                                                                        if (it->size() > reproVarPosition["Name"]) if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[it->at(reproVarPosition["Name"])])
                                                                                                        {
                                                                                                            value = reproMeasureMap[measure.value()].at(reproMeasurePosition[it->at(reproVarPosition["Name"])]);
                                                                                                        }
                                                                                                    }
                                                                                                    if (it->size() > reproVarPosition["Target"] && it->size() > reproVarPosition["AppearingVar"])
                                                                                                    {
                                                                                                        if (((it->at(reproVarPosition["Target"]) == "Both") || (it->at(reproVarPosition["Target"]) == "Female")) && (it->at(reproVarPosition["AppearingVar"]) == "Y"))
                                                                                                        {
                                                                                                            if (i > 0)
                                                                                                            {
                                                                                                                outputStream << " _ ";
                                                                                                            }
                                                                                                            if (it->size() > reproVarPosition["TypeVar"]) if ((it->at(reproVarPosition["TypeVar"]).startsWith("eKEY")) && (value != ""))
                                                                                                            {
                                                                                                                if (it->size() > reproVarPosition["Name"]) if (factorMap[it->at(reproVarPosition["Name"])][value.toInt()].size() > factorPosition[it->at(reproVarPosition["Name"])]["Code"])
                                                                                                                {
                                                                                                                    outputStream << factorMap[it->at(reproVarPosition["Name"])][value.toInt()].at(factorPosition[it->at(reproVarPosition["Name"])]["Code"]);
                                                                                                                }
                                                                                                            }else{
                                                                                                                outputStream << value;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        outputStream << ";";
                                                                                    }
                                                                                    outputStream << "\n";
                                                                                    countLine++;
                                                                                }
                                                                                outputStream << "\n";
                                                                                countLine++;
                                                                            }
                                                                        }
                                                                        outputFile->close();
                                                                    }
                                                                }
                                                                QMessageBox::information(NULL, "Export terminé", "L'export des données est terminé, il s'est correctement déroulé");
                                                            }
                                                            reproMeasureFile.close();
                                                        }
                                                        measureFile.close();
                                                    }
                                                    reproVarFile.close();
                                                }
                                                varFile.close();
                                            }
                                            sexFile.close();
                                        }
                                        reproFile.close();
                                    }
                                    GUFile.close();
                                }
                                axisFile.close();
                            }
                            domFile.close();
                        }
                        expFile.close();
                    }
                    posFile.close();
                }
                statFile.close();
            }
            treeFile.close();
        }
        plotFile.close();
    }
}
