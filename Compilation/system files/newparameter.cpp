#include <MainWindow.h>

void MainWindow::newParameter()
{
    // First we launch a changeParameterClass object
    QDate mini, current;
    mini = QDate(0, 1, 1);
    QTableWidget *ParamTab;
    ParamTab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(4, 0)->widget());
    int row(ParamTab->rowCount());
    for (int r(0); r < row; r++)
    {
        if (ParamTab->item(r, 1)->text().split("/").size() > 2) current = QDate(ParamTab->item(r, 1)->text().split("/").at(2).toInt(), ParamTab->item(r, 1)->text().split("/").at(1).toInt(), ParamTab->item(r, 1)->text().split("/").at(0).toInt());
        if (current > mini) mini = current;
    }
    QList<QVariant> datas;
    datas = changeParameterClass::newParameters(NULL, userData, qobject_cast<QLabel*>(mainLayout->itemAtPosition(3, 0)->widget())->text(), mini);
    // Then we add a row to the table with these characteristics
    if (datas.size() > 9)
    {
        QTableWidgetItem *cell;
        ParamTab->insertRow(row);
        cell = new QTableWidgetItem;
        cell->setText(QString::number(datas.at(0).toInt()));
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 0, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 9) cell->setText(datas.at(9).toDate().toString("dd/MM/yyyy"));
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 1, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 1) cell->setText(datas.at(1).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 2, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 2) cell->setText(datas.at(2).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 3, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 3) cell->setText(datas.at(3).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 4, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 4) cell->setText(datas.at(4).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 5, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 5) cell->setText(datas.at(5).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 6, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 6) cell->setText(datas.at(6).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 7, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 7) cell->setText(datas.at(7).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 8, cell);
        cell = new QTableWidgetItem;
        if (datas.size() > 8) cell->setText(datas.at(8).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        ParamTab->setItem(row, 9, cell);
    }
}
