#include <MainWindow.h>

void MainWindow::checkValue(int a)
{
    QSpinBox *source;
    source = qobject_cast<QSpinBox *>(QObject::sender());
    if (source->property("d") == QVariant::Invalid)
    {
        source->setStyleSheet("QSpinBox { background-color: greenyellow; }");
    }else{
        QMap<QString, QVariant> datas(source->property("d").toMap());
        if (datas.size() > 0)
        {
            int value(datas[datas.keys().at(datas.size() - 1)].toString().toInt());
            if (source->property("i").toString() == "In")
            {
                if (a >= value)
                {
                    source->setStyleSheet("QSpinBox { background-color: greenyellow; }");
                }else{
                    source->setStyleSheet("QSpinBox { background-color: salmon; }");
                }
            }else if (source->property("i").toString() == "De")
            {
                if (a > value)
                {
                    source->setStyleSheet("QSpinBox { background-color: salmon; }");
                }else{
                    source->setStyleSheet("QSpinBox { background-color: greenyellow; }");
                }
            }else{
                source->setStyleSheet("QSpinBox { background-color: greenyellow; }");
            }
        }else{
            source->setStyleSheet("QSpinBox { background-color: greenyellow; }");
        }
    }
    source->update();
}

void MainWindow::checkValue(double a)
{
    QDoubleSpinBox *source;
    source = qobject_cast<QDoubleSpinBox *>(QObject::sender());
    if (source->property("d") == QVariant::Invalid)
    {
        source->setStyleSheet("QDoubleSpinBox { background-color: greenyellow; }");
    }else{
        QMap<QString, QVariant> datas(source->property("d").toMap());
        if (datas.size() > 0)
        {
            double value(datas[datas.keys().at(datas.size() - 1)].toString().toDouble());
            if (source->property("i").toString() == "In")
            {
                if (a >= value)
                {
                    source->setStyleSheet("QDoubleSpinBox { background-color: greenyellow; }");
                }else{
                    source->setStyleSheet("QDoubleSpinBox { background-color: salmon; }");
                }
            }else if (source->property("i").toString() == "De")
            {
                if (a > value)
                {
                    source->setStyleSheet("QDoubleSpinBox { background-color: salmon; }");
                }else{
                    source->setStyleSheet("QDoubleSpinBox { background-color: greenyellow; }");
                }
            }else{
                source->setStyleSheet("QDoubleSpinBox { background-color: greenyellow; }");
            }
        }else{
            source->setStyleSheet("QDoubleSpinBox { background-color: greenyellow; }");
        }
    }
    source->update();
}

void MainWindow::checkValue(QString a)
{
    QComboBox *source;
    source = qobject_cast<QComboBox *>(QObject::sender());
    if (a != "")
    {
        source->setStyleSheet("QComboBox { background-color: greenyellow; }");
    }else{
        source->setStyleSheet("QComboBox");
    }
}
