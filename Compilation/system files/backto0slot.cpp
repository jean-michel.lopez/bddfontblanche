#include <MainWindow.h>

void MainWindow::backTo0Slot()
{
    // Empty mainLayout
    while(mainLayout->count() > 0){
        QLayoutItem *item = mainLayout->takeAt(0);
        delete item->widget();
        delete item;
    }
    delete mainLayout;
    mainLayout = new QGridLayout;
    setLayout(mainLayout);

    // 3 buttons when created
    QPushButton *button0 = new QPushButton("Consulter/Exporter les données");
    QPushButton *button1 = new QPushButton("Enregistrer des données");
    QPushButton *button2 = new QPushButton("Modifier la base de données");
    QPushButton *button3 = new QPushButton("Charger la saisie de terrain");

    // Add buttons to the mainLayout
    mainLayout -> addWidget(button0, 0, 0);
    mainLayout -> addWidget(button1, 0, 1);
    mainLayout -> addWidget(button2, 0, 2);
    mainLayout -> addWidget(button3, 0, 3);

    // if state = 2, switch to 0
    if (*state == "2")
    {
        *state = "0";
        QFile stateFile("state");
        stateFile.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream in(&stateFile);
        in << 0;
        stateFile.close();
        button3->hide();
    }

    // Hide buttons in case of state different than 0
    if (*state != "0")
    {
       if (*state != "4")
       {
           button1->hide();
       }
       button2->hide();
       if (*state != "3")
       {
           button3->hide();
       }
    }else{
        // Set the state to the previous state
        QFile stateFile("state");
        stateFile.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream in(&stateFile);
        in << 1;
        stateFile.close();
        button3->hide();
    }

    // Connexion slots to button clicks
    QObject::connect(button0, SIGNAL(clicked()), this, SLOT(consultOrExportData()));
    QObject::connect(button1, SIGNAL(clicked()), this, SLOT(saveDataContext()));
    QObject::connect(button2, SIGNAL(clicked()), this, SLOT(modifiesDataBase()));
    this->setObjectName("MainWindow");
}
