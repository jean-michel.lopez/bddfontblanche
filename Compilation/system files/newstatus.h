#ifndef NEWSTATUS
#define NEWSTATUS

#include <QDialog>
#include <QWidget>
#include <QString>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QMap>
#include <QMessageBox>
#include <QFile>
#include <QStringList>
#include <QTextStream>
#include <QGridLayout>

class newStatus : public QDialog
{
    Q_OBJECT

public:
    newStatus();
    static int getNewStatus(QWidget *parent, QString *statusName, bool *followed);
    int getStatusCode();
    QString getStatusName();

private:
    void urgentStop();

public slots:
    void guessStatus(QString currentStatus);
    void validation();

private:
    QMap<QString, int> statusList;
    QLabel *title;
    QPushButton *validateButton;
    QPushButton *cancelButton;
    QLineEdit *statusText;
    QList<QString> statusPosition;
    QString prevStatus;
};

#endif // NEWSTATUS

