#include <MainWindow.h>

void MainWindow::changeLocalisationParameter(int a)
{
    QTableWidget *newVarTab;
    newVarTab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(4, 0)->widget());
    QLineEdit *textEdit;
    QComboBox *factorEdit;
    if (a == 0)
    {
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 2));
        textEdit->setToolTip("Nom court de la variable utilisé pour les colonnes des tableaux.");
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 3));
        textEdit->setToolTip("Nom moyen de la variable utilisé pour les colonnes des tableaux.");
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 4));
        textEdit->setToolTip("Nom long de la variable utilisé pour les exportations de données.");
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 5));
        textEdit->setToolTip("Explicitation de la variable utilisé pour l'aide dans les tables.");
        factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 7));
        factorEdit->setEnabled(false);
        factorEdit->setToolTip("");
    }else if (a == 1){
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 2));
        textEdit->setToolTip("Nom court de la variable utilisé pour les colonnes des tableaux, un # doit apparaître pour faire apparaître la mention mâle ou femelle.");
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 3));
        textEdit->setToolTip("Nom moyen de la variable utilisé pour les fiches de terrain, un # doit apparaître pour faire apparaître la mention mâle ou femelle.");
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 4));
        textEdit->setToolTip("Nom long de la variable utilisé pour les exportations de données, un # doit apparaître pour faire apparaître la mention mâle ou femelle.");
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 5));
        textEdit->setToolTip("Explicitation de la variable utilisé pour l'aide dans les tables, un # peut apparaître pour faire apparaître la mention mâle ou femelle.");
        factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 7));
        factorEdit->setEnabled(true);
        factorEdit->setToolTip("Cible sexuelle (mâle, femelle ou les deux)");
    }
}

void MainWindow::changeTypeParameter(int a)
{
    QTableWidget *newVarTab;
    newVarTab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(4, 0)->widget());
    QLineEdit *textEdit;
    QComboBox *factorEdit;
    if ((a == 3) || (a == 2))
    {
        factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 8));
        factorEdit->setCurrentIndex(-1);
        factorEdit->setEnabled(false);
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 9));
        textEdit->setText("");
        textEdit->setEnabled(false);
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 10));
        textEdit->setText("");
        textEdit->setEnabled(false);
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 11));
        textEdit->setText("");
        textEdit->setEnabled(false);
    }else{
        factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 8));
        factorEdit->setEnabled(true);
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 9));
        textEdit->setEnabled(true);
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 10));
        textEdit->setEnabled(true);
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 11));
        textEdit->setEnabled(true);
    }
}

void MainWindow::addParameterSlot()
{
    QTableWidget *newVarTab;
    newVarTab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(4, 0)->widget());
    QLineEdit *textEdit;
    QComboBox *factorEdit;

    // we will check each column of the table
    int localisation;
    factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 0));
    localisation = factorEdit->currentIndex();
    if (localisation == -1)
    {
        QMessageBox::critical(NULL, "Erreur", "La localisation de la variable n'a pas été renseignée.");
        factorEdit->setFocus();
        return;
    }
    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 1));
    if (textEdit->text() == "")
    {
        QMessageBox::critical(NULL, "Erreur", "Il me faut un nom pour cette variable, merci.");
        textEdit->setFocus();
        return;
    }
    QTableWidget *targetTable;
    if (localisation == 0)
    {
        targetTable = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 0)->widget());
    }else{
        targetTable = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2, 1)->widget());
    }
    for (int row(0); row < targetTable->rowCount(); row++)
    {
       if (targetTable->item(row, 0)->text() == textEdit->text())
       {
           QMessageBox::critical(NULL, "Erreur", "Une variable portant ce nom existe déjà dans la base de données.");
           textEdit->setFocus();
           return;
       }
    }
    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 2));
    if (textEdit->text() == "")
    {
        QMessageBox::critical(NULL, "Erreur", "Il me faut un nom court pour cette variable, merci.");
        textEdit->setFocus();
        return;
    }
    if ((localisation == 1) && (!textEdit->text().contains("#")))
    {
        QMessageBox::critical(NULL, "Erreur", "Il manque un # au nom court.");
        textEdit->setFocus();
        return;
    }
    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 3));
    if (textEdit->text() == "")
    {
        QMessageBox::critical(NULL, "Erreur", "Il me faut un nom moyen pour cette variable, merci.");
        textEdit->setFocus();
        return;
    }
    if ((localisation == 1) && (!textEdit->text().contains("#")))
    {
        QMessageBox::critical(NULL, "Erreur", "Il manque un # au nom court.");
        textEdit->setFocus();
        return;
    }
    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 4));
    if (textEdit->text() == "")
    {
        QMessageBox::critical(NULL, "Erreur", "Il me faut un nom long pour cette variable, merci.");
        textEdit->setFocus();
        return;
    }
    if ((localisation == 1) && (!textEdit->text().contains("#")))
    {
        QMessageBox::critical(NULL, "Erreur", "Il manque un # au nom court.");
        textEdit->setFocus();
        return;
    }
    int type;
    factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 6));
    type = factorEdit->currentIndex();
    if (type == -1)
    {
        QMessageBox::critical(NULL, "Erreur", "Il me faut un type de variable, merci.");
        factorEdit->setFocus();
        return;
    }
    int target;
    factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 7));
    if (localisation == 1)
    {
        target = factorEdit->currentIndex();
        if (target == -1)
        {
            QMessageBox::critical(NULL, "Erreur", "Il faut me renseigner le sexe cible de la variable, merci.");
            factorEdit->setFocus();
            return;
        }
    }
    double mini, maxi;
    bool hasMin(false), hasMax(false);
    if ((type == 0) || (type == 1))
    {
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 9));
        if (textEdit->text() != "") hasMin = true;
        mini = textEdit->text().toDouble();
        textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 10));
        if (textEdit->text() != "") hasMax = true;
        maxi = textEdit->text().toDouble();
        if (hasMin && hasMax)
        {
            if (mini >= maxi)
            {
                QMessageBox::critical(NULL, "Erreur", "J'ai décelé une erreur avec les valeurs minimales et maximales.");
                textEdit->setFocus();
                return;
            }
        }
    }

    // And we can now add the parameter to the files
    QString fileName;
    if (localisation == 0)
    {
        fileName = "bin/dictionary/variables";
    }else{
        fileName = "bin/dictionary/reproductionVariables";
    }
    QFile varFile(fileName);
    QString line, cat;
    QList<QString> nameList;
    int code;
    if (!varFile.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : " + fileName);
        urgentStop();
        return;
    }else{
        QTextStream varStream(&varFile);
        line = varStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : " + fileName +"\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = varStream.readLine();
            while (!varStream.atEnd()  && !line.startsWith("###"))
            {
                cat = line.split("\t").at(0);
                nameList.append(cat);
                line = varStream.readLine();
            }
            while(!varStream.atEnd())
            {
                code = varStream.readLine().split("\t").at(0).toInt();
            }
            code++;
            for (QList<QString>::iterator it(nameList.begin()); it != nameList.end(); it++)
            {
                if ((*it).startsWith("IdVar"))
                {
                    varStream << code;
                }else if (*it == "Name")
                {
                    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 1));
                    varStream << textEdit->text();
                }else if (*it == "ShortName")
                {
                    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 2));
                    varStream << textEdit->text();
                }else if (*it == "MediumName")
                {
                    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 3));
                    varStream << textEdit->text();
                }else if (*it == "LongName")
                {
                    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 4));
                    varStream << textEdit->text();
                }else if (*it == "ExplainVar")
                {
                    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 5));
                    varStream << textEdit->text();
                }else if (*it == "TypeVar")
                {
                    switch(type)
                    {
                    case 0 : varStream << "INTEGER";
                        break;
                    case 1 : varStream << "DOUBLE";
                        break;
                    case 2 : varStream << "STRING";
                        break;
                    case 3 : varStream << "eKEY:dictionary/" << qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 1))->text();
                        // we need to create the key file
                        break;
                    }
                }else if ((*it == "Target") && (localisation == 1))
                {
                    switch (target)
                    {
                    case 0 : varStream << "Both";
                        break;
                    case 1 : varStream << "Male";
                        break;
                    case 2 : varStream << "Female";
                        break;
                    }
                }else if (*it == "RangeableVar")
                {
                    varStream << "N";
                }else if ((*it == "IncreasingVar") && ((type == 0) || (type == 1)))
                {
                    factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 8));
                    switch (factorEdit->currentIndex())
                    {
                    case -1 : varStream << "N";
                        break;
                    case 0 : varStream << "N";
                        break;
                    case 1 : varStream << "Y";
                        break;
                    case 2 : varStream << "N";
                        break;
                    }
                }else if ((*it == "DecreasingVar") && ((type == 0) || (type == 1)))
                {
                    factorEdit = qobject_cast<QComboBox *>(newVarTab->cellWidget(0, 8));
                    switch (factorEdit->currentIndex())
                    {
                    case -1 : varStream << "N";
                        break;
                    case 0 : varStream << "N";
                        break;
                    case 1 : varStream << "N";
                        break;
                    case 2 : varStream << "Y";
                        break;
                    }
                }else if ((*it == "MinValueVar") && (hasMin))
                {
                    varStream << mini;
                }else if ((*it == "MaxValueVar") && (hasMax))
                {
                    varStream << maxi;
                }else if (*it == "AppearingVar")
                {
                    varStream << "Y";
                }else if (*it == "UnitVar")
                {
                    textEdit = qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 11));
                    varStream << textEdit->text();
                }
                if ((*it != ""))
                {
                    varStream << "\t";
                }
            }
            varStream << "\n";
        }
    }
    varFile.close();

    // we add an empty column to the existing datas
    if (localisation == 0)
    {
        fileName = "bin/measure";
    }else{
        fileName = "bin/reproductionMeasure";
    }
    QFile measureFile(fileName);
    QString content;
    if (!measureFile.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : " + fileName + ".");
        urgentStop();
        return;
    }else{
        QTextStream measureStream(&measureFile);
        line = measureStream.readLine();
        content.append(line + "\n");
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : " + fileName + "\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = measureStream.readLine();
            while (!measureStream.atEnd()  && line != "###")
            {
                content.append(line + "\n");
                line = measureStream.readLine();
            }
            content.append(qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 1))->text() + "\t");
            switch (type)
            {
            case 0 : content.append("INTEGER\n");
                break;
            case 1 : content.append("DOUBLE\n");
                break;
            case 2 : content.append("STRING\n");
                break;
            case 3 : content.append("eKEY:dictionary/" + qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 1))->text() + "\n");
                break;
            }
            content.append(line + "\n");
            while(!measureStream.atEnd())
            {
                line = measureStream.readLine();
                content.append(line + "\t\n");
            }
            measureFile.resize(0);
            measureStream << content;
        }
    }
    measureFile.close();

    // We then can add the parameter to the table
    int row;
    row = targetTable->rowCount();
    targetTable->insertRow(row);
    QTableWidgetItem *cell = new QTableWidgetItem;
    cell->setText(qobject_cast<QLineEdit *>(newVarTab->cellWidget(0, 1))->text());
    targetTable->setItem(row, 0, cell);
    cell = new QTableWidgetItem;
    switch (type)
    {
    case 0 : cell->setText("ENTIER");
        break;
    case 1 : cell->setText("REEL");
        break;
    case 2 : cell->setText("TEXTE");
        break;
    case 3 : cell->setText("FACTORISEE (dictionary/" + qobject_cast<QLineEdit *>(newVarTab->cellWidget(row, 0))->text() + ")");
        break;
    }
    targetTable->setItem(row, 1, cell);
}
