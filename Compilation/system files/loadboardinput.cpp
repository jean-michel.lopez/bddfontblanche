#include <MainWindow.h>

void MainWindow::loadBoardInput()
{
    // We first need to get access to the board input files
    QString folderName = QFileDialog::getExistingDirectory(NULL, "Dossier contenant la saisie de terrain (\"terrain-JJ.MM.AAAA\")", "", QFileDialog::ShowDirsOnly);
    if (folderName != "")
    {
        // We check if the folder contains a folder bin, and dictionary
        bool existence(false);
        if (QDir(folderName + "/bin/dictionary").exists()) existence = true;
        if (!QFile(folderName + "/bin/dictionary/dominance").exists()) existence = false;
        if (!QFile(folderName + "/bin/dictionary/position").exists()) existence = false;
        if (!QFile(folderName + "/bin/dictionary/exposition").exists()) existence = false;
        if (!QFile(folderName + "/bin/dictionary/sex").exists()) existence = false;
        if (!QFile(folderName + "/bin/dictionary/status").exists()) existence = false;
        if (!QFile(folderName + "/bin/dictionary/variables").exists()) existence = false;
        if (!QFile(folderName + "/bin/dictionary/reproductionVariables").exists()) existence = false;
        if (!QFile(folderName + "/bin/comment").exists()) existence = false;
        if (!QFile(folderName + "/bin/list0").exists()) existence = false;
        if (!QFile(folderName + "/bin/list1").exists()) existence = false;
        if (!QFile(folderName + "/bin/list2").exists()) existence = false;
        if (!QFile(folderName + "/bin/list3").exists()) existence = false;
        if (!QFile(folderName + "/bin/list4").exists()) existence = false;
        if (!QFile(folderName + "/bin/measure").exists()) existence = false;
        if (!QFile(folderName + "/bin/reproductionMeasure").exists()) existence = false;

        if (existence)
        {
            // Then we delete the current contents of bin and dictionary
            QStringList fileList;
            QDir originDir("bin");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::remove("bin/" + *it);
            }
            originDir.cd("dictionary");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::remove("bin/dictionary/" + *it);
            }

            // Then we copy the content of bin and dictionary
            QDir targetDir("bin");
            originDir.cd(folderName + "/bin");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("%1/%2").arg(originDir.path()).arg(*it), QString("bin/%1").arg(*it));
            }
            originDir.cd("dictionary");
            targetDir.cd("dictionary");
            fileList = originDir.entryList(QStringList(), QDir::Files);
            for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
            {
                QFile::copy(QString("%1/%2").arg(originDir.path()).arg(*it), QString("bin/dictionary/%1").arg(*it));
            }

            // Then we change the value of state
            *state = "0";
            QFile stateFile("state");
            stateFile.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream in(&stateFile);
            in << state->toInt();
            stateFile.close();

            // Then we update the view
            backTo0Slot();
        }else{
            QMessageBox::critical(NULL, "Fichiers non trouvés", "Je ne parviens pas à trouver les fichiers dans le dossier donné.");
        }
    }
}
